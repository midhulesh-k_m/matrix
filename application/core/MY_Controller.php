<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller
{
	public $method = '';
	function __construct()
	{


		parent::__construct();
		$this->_hmvc_fixes();

		parent::__construct();

		if (($this->router->fetch_class() == 'login') && (($this->router->fetch_method() == 'index') || ($this->router->fetch_method() == 'login_chk'))) {
			return TRUE;
		} else {
			$this->isLoggedIn();
		}
	}

	function _hmvc_fixes()
	{
		//fix callback form_validation		
		//https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
		$this->load->library('form_validation');
		$this->form_validation->CI = &$this;
	}
	function isLoggedIn()
	{
		if (isset($_SESSION['login_id'])) {
			return TRUE;
		} else {
			redirect('login');
		}
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
