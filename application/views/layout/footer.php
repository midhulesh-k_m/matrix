 <!--Footer-->
 <footer class="footer side-footer">
     <div class="container">
         <div class="row align-items-center flex-row-reverse">
             <div class="col-lg-12 col-sm-12   text-center">
                 Copyright © 2020 <a href="#">Matrix</a>. Designed by <a href="https://www.ecraftz.in/" target="_blank">Ecraftz</a> All rights reserved.
             </div>
         </div>
     </div>
 </footer>
 <!-- End Footer-->
 </div><!-- End page-main-->
 </div><!-- End Page -->
 <!-- Back to top -->
 <a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
 <!-- Jquery js-->
 <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
 <!--Bootstrap.min js-->
 <script src="<?= base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
 <!--Jquery Sparkline js-->
 <script src="<?= base_url() ?>assets/js/jquery.sparkline.min.js"></script>
 <!-- Chart Circle js-->
 <script src="<?= base_url() ?>assets/js/circle-progress.min.js"></script>
 <!-- Star Rating js-->
 <script src="<?= base_url() ?>assets/plugins/rating/jquery.rating-stars.js"></script>
 <!--Moment js-->
 <script src="<?= base_url() ?>assets/plugins/moment/moment.min.js"></script>
 <!-- Daterangepicker js-->
 <script src="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!--Side-menu js-->
 <script src="<?= base_url() ?>assets/plugins/sidemenu/sidemenu.js"></script>

 <!-- P-scrollbar js-->
 <script src="<?= base_url() ?>assets/plugins/p-scrollbar/p-scrollbar.js"></script>
 <script src="<?= base_url() ?>assets/plugins/p-scrollbar/p-scrollbar-leftmenu.js"></script>
 <!-- Charts js-->
 <script src="<?= base_url() ?>assets/plugins/chart/chart.bundle.js"></script>
 <script src="<?= base_url() ?>assets/plugins/chart/utils.js"></script>
 <!-- Owl-Carousel js-->
 <script src="<?= base_url() ?>assets/plugins/owl-carousel/owl.carousel.js"></script>
 <script src="<?= base_url() ?>assets/plugins/owl-carousel/owl.js"></script>
 <!--Morris  Charts js-->
 <script src="<?= base_url() ?>assets/plugins/morris/raphael-min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/morris/morris.js"></script>
 <!--Peitychart js -->
 <script src="<?= base_url() ?>assets/plugins/peitychart/jquery.peity.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/peitychart/peitychart.init.js"></script>
 <!-- Custom-charts js-->
 <script src="<?= base_url() ?>assets/js/index2.js"></script>
 <!--Time Counter js-->
 <script src="<?= base_url() ?>assets/plugins/counters/jquery.missofis-countdown.js"></script>
 <script src="<?= base_url() ?>assets/plugins/counters/counter.js"></script>
 <!-- Rightsidebar js -->
 <script src="<?= base_url() ?>assets/plugins/sidebar/sidebar.js"></script>
 <!-- Custom js-->
 <script src="<?= base_url() ?>assets/js/custom.js"></script>

 <!-- Switcher js-->
 <script src="<?= base_url() ?>assets/switcher/js/switcher.js"></script>
 <!--MutipleSelect js-->
 <!-- Data tables js-->
 <script src="<?= base_url() ?>assets/plugins/datatable/jquery.dataTables.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/datatable/datatable.js"></script>
 <script src="<?= base_url() ?>assets/plugins/datatable/dataTables.responsive.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
 <script src="<?= base_url() ?>assets/js/select2.js"></script>

 <!-- Form-wizard js-->
 <script src="<?= base_url() ?>assets/plugins/formwizard/jquery.smartWizard.js"></script>
 <script src="<?= base_url() ?>assets/plugins/formwizard/fromwizard.js"></script>
 <!--Accordion-Wizard-Form js-->
 <script src="<?= base_url() ?>assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js"></script>
 <script src="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.js"></script>
 <script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>
 </body>


 <!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/index2 by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:29:32 GMT -->

 </html>