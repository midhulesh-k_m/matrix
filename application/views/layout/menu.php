<aside class="app-sidebar toggle-sidebar">
    <ul class="side-menu toggle-menu">
        <li>
            <h3>Main</h3>
        </li>

        <li> <a class="side-menu__item" href="<?= base_url() ?>"><i class="side-menu__icon fe fe-codepen"></i><span class="side-menu__label">Dashboard</span></i></a></li>
        <li>
            <h3>Master</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-file-text"></i><span class="side-menu__label">Master</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="<?= base_url() ?>master" class="slide-item"> Manage User Details</a></li>
                <li><a href="<?= base_url() ?>master/technician" class="slide-item"> Technician Details</a></li>
                <li><a href="<?= base_url() ?>master/customer" class="slide-item">Customer details</a></li>
                <li><a href="wysiwyag.html" class="slide-item">Add location</a></li>
                <li><a href="<?= base_url() ?>inhouse/addissue" class="slide-item"> Add Item</a></li>
                <li><a href="<?= base_url() ?>inhouse/issueItem" class="slide-item"> Issue Item</a></li>
            </ul>
        </li>
        <li>
            <h3>Inhouse Service</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-bar-chart-2"></i><span class="side-menu__label">Inhouse Service</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="<?= base_url() ?>inhouse" class="slide-item">New Order</a></li>
                <li><a href="<?= base_url() ?>inhouse/allocation" class="slide-item"> Allotment</a></li>
                <li><a href="<?= base_url() ?>inhouse/allote" class="slide-item"> Repair</a></li>
                <li><a href="<?= base_url() ?>inhouse/verification" class="slide-item"> Verification</a></li>
                <li><a href="<?= base_url() ?>inhouse/delivery" class="slide-item"> Deivery</a></li>
                <li><a href="<?= base_url() ?>inhouse/delivered" class="slide-item"> Delivered Orders</a></li>
                <li><a href="<?= base_url() ?>inhouse/unpaid" class="slide-item"> Unpaid Orders</a></li>
                <li><a href="<?= base_url() ?>inhouse/cancelled" class="slide-item"> Cancelled Orders</a></li>

                <li><a href="<?= base_url() ?>inhouse/returned" class="slide-item"> Returned List</a></li>
                <li><a href="<?= base_url() ?>inhouse/refund" class="slide-item"> Refund List</a></li>
                <li><a href="<?= base_url() ?>inhouse/allorders" class="slide-item"> All orders</a></li>

            </ul>
        </li>
        <li>
            <h3>Onsite Service</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-bar-chart-2"></i><span class="side-menu__label">Onsite Service</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="<?= base_url() ?>onsite" class="slide-item">New Order</a></li>
                <li><a href="<?= base_url() ?>onsite/allocation" class="slide-item"> Allotment</a></li>
                <li><a href="<?= base_url() ?>onsite/allote" class="slide-item"> Onsite Order List</a></li>
                <li><a href="<?= base_url() ?>onsite/verification" class="slide-item"> Service Verification</a></li>
                <li><a href="<?= base_url() ?>onsite/delivery" class="slide-item"> Onsite Delivery List</a></li>
                <li><a href="<?= base_url() ?>onsite/delivered" class="slide-item"> Onsite Delivered List</a></li>
                <li><a href="<?= base_url() ?>onsite/cancelled" class="slide-item"> Onsite Cancelled List</a></li>

                <!-- <li><a href="chart-js.html" class="slide-item"> Service Call Report</a></li>
                <li><a href="chart-js.html" class="slide-item"> Service Completed List</a></li> -->
                <!-- <li><a href="chart-js.html" class="slide-item"> Manage Onsite Order</a></li> -->

            </ul>
        </li>


        <li>
            <h3>Toner Service</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-bar-chart-2"></i><span class="side-menu__label">Toner Service</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="<?= base_url() ?>toner" class="slide-item">New Order</a></li>
                <li><a href="<?= base_url() ?>toner/allocated" class="slide-item"> Allocated List</a></li>
                <li><a href="<?= base_url() ?>toner/finished" class="slide-item"> Finished List</a></li>
                <li><a href="<?= base_url() ?>toner/delivered" class="slide-item">Delivered</a></li>
                <!-- <li><a href="<?= base_url() ?>onsite/delivery" class="slide-item"> Onsite Delivery List</a></li>
                <li><a href="<?= base_url() ?>onsite/delivered" class="slide-item"> Onsite Delivered List</a></li>
                <li><a href="<?= base_url() ?>onsite/cancelled" class="slide-item"> Onsite Cancelled List</a></li> -->

                <!-- <li><a href="chart-js.html" class="slide-item"> Service Call Report</a></li>
                <li><a href="chart-js.html" class="slide-item"> Service Completed List</a></li> -->
                <!-- <li><a href="chart-js.html" class="slide-item"> Manage Onsite Order</a></li> -->

            </ul>
        </li>

        <!-- <li>
            <h3>Verification & Delivery Orders</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-box"></i><span class="side-menu__label">Verification</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="alerts.html" class="slide-item"> Verification list</a></li>

            </ul>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-calendar"></i><span class="side-menu__label">Delivery</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="calendar.html" class="slide-item">Delivered list</a></li>
                <li><a href="calendar2.html" class="slide-item">Delivery pending</a></li>
            </ul>
        </li> -->
        <!-- <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-layers"></i><span class="side-menu__label">Advanced UI</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="chat.html" class="slide-item"> Default Chat</a></li>
                <li><a href="notify.html" class="slide-item"> Notifications</a></li>
                <li><a href="sweetalert.html" class="slide-item"> Sweet alerts</a></li>
                <li><a href="rangeslider.html" class="slide-item"> Range slider</a></li>
                <li><a href="scroll.html" class="slide-item"> Content Scroll bar</a></li>
                <li><a href="counters.html" class="slide-item">Counters</a></li>
                <li><a href="loaders.html" class="slide-item"> Loaders</a></li>
                <li><a href="time-line.html" class="slide-item"> Time Line</a></li>
                <li><a href="rating.html" class="slide-item"> Rating</a></li>
                <li><a href="accordion.html" class="slide-item"> Accordions</a></li>
                <li><a href="tabs.html" class="slide-item"> Tabs</a></li>
                <li><a href="footers.html" class="slide-item">Footers</a></li>
                <li><a href="crypto-currencies.html" class="slide-item"> Crypto-currencies</a></li>
                <li><a href="users-list.html" class="slide-item"> User List</a></li>
                <li><a href="search.html" class="slide-item"> Search page</a></li>
            </ul>
        </li> -->
        <!-- <li>
            <a class="side-menu__item" href="maps.html"><i class="side-menu__icon fe fe-map-pin"></i><span class="side-menu__label">Maps</span></a>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-compass"></i><span class="side-menu__label">Icons</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="icons.html" class="slide-item"> Font Awesome</a></li>
                <li><a href="icons2.html" class="slide-item"> Material Design Icons</a></li>
                <li><a href="icons3.html" class="slide-item"> Simple Line Icons</a></li>
                <li><a href="icons4.html" class="slide-item"> Feather Icons</a></li>
                <li><a href="icons5.html" class="slide-item"> Ionic Icons</a></li>
                <li><a href="icons6.html" class="slide-item"> Flag Icons</a></li>
                <li><a href="icons7.html" class="slide-item"> pe7 Icons</a></li>
                <li><a href="icons8.html" class="slide-item"> Themify Icons</a></li>
                <li><a href="icons9.html" class="slide-item">Typicons Icons</a></li>
                <li><a href="icons10.html" class="slide-item">Weather Icons</a></li>
            </ul>
        </li> -->
        <!-- <li>
            <h3>Employees</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-briefcase"></i><span class="side-menu__label">Create Employees</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="<?= base_url() ?>profile" class="slide-item"> Create new employe</a></li>
                <li><a href="editprofile.html" class="slide-item"> List employe</a></li>
                <!-- <li><a href="email.html" class="slide-item"> Email</a></li>
                <li><a href="emailservices.html" class="slide-item"> Email Inbox</a></li>
                <li><a href="gallery.html" class="slide-item"> Gallery</a></li>
                <li><a href="about.html" class="slide-item"> About Company</a></li>
                <li><a href="services.html" class="slide-item"> Services</a></li>
                <li><a href="faq.html" class="slide-item"> FAQS</a></li>
                <li><a href="terms.html" class="slide-item"> Terms and Conditions</a></li>
                <li><a href="empty.html" class="slide-item"> Empty Page</a></li>
                <li><a href="construction.html" class="slide-item"> Under Construction</a></li>
                <li><a href="blog.html" class="slide-item"> Blog</a></li>
                <li><a href="invoice.html" class="slide-item"> Invoice</a></li>
                <li><a href="pricing.html" class="slide-item"> Pricing Tables</a></li> -->
        <!--  </ul>
    </li> -->
        <li>
            <h3>Add Brand & Model</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-shopping-cart"></i><span class="side-menu__label"> Brand</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="product.html" class="slide-item"> Add brand</a></li>
                <li><a href="product-details.html" class="slide-item">list brand</a></li>
                <!-- <li><a href="cart.html" class="slide-item"> Shopping Cart</a></li> -->
            </ul>
        </li>

        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-unlock"></i><span class="side-menu__label">Add model</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="login.html" class="slide-item"> Add model</a></li>
                <li><a href="register.html" class="slide-item"> List model</a></li>
                <!-- <li><a href="forgot-password.html" class="slide-item"> Forgot Password</a></li>
                <li><a href="lockscreen.html" class="slide-item"> Lock screen</a></li> -->
            </ul>
        </li>
        <li>
            <h3>Reports</h3>
        </li>
        <li class="slide">
            <a class="side-menu__item" data-toggle="slide" href="index-2.html#"><i class="side-menu__icon fe fe-alert-triangle"></i><span class="side-menu__label">Report Pages</span><i class="angle fa fa-angle-right"></i></a>
            <ul class="slide-menu">
                <li><a href="400.html" class="slide-item"> 400 Error</a></li>
                <li><a href="401.html" class="slide-item"> 401 Error</a></li>
                <li><a href="403.html" class="slide-item"> 403 Error</a></li>
                <li><a href="404.html" class="slide-item"> 404 Error</a></li>
                <li><a href="500.html" class="slide-item"> 500 Error</a></li>
                <li><a href="503.html" class="slide-item"> 503 Error</a></li>
            </ul>
        </li>
        <li> <a class="side-menu__item" href="<?= base_url() ?>"><i class="side-menu__icon fe fe-codepen"></i><span class="side-menu__label">Settings</span></i></a></li>
        <li>
    </ul>
</aside>
<!--sidemenu end-->