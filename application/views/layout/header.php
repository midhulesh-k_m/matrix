<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/index2 by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:29:29 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="IndoUi – Bootstrap 4 Admin Dashboard HTML Template" name="description">
    <meta content="Spruko Technologies Private Limited" name="author">
    <meta name="keywords" content="admin, admin dashboard template, admin panel template, admin template, best bootstrap admin template, bootstrap 4 admin template, bootstrap 4 dashboard template, bootstrap admin template, bootstrap dashboard template, html admin template, html5 dashboard template, html5 admin template, modern admin template, simple admin template, template admin bootstrap 4" />
    <!-- Favicon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/brand/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/images/brand/favicon.ico" />
    <!-- Title -->
    <title>Matrix</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!--Style css -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/dark-style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/skin-modes.css" rel="stylesheet">
    <!-- P-scrollbar css-->
    <link href="<?= base_url() ?>assets/plugins/p-scrollbar/p-scrollbar.css" rel="stylesheet" />
    <!-- Sidemenu css -->
    <link href="<?= base_url() ?>assets/plugins/sidemenu/sidemenu.css" rel="stylesheet" />
    <!--Daterangepicker css-->
    <link href="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <!-- Owl-Carousel css -->
    <link href="<?= base_url() ?>assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
    <!-- Rightsidebar css -->

    <link href="<?= base_url() ?>assets/plugins/sidebar/sidebar.css" rel="stylesheet">
    <!---Icons css-->
    <link href="<?= base_url() ?>assets/css/icons.css" rel="stylesheet" />

    <!---Switcher css-->
    <link href="<?= base_url() ?>assets/switcher/css/switcher.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/switcher/demo.css" rel="stylesheet" />
</head>



<!-- second moule -->


<body class="app sidebar-mini rtl">

    <!-- Switcher -->
    <div class="switcher-wrapper">
        <div class="demo_changer">
            <div class="demo-icon bg_dark">
                <i class="fa fa-cog fa-spin text_primary"></i>
            </div>
            <div class="form_holder sidebar-right1">
                <div class="row">
                    <div class="predefined_styles">
                        <div class="swichermainleft border-top mt-2 text-center">
                            <div class="p-3">
                                <a class="btn btn-warning btn-block mt-0" href="<?= base_url() ?>">LTR Version</a>
                                <a class="btn btn-info btn-block" href="https://laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-rtl/index">RTL Version</a>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Navigation Style</h4>
                            <div class="pl-3 pr-3">
                                <a class="wscolorcode blackborder nav-hor navstyle1" href="https://laravel.spruko.com/indoui/Horizontal-Light-ltr/index">Horizontal</a>
                                <a class="wscolorcode blackborder nav-hor navstyle1" href="<?= base_url() ?>">Left-menu</a>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Skin Modes</h4>
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Light Mode</span>
                                    <div class="onoffswitch2">
                                        <input checked class="onoffswitch2-checkbox" id="myonoffswitch" name="onoffswitch2" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Dark Mode</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch1" name="onoffswitch2" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch1"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Skin Modes</h4>
                            <div class="switch_section">
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Default Body</span>
                                    <div class="onoffswitch2">
                                        <input checked class="onoffswitch2-checkbox" id="myonoffswitch7" name="onoffswitch" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch7"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Body Style1</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch6" name="onoffswitch" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch6"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Background Patterns</h4>
                            <div class="skin-body light-pattern">
                                <button class="bg1 wscolorcode1 blackborder" id="background1" type="button"></button>
                                <button class="bg2 wscolorcode1 blackborder" id="background2" type="button"></button>
                                <button class="bg3 wscolorcode1 blackborder" id="background3" type="button"></button>
                                <button class="bg4 wscolorcode1 blackborder" id="background4" type="button"></button>
                                <button class="bg5 wscolorcode1 blackborder" id="background5" type="button"></button>
                            </div>
                            <div class="skin-body dark-pattern">
                                <button class="bg6 wscolorcode1 blackborder" id="background6" type="button"></button>
                                <button class="bg7 wscolorcode1 blackborder" id="background7" type="button"></button>
                                <button class="bg8 wscolorcode1 blackborder" id="background8" type="button"></button>
                                <button class="bg9 wscolorcode1 blackborder" id="background9" type="button"></button>
                                <button class="bg10 wscolorcode1 blackborder" id="background10" type="button"></button>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Leftmenu Bg-Image</h4>
                            <div class="skin-body">
                                <button class="bgimage1 wscolorcode1 blackborder" id="leftmenuimage1" type="button"></button>
                                <button class="bgimage2 wscolorcode1 blackborder" id="leftmenuimage2" type="button"></button>
                                <button class="bgimage3 wscolorcode1 blackborder" id="leftmenuimage3" type="button"></button>
                                <button class="bgimage4 wscolorcode1 blackborder" id="leftmenuimage4" type="button"></button>
                                <button class="bgimage5 wscolorcode1 blackborder" id="leftmenuimage5" type="button"></button>
                            </div>
                        </div>
                        <div class="swichermainleft">
                            <h4>Leftmenu Styles</h4>
                            <div class="switch_section">
                                <div class="switch-toggle horizontal-light-switcher d-flex">
                                    <span class="mr-auto">Leftmenu Light</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch9" name="onoffswitch3" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch9"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Leftmenu Color</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch10" name="onoffswitch3" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch10"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle horizontal-Dark-switcher d-flex">
                                    <span class="mr-auto">Leftmenu Dark</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch11" name="onoffswitch3" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch11"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Leftmenu Gradient Color</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch12" name="onoffswitch3" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch12"></label>
                                    </div>
                                </div>
                                <div class="switch-toggle d-flex">
                                    <span class="mr-auto">Reset Leftmenu Styles</span>
                                    <div class="onoffswitch2">
                                        <input class="onoffswitch2-checkbox" id="myonoffswitch13" name="onoffswitch3" type="radio"> <label class="onoffswitch2-label" for="myonoffswitch13"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swichermainleft border-top mt-2 text-center">
                            <div class="p-3">
                                <a class="btn btn-primary btn-block mt-0" href="#">View Demo</a>
                                <a class="btn btn-secondary btn-block" href="https://themeforest.net/item/indoui-admin-bootstrap-4-dashboard-html-template/24864971">Buy Now</a>
                                <a class="btn btn-success btn-block" href="https://themeforest.net/user/sprukosoft/portfolio">Our Portfolio</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Switcher -->

    <!--Global-Loader-->
    <div id="global-loader">
        <img src="<?= base_url() ?>assets/images/loader.svg" alt="loader">
    </div>
    <div class="page">
        <div class="page-main">
            <!--app-header-->
            <div class="app-header header d-flex navbar-collapse">
                <div class="container-fluid">
                    <div class="d-flex">
                        <a class="header-brand" href="<?= base_url() ?>">
                            <img src="<?= base_url() ?>assets/images/brand/logo.png" class="header-brand-img main-logo" alt="IndoUi logo">
                            <img src="<?= base_url() ?>assets/images/brand/logo-light.png" class="header-brand-img dark-main-logo" alt="IndoUi logo">
                            <img src="<?= base_url() ?>assets/images/brand/icon-light.png" class="header-brand-img dark-icon-logo" alt="IndoUi logo">
                            <img src="<?= base_url() ?>assets/images/brand/icon.png" class="header-brand-img icon-logo" alt="IndoUi logo">
                        </a><!-- logo-->
                        <div class="app-sidebar__toggle" data-toggle="sidebar">
                            <a class="open-toggle" href="#"><i class="fe fe-align-left"></i></a>
                            <a class="close-toggle" href="#"><i class="fe fe-x"></i></a>
                        </div>
                        <div class="d-none dropdown d-md-flex header-settings">
                            <a class="nav-link icon" data-toggle="dropdown">
                                <i class="fe fe-grid mr-2"></i><span class="lay-outstyle mt-1">Settings</span>
                                <span class="pulse2 bg-warning" aria-hidden="true"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left dropdown-menu-arrow">
                                <a class="dropdown-item" href="#">Option 1</a>
                                <a class="dropdown-item" href="#">Option 2</a>
                                <a class="dropdown-item" href="#">Option 3</a>
                                <a class="dropdown-item" href="#">Option 4</a>
                                <a class="dropdown-item" href="#">Option 5</a>
                            </div>
                        </div>
                        <div class="d-flex order-lg-2 ml-auto header-right">
                            <div class="d-md-flex header-search" id="bs-example-navbar-collapse-1">
                                <form class="navbar-form" role="search">
                                    <div class="input-group ">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <button type="reset" class="btn btn-default">
                                                <i class="fe fe-x"></i>
                                            </button>
                                            <button type="submit" class="btn btn-default">
                                                <i class="fe fe-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div><!-- Search -->
                            <div class="d-md-flex">
                                <a href="#" class="nav-link icon full-screen-link">
                                    <i class="fe fe-minimize fullscreen-button"></i>
                                </a>
                            </div>
                            <div class="dropdown d-md-flex header-message">
                                <a class="nav-link icon" data-toggle="dropdown">
                                    <i class="fe fe-bell"></i>
                                    <span class="nav-unread badge badge-danger badge-pill">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item text-center" href="#">Notifications</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item d-flex pb-4" href="#">
                                        <span class="avatar mr-3 br-3 align-self-center avatar-md cover-image bg-primary-transparent text-primary"><i class="fe fe-mail"></i></span>
                                        <div>
                                            <span class="font-weight-bold"> Commented on your post </span>
                                            <div class="small text-muted d-flex">
                                                3 hours ago
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex pb-4" href="#">
                                        <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-secondary-transparent text-secondary"><i class="fe fe-download"></i>
                                        </span>
                                        <div>
                                            <span class="font-weight-bold"> New file has been Uploaded </span>
                                            <div class="small text-muted d-flex">
                                                5 hour ago
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex pb-4" href="#">
                                        <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-warning-transparent text-warning"><i class="fe fe-user"></i>
                                        </span>
                                        <div>
                                            <span class="font-weight-bold"> User account has Updated</span>
                                            <div class="small text-muted d-flex">
                                                20 mins ago
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex pb-4" href="#">
                                        <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-info-transparent text-info"><i class="fe fe-shopping-cart"></i>
                                        </span>
                                        <div>
                                            <span class="font-weight-bold"> New Order Recevied</span>
                                            <div class="small text-muted d-flex">
                                                1 hour ago

                                            </div>
                                        </div>
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <div class="text-center dropdown-btn pb-3">
                                        <div class="btn-list">
                                            <a href="#" class="btn btn-primary btn-sm"><i class="fe fe-plus mr-1"></i>Add New</a>
                                            <a href="#" class=" btn btn-secondary btn-sm"><i class="fe fe-eye mr-1"></i>View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Navbar -->
                            <div class="dropdown header-profile">
                                <a class="nav-link pr-0 leading-none d-flex pt-1" data-toggle="dropdown" href="#">
                                    <span class="avatar avatar-md brround cover-image" data-image-src="<?= base_url() ?>assets/images/users/female/2.jpg"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <div class="drop-heading">
                                        <div class="text-center">
                                            <h5 class="text-dark mb-1">Vanessa Dyer</h5>
                                            <small class="text-muted">Web Developer</small>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider m-0"></div>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-user"></i>Profile</a>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-edit"></i>Schedule</a>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-mail"></i> Inbox</a>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-unlock"></i> Look Screen</a>
                                    <a class="dropdown-item" href="<?= base_url() ?>login/logout"><i class="dropdown-icon fe fe-power"></i> Log Out</a>
                                    <div class="dropdown-divider"></div>
                                    <div class="text-center dropdown-btn pb-3">
                                        <div class="btn-list">
                                            <a href="#" class="btn btn-icon btn-facebook btn-sm"><i class="icon icon-social-facebook"></i></a>
                                            <a href="#" class="btn btn-icon btn-twitter btn-sm"><i class="icon icon-social-twitter"></i></a>
                                            <a href="#" class="btn btn-icon btn-instagram btn-sm"><i class="icon icon-social-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown d-md-flex Sidebar-setting">
                                <a href="#" class="nav-link icon" data-toggle="sidebar-right" data-target=".sidebar-right">
                                    <i class="fe fe-more-horizontal"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--app-header end-->
            <!-- Sidebar menu-->
            <div class="app-sidebar__overlay" data-toggle="sidebar"></div>