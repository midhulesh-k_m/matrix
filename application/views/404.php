<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/404 by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:31:07 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="IndoUi – Bootstrap 4 Admin Dashboard HTML Template" name="description">
    <meta content="Spruko Technologies Private Limited" name="author">
    <meta name="keywords" content="admin, admin dashboard template, admin panel template, admin template, best bootstrap admin template, bootstrap 4 admin template, bootstrap 4 dashboard template, bootstrap admin template, bootstrap dashboard template, html admin template, html5 dashboard template, html5 admin template, modern admin template, simple admin template, template admin bootstrap 4" />
    <!-- Favicon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/brand/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/brand/favicon.ico" />
    <!-- Title -->
    <title>IndoUi – Bootstrap 4 Admin Dashboard HTML Template</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!--Style css -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/dark-style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/skin-modes.css" rel="stylesheet">
    <!-- P-scrollbar css-->
    <link href="<?= base_url() ?>assets/plugins/p-scrollbar/p-scrollbar.css" rel="stylesheet" />
    <!-- Sidemenu css -->
    <link href="<?= base_url() ?>assets/plugins/sidemenu/sidemenu.css" rel="stylesheet" />
    <!--Daterangepicker css-->
    <link href="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <!-- Rightsidebar css -->
    <link href="<?= base_url() ?>assets/plugins/sidebar/sidebar.css" rel="stylesheet">
    <!---Icons css-->
    <link href="<?= base_url() ?>assets/css/icons.css" rel="stylesheet" />

    <!---Switcher css-->
    <link href="<?= base_url() ?>assets/switcher/css/switcher.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/switcher/demo.css" rel="stylesheet" />
</head>

<body class="bg-account">
    <!-- page -->
    <div class="page">

        <!-- page-content -->
        <div class="page-content">
            <div class="container text-center text-dark">
                <div class="display-2 mb-5"><span class=""><i class="ti-face-smile mr-1"></i></span>ops!</div>
                <h1 class="h2  mb-5">Error 404: Page Not Found</h1>
                <a class="btn btn-primary mb-0" href="<?= base_url() ?>">
                    Back To Home
                </a>
            </div>
        </div>
        <!-- page-content end -->

    </div>
    <!-- page End-->

    <!-- Jquery js-->
    <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
    <!--Bootstrap.min js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--Jquery Sparkline js-->
    <script src="<?= base_url() ?>assets/js/jquery.sparkline.min.js"></script>
    <!-- Chart Circle js-->
    <script src="<?= base_url() ?>assets/js/circle-progress.min.js"></script>
    <!-- Star Rating js-->
    <script src="<?= base_url() ?>assets/plugins/rating/jquery.rating-stars.js"></script>
    <!--Moment js-->
    <script src="<?= base_url() ?>assets/plugins/moment/moment.min.js"></script>
    <!-- Daterangepicker js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Custom js-->
    <script src="<?= base_url() ?>assets/js/custom.js"></script>
</body>

<!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/404 by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:31:07 GMT -->

</html>