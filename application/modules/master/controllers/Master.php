<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('master_model');
    }
    public function index()
    {
        $data = [];
        $data['u_types'] = $this->master_model->getUserTypes();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('manage_user', $data);
        $this->load->view('layout/footer');
    }
    public function technician()
    {
        $data = [];
        $data['u_types'] = $this->master_model->getUserTypes();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('technician', $data);
        $this->load->view('layout/footer');
    }
    public function customer()
    {
        $data = [];
        $data['u_types'] = $this->master_model->getUserTypes();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('customer', $data);
        $this->load->view('layout/footer');
    }
}
