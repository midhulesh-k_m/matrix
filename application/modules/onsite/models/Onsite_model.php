<?php
class Onsite_model extends CI_Model
{
    public function __construct()
    {
        $this->lgid = $_SESSION['login_id'];
        $this->u_type = $_SESSION['user_type'];
        $this->softid = $_SESSION['soft_id'];
    }
    public function getPlaces()
    {
        return   $this->db->select('loc_id,location')->where('soft_id', $this->softid)->get('locations')->result_array();
    }
    public function getdelaerlikecus($id)
    {
        if ($id == 'Dealer') {
            $wre = "ordertype = 'Dealer'";
        } else {
            $wre = "ordertype != 'Dealer'";
        }
        return   $this->db->where('soft_id', $this->softid)->where('worktype', 'ONSITE')->where($wre)->get('customers')->result_array();
    }
    public function getMaxOfCusId()
    {
        return  $this->db->query("SELECT IFNULL(max(`cust_id` + 1),1)  as ord_id FROM  customers")->row_array();
    }
    public function getOrderNum()
    {
        $date = date('Y');
        $array = str_split($date, 1);
        $int = $array[3] + 1;
        $yr =  $array[2] . $array[3];

        // $order_id =   $this->db->select('onsiteorderno as orderno')->where('soft_id',  $this->softid)->get('onsiteorders')->row_array();
        $order_id = $this->db->query("SELECT `onsiteorderno`  as `orderno` FROM `onsiteorders` WHERE `soft_id` = '$this->softid' ORDER BY onsiteorder_id DESC")->row_array();

        if ($order_id) {

            $id = explode('/', $order_id['orderno']);


            $id = $id[1] + 1;


            return  'ON' . $int . '-' . $yr . '/' . $id;
        } else {
            return  'ON' . $int . '-' . $yr . '/1';
        }
    }
    public function selectmax_smsId()
    {
        return  $this->db->query("SELECT IFNULL(max(`smsId` + 1),1)  as ord_id FROM sendsms")->row_array();
    }
    public function getOnsiteCus()
    {
        return   $this->db->where('worktype', 'ONSITE')->where('soft_id', $this->softid)->get('customers')->result_array();
    }
    public function getlocation()
    {
        return   $this->db->where('soft_id', $this->softid)->get('locations')->result_array();
    }
    public function getUserDtlsById($refno = null, $uid = null)
    {

        if (!($refno)) {

            $customer =   $this->db->select('customers.custname,customers.address,customers.emailid,customers.mobile,customers.landline,customers.place,customers.landmark')->where('cust_id', $uid)->where('soft_id',   $this->softid)->get('customers')->row_array();

            if ($customer) {
                $xus = [];
                $xus['custname'] = $customer['custname'];
                $xus['address'] = $customer['address'];
                $xus['emailid'] = $customer['emailid'];
                $xus['mobile'] = $customer['mobile'];
                $xus['landline'] = $customer['landline'];
                $xus['place'] = $customer['place'];
                $xus['landmark'] = $customer['landmark'];
                $xus['cus_orders'] = $this->getCusOrders($uid);
                return  $xus;
            }
        } else {
        }
    }
    public function getCusOrders($id)
    {

        $orders =  $this->db->select('onsiteorderno as orderno,item,servicedate,servicetime,description,status,order_date,servicetype')->where('cust_id', $id)->where('soft_id',  $this->softid)->order_by('onsiteorder_id', 'desc')->get('onsiteorders')->result_array();

        if ($orders) {
            $odrs = [];
            foreach ($orders as $key => $ord_vl) {
                $odrs[$key]['orderno'] = $ord_vl['orderno'];
                $odrs[$key]['item'] = $ord_vl['item'];
                $odrs[$key]['servicedate'] = $ord_vl['servicedate'];
                $odrs[$key]['servicetime'] = $ord_vl['servicetime'];
                $odrs[$key]['servicetype'] = $ord_vl['servicetype'];
                $odrs[$key]['status'] = $ord_vl['status'];
                $odrs[$key]['order_date'] = $ord_vl['order_date'];
                // $odrs[$key]['create_date'] = $ord_vl['create_date'];
                // $odrs[$key]['status'] = $ord_vl['status'];
                // $odrs[$key]['brand'] = $this->getBrandByBrandId($ord_vl['branditem_id']);
                // $odrs[$key]['model'] = $this->getModelByModelId($ord_vl['model_id']);
            }
            return  $odrs;
        }
    }
    public function getBrandByBrandId($bid)
    {
        return $this->db->select('brands.brandname,branditems.brand_id')->join('brands', 'brands.brand_id = branditems.brand_id', 'inner')->where('branditems.branditem_id', $bid)->where('brands.brand_id = branditems.brand_id')->where('branditems.soft_id',  $this->softid)->get('branditems')->row_array();
    }
    public function getModelByModelId($bid)
    {
        return $this->db->select('modelname')->where('model_id', $bid)->where('soft_id', $this->softid)->get('models')->row_array();
    }
    public function getBrandByItem($item)
    {
        // return $this->db->select('brand_id,brandname,')->where('soft_id', 0)->get('brands')->result_array();

        return $this->db->select('branditems.brand_id,branditems.branditem_id,branditems.item,brands.brand_id,brands.brandname')->join('brands', 'brands.brand_id = branditems.brand_id', 'inner')->where('branditems.item', $item)->where('branditems.soft_id',  $this->softid)->get('branditems')->result_array();
    }
    public function getModelById($brand_id)
    {

        return $this->db->select('model_id,modelname,')->where('branditem_id', $brand_id)->where('soft_id', $this->softid)->get('models')->result_array();
    }
    public function getCusDataByRefNo($ref_no)
    {
        $ref_data = [];
        $cusid =  $this->db->where('onsiteorderno', $ref_no)->where('soft_id',  $this->softid)->get('onsiteorders')->row_array();

        $ref_data['order_dtls'] =  $this->db->query("SELECT * FROM `onsiteorders` WHERE `onsiteorderno` = '$ref_no' AND `soft_id` = '$this->softid'")->row_array();


        $dtex = strtotime($cusid['servicedate']);
        $ref_data['servicedatex'] = date('Y-m-d', $dtex);

        $dtext = strtotime($cusid['servicetime']);
        $ref_data['servicetimex'] = date('H:i', $dtext);


        $customer =   $this->db->select('customers.cust_id,customers.ordertype,customers.custname,customers.address,customers.emailid,customers.mobile,customers.landline,customers.place')->where('cust_id', $cusid['cust_id'])->where('soft_id',  $this->softid)->get('customers')->row_array();


        $ref_data['cus_data'] =  $customer;

        // $ref_data['allcustomers'] =  $this->db->get('onsiteorders')->result_array();
        $ref_data['allcustomers'] = $this->db->where('worktype', 'ONSITE')->where('soft_id', $this->softid)->get('customers')->result_array();


        $customerx =   $this->db->where('worktype', 'ONSITE')->where('cust_id', $cusid['cust_id'])->where('soft_id',  $this->softid)->get('customers')->row_array();


        $xus = [];
        $xus['custname'] = $customerx['custname'];
        $xus['address'] = $customerx['address'];
        $xus['emailid'] = $customerx['emailid'];
        $xus['mobile'] = $customerx['mobile'];
        $xus['landline'] = $customerx['landline'];
        $xus['landmark'] = $customerx['landmark'];
        $xus['place'] = $customerx['place'];
        $xus['cus_orders'] = $this->getCusOrdersz($cusid['cust_id']);
        $ref_data['cus_address'] =  $xus;


        $ref_data['complaints']  = $this->db->query("SELECT onsitecomplaints.prob_id,problems.probname FROM onsitecomplaints INNER JOIN problems ON problems.prob_id = onsitecomplaints.prob_id WHERE  onsitecomplaints.orderno = '$ref_no' AND onsitecomplaints.status ='Pending' ")->result_array();

        return  $ref_data;
    }
    public function getCusOrdersz($id)
    {


        $orders =  $this->db->select('onsiteorderno as orderno,item,servicedate,servicetime,description,status,order_date,servicetype')->where('cust_id', $id)->where('soft_id',  $this->softid)->order_by('onsiteorder_id', 'desc')->get('onsiteorders')->result_array();

        if ($orders) {
            $odrs = [];
            foreach ($orders as $key => $ord_vl) {
                $odrs[$key]['orderno'] = $ord_vl['orderno'];
                $odrs[$key]['item'] = $ord_vl['item'];
                $odrs[$key]['servicedate'] = $ord_vl['servicedate'];
                $odrs[$key]['servicetime'] = $ord_vl['servicetime'];
                $odrs[$key]['servicetype'] = $ord_vl['servicetype'];
                $odrs[$key]['status'] = $ord_vl['status'];
                $odrs[$key]['order_date'] = $ord_vl['order_date'];
                // $odrs[$key]['create_date'] = $ord_vl['create_date'];
                // $odrs[$key]['status'] = $ord_vl['status'];
                // $odrs[$key]['brand'] = $this->getBrandByBrandId($ord_vl['branditem_id']);
                // $odrs[$key]['model'] = $this->getModelByModelId($ord_vl['model_id']);
            }
            return  $odrs;
        }
    }
    public function getAllGroups()
    {
        return  $this->db->select('group_id,groupname')->where('soft_id',  $this->softid)->get('groups')->result_array();
    }
    public function getitemsbygroup($grp_val)
    {
        return  $this->db->select('item_id,itemname')->where('group_id', $grp_val)->where('soft_id',  $this->softid)->get('items')->result_array();
    }
    public function getproblems()
    {
        return  $this->db->select('probname,prob_id')->where('soft_id',  $this->softid)->get('problems')->result_array();
    }
    public function newOrser($curtomer, $ord_num)
    {
        $this->db->insert('onsiteorders', $curtomer);

        // try {
        //     // $this->db->trans_start(FALSE);
        //     $this->db->insert('onsiteorders', $curtomer);
        //     echo $this->db->last_query();die();
        //     // $this->db->trans_complete();
        //     $db_error = $this->db->error();
        //     if (!empty($db_error['message'])) {
        //         throw new Exception('Failed to insert complaint => Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
        //         return false;
        //     } else {
        return $ord_num;
        //     }
        // } catch (Exception $e) {
        //     return  log_message('error', $e->getMessage());
        // }
    }
    public function selectmax_ordId()
    {
        return  $this->db->query("SELECT IFNULL(max(`onsiteorder_id` + 1),1)  as ord_id FROM onsiteorders")->row_array();
    }
    public function selectmax_cmpId()
    {
        return  $this->db->query("SELECT IFNULL(max(`complaint_id` + 1),1)  as ord_id FROM onsitecomplaints")->row_array();
    }
    public function setApproxPrc($ord_num,   $rough_amt, $date)
    {
        $this->db->set('rough_estimate', $rough_amt)->set('appx_date', $date)->where('orderno', $ord_num)->update('orders');
    }
    public function newOrderCmplnt($cmplnt)
    {
        $this->db->insert('onsitecomplaints', $cmplnt);
    }
    public function selectmax_recieved()
    {
        return  $this->db->query("SELECT IFNULL(max(`revitem_id` + 1),1)  as ord_id FROM recieveitems")->row_array();
    }
    public function newrecievedItem($item)
    {
        $this->db->insert('recieveitems', $item);
    }

    public function getFinishedInhouseOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Finished' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getVerifiedOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date,orderverify.serviceamt FROM orderverify INNER JOIN orders ON orders.orderno = orderverify.orderno INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Verified' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getOnsiteTechnician()
    {
        return  $this->db->query("SELECT techn_id,tech_name FROM technicians WHERE `onsite` = 1 AND status = 'Valid' AND soft_id = $this->softid")->result_array();
    }
    public function getMaxOfTechId()
    {
        return  $this->db->query("SELECT IFNULL(max(`allocatetech_id` + 1),1)  as ord_id FROM onsiteallocatetechinician")->row_array();
    }
    public function allocateNewOrder($allocate, $ord_num, $tech_id)
    {


        $this->db->set('status', "Allocated")->where('onsiteorderno', $ord_num)->update('onsiteorders');
        $this->db->set('allocator_id',  $this->lgid)->where('orderno', $ord_num)->update(' onsitecomplaints');
        $this->db->insert('onsiteallocatetechinician', $allocate);
        return $this->db->insert_id();
    }
    public function getProbLists($id)
    {
        return    $this->db->query("SELECT onsitecomplaints.prob_id,problems.probname FROM onsitecomplaints INNER JOIN problems ON problems.prob_id = onsitecomplaints.prob_id WHERE  onsitecomplaints.orderno = '$id' AND onsitecomplaints.status ='Pending' ")->result_array();
    }
    public function getPendingOnsiteOrders()
    {
        return  $this->db->query("SELECT customers.place,customers.custname,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicedatetime,onsiteorders.item,onsiteorders.servicetype,onsiteorders.servicetime,onsiteorders.status,onsiteorders.order_date FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.status = 'Pending' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id =   $this->softid")->result_array();
    }
    public function getDatatoprint($id)
    {
        return  $this->db->query("SELECT customers.place,customers.custname,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicedatetime,onsiteorders.item,onsiteorders.servicetype,onsiteorders.servicetime,onsiteorders.status,onsiteorders.order_date FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.onsiteorderno = '$id' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id =   $this->softid")->row_array();
    }
    public function getAllotedOrders()
    {

        return   $this->db->query("SELECT onsiteorders.status,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicetime,onsiteorders.servicetype,customers.landmark,customers.address,customers.custname,customers.mobile FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id  WHERE (onsiteorders.status ='Recheck' OR onsiteorders.status ='Allocated') AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id = $this->softid")->result_array();
    }
    public function getOrderTOVerify()
    {
        return  $this->db->query("SELECT onsiteorders.status,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicetime,onsiteorders.servicetype,customers.landmark,customers.address,customers.custname,customers.mobile FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.status ='Finished' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id = $this->softid")->result_array();
    }
    public function getVerifiedOrderListsOnsite()
    {
        return  $this->db->query("SELECT onsiteorders.status,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicetime,onsiteorders.servicetype,customers.landmark,customers.address,customers.custname,customers.mobile FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.status ='Verified' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id = $this->softid")->result_array();
    }
    public function getOnsiteDeliveredOrders()
    {
        return  $this->db->query("SELECT onsiteorders.status,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicetime,onsiteorders.servicetype,customers.landmark,customers.address,customers.custname,customers.mobile FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.status ='Delivered' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id = $this->softid")->result_array();
    }
    public function getCancelledLists()
    {
        return  $this->db->query("SELECT onsiteorders.status,onsiteorders.onsiteorderno as orderno,onsiteorders.orderform,onsiteorders.ordertype,onsiteorders.servicedate,onsiteorders.servicetime,onsiteorders.servicetype,customers.landmark,customers.address,customers.custname,customers.mobile FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.status ='Cancelled' AND customers.worktype = 'ONSITE' AND onsiteorders.soft_id = $this->softid")->result_array();
    }
    public function getReceiptsDataById($id)
    {
        return  $this->db->where('orderno', $id)->get('onsitereceipts')->row_array();
    }
    public function getOrderDtlsForTechi($id)
    {
        return   $this->db->query("SELECT onsiteorders.item,onsiteorders.cust_id,customers.custname FROM onsiteorders INNER JOIN customers on customers.cust_id = onsiteorders.cust_id WHERE onsiteorders.soft_id = $this->softid AND onsiteorders.onsiteorderno = '$id' ")->row_array();
    }
    public function getIssuedItemsTechi($id)
    {
        // return   $this->db->query("SELECT issue_item.price,issue_item.issued_item,issue_item.serial_number,issue_item.warranty_status, issue_item.orderno,brands.brandname,models.modelname,branditems.brand_id FROM issue_item INNER JOIN branditems on branditems.branditem_id = issue_item.branditem_id INNER JOIN brands on brands.brand_id = branditems.brand_id INNER JOIN models on models.model_id = issue_item.model WHERE issue_item.orderno = '$id' AND  issue_item.soft_id = $this->softid ")->result_array();
        return   $this->db->where('orderno', $id)->get('issue_item')->result_array();
    }
    public function getAlottedOrdersForTechi($id)
    {
        return  $this->db->query("SELECT onsitecomplaints.status,onsitecomplaints.remarks,onsitecomplaints.amount,onsitecomplaints.repairby_technid,onsitecomplaints.complaint_id,onsitecomplaints.prob_id,problems.probname FROM onsitecomplaints INNER JOIN problems on problems.prob_id = onsitecomplaints.prob_id WHERE onsitecomplaints.soft_id = $this->softid AND onsitecomplaints.orderno = '$id' ORDER BY  onsitecomplaints.complaint_id ASC ")->result_array();
    }
    public function getAlottedOrdersForVerify($id)
    {
        $alocate['remarks_sts'] =  $this->db->query("SELECT IFNULL(complaints.remarks,complaints.remarks) as remarks,allocatetechinician.status,allocatetechinician.rough_estimate,allocatetechinician.remarks as tech_remarks FROM complaints INNER JOIN allocatetechinician on allocatetechinician.orderno = complaints.orderno WHERE complaints.orderno = '$id' AND complaints.remarks != 'NULL'  AND complaints.remarks != ''  AND complaints.soft_id = $this->softid")->row_array();

        $alocate['complaints'] =  $this->db->query("SELECT technicians.tech_name,complaints.status,complaints.amount,complaints.complaint_id,complaints.prob_id,problems.probname,complaints.repairby_technid,complaints.finisheddate,complaints.remarks,complaints.verified FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id   INNER JOIN technicians on technicians.techn_id = complaints.repairby_technid  WHERE complaints.orderno = '$id' AND  complaints.soft_id = $this->softid")->result_array();
        return   $alocate;
    }
    public function getDlryOrdrs($id)
    {
        $dlry = [];
        $dlry['cmplnt'] = $this->db->query("SELECT complaints.prob_id,complaints.status,complaints.amount,complaints.remarks,problems.probname FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id WHERE complaints.soft_id = $this->softid AND complaints.orderno = '$id' ")->result_array();

        $dlry['spare'] = $this->db->query("SELECT issue_item.issued_item,issue_item.price FROM issue_item WHERE orderno = '$id' AND soft_id = $this->softid")->result_array();
        return  $dlry;
    }
    public function updatevefificationstatus($sts, $ord_num)
    {
        $this->db->set('verified', $sts)->set('verifiedby', $this->lgid)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function updatetechistatus($sts, $ord_num, $order_number)
    {

        $this->db->set('status', $sts)->set('finisheddate', date('Y-m-d'))->set('repairby_technid', $this->lgid)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');

        $this->db->set('validate_date', date('Y-m-d'))->where('orderno', $order_number)->where('soft_id', $this->softid)->update(' allocatetechinician');

        return $this->db->affected_rows();
    }
    public function updatetechiectimate($sts, $ord_num)
    {

        $this->db->set('amount', $sts)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function updatetechiremarks($sts, $ord_num)
    {
        $this->db->set('remarks', $sts)->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        return $this->db->affected_rows();
    }
    public function addNewTechiRemarkz($data, $id)
    {
        $this->db->set('remarks', $data)->where('complaint_id', $id)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function finishtechiworks($ord_num)
    {
        $this->db->set('finished_date', date('Y-m-d'))->set('status', "Finished")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');

        $this->db->set('finished_date', date('Y-m-d'))->set('status', "Finished")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('orders');
        return $this->db->affected_rows();
    }
    public function getUserDetails($ordnum)
    {
        $type =  $this->db->query("SELECT customers.ordertype,orders.cust_id FROM orders INNER JOIN customers on customers.cust_id = orders.cust_id WHERE orders.orderno = '$ordnum' AND orders.soft_id = $this->softid")->row_array();
        return $type['ordertype'];
    }
    public function getCallMAxID()
    {
        return  $this->db->query("SELECT IFNULL(max(`call_id` + 1),1)  as ord_id FROM callstatus")->row_array();
    }
    public function recheckworks($ord_num)
    {
        $this->db->set('status', "Recheck")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status', "Recheck")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('orders');
        return $this->db->affected_rows();
    }
    public function getMaxVerifiedId()
    {
        return  $this->db->query("SELECT IFNULL(max(`orderverifyId` + 1),1)  as ord_id FROM  orderverify")->row_array();
    }
    public function getIssuedItems()
    {
        return  $this->db->query("SELECT * FROM `issue_item_name` WHERE `soft_id` = $this->softid")->result_array();
    }
    public function getissueditemname($id)
    {
        return   $this->db->query("SELECT `issued_item` FROM issue_item WHERE issued_item LIKE '%$id%' AND soft_id = $this->softid")->result_array();
    }
    public function getDeliveryCharge($id)
    {
        return   $this->db->query("SELECT `serviceamt`,`otheramt`,`totalamt`,`outsourceamt`,`tallyamt` FROM orderverify WHERE orderno = '$id' ")->row_array();
    }
    public function getMaxDeliveryId()
    {
        return  $this->db->query("SELECT IFNULL(max(`deliveryId` + 1),1)  as ord_id FROM  orderdelivery")->row_array();
    }
    public function getReceiptNo()
    {
        $r =   $this->db->query("SELECT IFNULL(max(`receiptno` + 1),1)  as ord_id FROM  onsitereceipts")->row_array();
        return $r['ord_id'];
    }
    public function getMaxOfRprtId()
    {
        $r =   $this->db->query("SELECT IFNULL(max(`servcallsId` + 1),1)  as ord_id FROM  servicecallsentry")->row_array();
        return $r['ord_id'];
    }
    public function getMaxOfReceiptId()
    {
        $r =   $this->db->query("SELECT IFNULL(max(`receipt_id` + 1),1)  as ord_id FROM  onsitereceipts")->row_array();
        return $r['ord_id'];
    }
    public function getOnsiteRepTechi($ordnum)
    {
        $id =  $this->db->select('techn_id')->where('orderno', $ordnum)->get('onsiteallocatetechinician')->row_array();
        return $id['techn_id'];
    }
    public function getCallSts($ordnum)
    {
        $mchne = [];
        $mchne['servicecall'] =  $this->db->select('attended_date,callstartat,callcompletedat,modelno,serialno,mechine_status,eng_analysis,mechine_status')->where('orderno', $ordnum)->get('servicecallsentry')->row_array();

        $mchne['reciept'] =  $this->db->select('bal_amt,receiptno,receipt_date,incured_amt,service_amt,spare_amt,tallyamt,net_amt,paid_amt')->where('orderno', $ordnum)->get('onsitereceipts')->row_array();
        return $mchne;
    }
    public function getComplaintsToPrint($id)

    {
        return  $this->db->query("SELECT onsitecomplaints.prob_id,problems.probname FROM onsitecomplaints INNER JOIN problems ON problems.prob_id = onsitecomplaints.prob_id WHERE  onsitecomplaints.orderno = '$id'  ")->result_array();
    }
    public function getServicedata($id)
    {
        return   $this->db->select('receiptno,paid_amt as net_amt,receipt_date')->where('orderno', $id)->get('onsitereceipts')->row_array();
    }
}
