<!-- app-content-->
<!--Select2 css -->
<link href="<?= base_url() ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard_theme_dots.css" rel="stylesheet">
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">New Onsite Order</span></h1>

            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        <!-- <h3 class="card-title">Form Wizard</h3> -->
                    </div>
                    <div class="card-options">
                        <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <span class="fe fe-more-horizontal fs-20"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                            <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                            <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                            <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                            <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div id="smartwizard-3" class="sw-main sw-theme-dots">
                        <ul class="nav nav-tabs step-anchor" id="wizrd_z">
                            <li class="nav-item active " id="new"><a href="#step-10" class="nav-link">New Order</a></li>
                            <li class="nav-item done " id="cmp"><a href="#step-11" class="nav-link">Complaint</a></li>
                            <!-- <li class="nav-item done"><a href="#step-12" class="nav-link">End</a></li> -->
                        </ul>
                        <div class="sw-container tab-content" style="min-height: 223.375px;">
                            <div id="step-10" class="tab-pane step-content" style="display: block;">
                                <form id="cus_frm">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="hidden" name="updateId" value="<?= isset($ecnryptedid) ? $ecnryptedid : ''; ?>">
                                            Order NO:<input type="text" readonly="" name="order_no" value="<?php if ($ecnryptedid) {
                                                                                                                echo  $ecnryptedid;
                                                                                                            } else {
                                                                                                                echo $order_no;
                                                                                                            }  ?>" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            Order Date:<input name="ord_date" type="date" value="<?= date('Y-m-d') ?>" class="form-control">
                                        </div>


                                        <div class="col-md-6">Order From:<input type="text" name="ord_from" readonly="" value="ONSITE" class="form-control"></div>
                                        <div class="col-md-5">Ref No:<input name="ref_no" id="ref_no" type="text" class="form-control"></div>
                                        <div class="col-md-1"><br>
                                            <button class="btn btn-info" id="find">Find</button>
                                        </div>
                                        <br><br><br><br>
                                        <div class="col-md-12">Order Type:
                                            &nbsp&nbsp&nbsp &nbsp End User
                                            <input name="order_type" class="end_user" value="End User" checked="true" type="checkbox"> &nbsp&nbsp&nbsp
                                            Dealer
                                            <input name="order_type" class="dealer" value="Dealer" type="checkbox"></div> <br><br>
                                        <div class="col-md-8" id="cus_div">
                                            Customer Name:
                                            <select name="customer_id" class="form-control select2-show-search usr_dtls" style="width: 100%;">
                                                <option value=""></option>
                                                <!-- <?php if ($customer) {
                                                            foreach ($customer as $key => $cus_val) { ?>
                                                        <option value="<?= $cus_val['cust_id']; ?>"><?= $cus_val['custname']; ?>-<?= $cus_val['mobile']; ?></option>
                                                <?php   }
                                                        } ?> -->
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <br>
                                            <button id="new_customer" class="btn btn-info">New Customer</button>
                                        </div>

                                        <div class="col-md-6">
                                            Address:

                                            <textarea name="address" class="form-control" id="addrs"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            Phone:
                                            <input name="phone" class="form-control" type="text" id="Phone">
                                        </div>
                                        <div class="col-md-6">
                                            Mobile:
                                            <input name="mobile" class="form-control" type="text" id="Mobile">
                                        </div>
                                        <div class="col-md-6">
                                            Email:
                                            <input name="email" class="form-control" type="text" id="Email">
                                        </div>
                                        <div class="col-md-6">

                                            Location:
                                            <input name="location" class="form-control" type="text" id="Location">
                                        </div>
                                        <div class="col-md-6">
                                            Land Mark:
                                            <input name="landmark" class="form-control" type="text" id="landmark">
                                        </div>
                                        <div class="col-md-6">
                                            <br> Item:
                                            Laptop<input name="item" id="items" value="Laptop" type="checkbox" class="lap itm">&nbsp &nbsp &nbsp &nbspDesktop<input id="items" name="item" value="Desktop" type="checkbox" class="desk itm">&nbsp &nbsp &nbsp &nbsp Gadget<input id="items" value="Gadget" name="item" type="checkbox" class="gad itm">
                                        </div>

                                        <!-- <div class="col-md-6">
                                            Brand:
                                            <select name="brand_id" class="form-control select2-show-search " id="brand">
                                                <option value=""></option>

                                            </select>
                                        </div> -->
                                        <!-- <div class="col-md-6">
                                            Model:
                                            <select name="model_id" class="form-control select2-show-search" id="mdl">

                                            </select>
                                        </div> -->
                                        <!-- <div class="col-md-6">

                                            Serial Number:
                                            <input name="serial_num" id="serial_numberz" type="text" class="form-control">
                                        </div> -->
                                        <div class="col-md-6"> <br>
                                            Service type:

                                            &nbsp &nbsp &nbsp &nbsp AMC:
                                            <input name="sts" id="sts" value="AMC" class="amc s_type" type="checkbox">
                                            &nbsp &nbsp &nbsp &nbsp On Call:
                                            <input name="sts" id="sts" value="On Call" class="war s_type" type="checkbox">
                                            &nbsp &nbsp &nbsp &nbsp Warranty:
                                            <input name="sts" id="sts" value="Warranty" class="out s_type" type="checkbox">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                Date Of Service:<input value="<?= date('Y-m-d'); ?>" type="date" name="aproxdate" class="form-control aproxdate">

                                            </div>
                                            <div class="col-md-12">

                                                Approx.Time:<input value="<?= date('H:m'); ?>" type="time" name="aproxdtime" class="form-control aproxdtime">
                                            </div>
                                        </div>

                                        <div class="col-md-6 ">
                                            Remark:
                                            <textarea name="cmp_remarks" class="form-control pull-right"></textarea>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div id="step-11" class="tab-pane step-content" style="display: none;">
                                <form id="cmplnt_frm">
                                    <input type="hidden" name="updateId" value="<?= isset($ecnryptedid) ? $ecnryptedid : ''; ?>">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div id="resp_divz"></div>

                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header bg-primary">
                                                        <center>Complaints/Problems</center>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="prb_card">

                                                        </div>
                                                    </div>
                                                    <div class="card-footer" style="height: 181px;">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    Problem:
                                                                    <select class="form-control select2-show-search problemz" style="width:100%">

                                                                        <?php if ($problems) {
                                                                            foreach ($problems as $key => $grp_val) { ?>
                                                                                <option data-prob_name=" <?= $grp_val['probname']; ?>" value="<?= $grp_val['prob_id']; ?>"><?= $grp_val['probname']; ?></option>
                                                                        <?php }
                                                                        } ?>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <br>
                                                                    <button style="" id="prob_btn" class="btn btn-primary btn-block">Add</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-12" class="tab-pane step-content" style="display: none;">
                                <div class="checkbox">
                                    <div class="custom-checkbox custom-control">
                                        <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox2">
                                        <label for="checkbox2" class="custom-control-label">I agree terms &amp; Conditions</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row table-responsive">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>
                                        Sl No
                                    </th>
                                    <th>
                                        Order No
                                    </th>
                                    <th>
                                        Order Date
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Service Date
                                    </th>
                                    <th>
                                        Service Time
                                    </th>
                                    <!-- <th>
                                        Serial No
                                    </th> -->
                                    <th>
                                        Service type
                                    </th>
                                    <th>
                                        Work Status
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script> -->
    <!--Select2 js -->




    <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
    <script>
        $(".btn_prnt").click(function(e) {
            var resp = '';

            $.ajax({
                url: '<?= base_url() ?>onsite/printorder',
                method: 'post',
                data: {
                    resp: resp,
                },
                success: function(response) {
                    Popup(response);
                }
            });
        });
    </script>
    <script>
        $('.dealer').click(function() {
            var html = '';
            if ($('.end_user').prop('checked') == true) {
                var _tpe = 'Dealer';
                $.ajax({
                    url: '<?= base_url() ?>onsite/getdelaerlikecus',
                    method: 'get',
                    data: {
                        _tpe: _tpe,
                    },
                    dataType: 'json',
                    success: function(response) {
                        html += '<option value = ""  ></option>';
                        $.each(response, function(cus_k, cus_val) {
                            html += '<option value = "' + cus_val.cust_id + '"  >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                        });
                        $('.usr_dtls ').html(html);
                    }

                });


                $('.end_user').prop('checked', false);
                $(this).prop('checked', true);

            } else {

                $(this).prop('checked', true);
            }
        });
    </script>

    <!-- <script>
        $('.end_user').click(function() {
            if ($('.dealer').prop('checked') == true) {
                $('.dealer').prop('checked', false);
            } else {
                $(this).prop('checked', true);
            }
        });
    </script> -->

    <script>
        $('.end_user').click(function() {
            var html = '';
            if ($('.dealer').prop('checked') == true) {
                var _tpe = 'End_user';
                $.ajax({
                    url: '<?= base_url() ?>onsite/getdelaerlikecus',
                    method: 'get',
                    data: {
                        _tpe: _tpe,
                    },
                    dataType: 'json',
                    success: function(response) {
                        html += '<option value = ""  ></option>';
                        $.each(response, function(cus_k, cus_val) {
                            html += '<option value = "' + cus_val.cust_id + '"  >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                        });
                        $('.usr_dtls ').html(html);
                    }

                });
                $('.dealer').prop('checked', false);
            } else {
                $(this).prop('checked', true);
            }
        });
    </script>

    <script>
        $('.lap').click(function() {
            $(this).prop('checked', true);


            $('.desk').prop('checked', false);
            $('.gad').prop('checked', false);
        });
        $('.desk').click(function() {
            $(this).prop('checked', true);


            $('.lap').prop('checked', false);
            $('.gad').prop('checked', false);
        });
        $('.gad').click(function() {
            $(this).prop('checked', true);


            $('.lap').prop('checked', false);
            $('.desk').prop('checked', false);
        });

        $('.amc').click(function() {
            $(this).prop('checked', true);
            $('.war').prop('checked', false);
            $('.out').prop('checked', false);
        });
        $('.war').click(function() {
            $(this).prop('checked', true);
            $('.amc').prop('checked', false);
            $('.out').prop('checked', false);
        });
        $('.out').click(function() {
            $(this).prop('checked', true);
            $('.amc').prop('checked', false);
            $('.war').prop('checked', false);
        });
    </script>

    <!-- user details   -->
    <script>
        // get customers on page load
        var ld_html = '';
        var _tpe = 'enduser';
        $.ajax({
            url: '<?= base_url() ?>onsite/getdelaerlikecus',
            method: 'get',
            data: {
                _tpe: _tpe,
            },
            dataType: 'json',
            success: function(response) {
                ld_html += '<option value = ""  ></option>';
                $.each(response, function(cus_k, cus_val) {
                    ld_html += '<option value = "' + cus_val.cust_id + '"  >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                });
                $('.usr_dtls ').html(ld_html);
            }

        });




        $(function() {
            // problems adding
            $("#prob_btn").click(function(e) {
                e.preventDefault();
                var _prid = $('.problemz option:selected').val();
                var _prname = $('.problemz option:selected').data('prob_name');
                // alert(_prid);
                // alert(_prname);
                $('.prb_card').append('<div class="p_' + _prid + '"><input type="text" name="" value="' + _prname + '" class="form-control prb_cls"><input type="hidden" name="prob[]" value="' + _prid + '" class="form-control"><button data-prb_delete="' + _prid + '" class="btn btn-danger dlt_prb">Remove</button><br>');
            });


            $(document).on('click', '.dlt_prb', function(e) {
                e.preventDefault();
                var _btnid = $(this).data('prb_delete');
                $('.p_' + _btnid).remove();
            });

            // priliminary button ckick
            var _nmbr = 1;
            $(".btn_priliminary").click(function(e) {
                e.preventDefault();
                var html = '';
                var _gpid = $('.groupz  option:selected').val();

                var _gpname = $('.groupz  option:selected').data('gp_nae');

                var _mdlId = $('.grp_item  option:selected').val();

                var _mdlname = $('.grp_item  option:selected').data('it_name');

                var _pdiv = $('#p_body').find('.' + _gpid + '');
                var _divlngth = _pdiv.length;
                var _num = 1;
                if (_divlngth > 0) {
                    $('.' + _gpid + '').append('<div class="rmv_' + _mdlId + '"><input type="hidden" name="num_i[]" value="' + _nmbr + '"><input name="mdlz_' + _gpid + '_' + _nmbr + '"  class="form-control" type="text" value="' + _mdlname + '" data-valid="' + _mdlId + '"><button data-rmv_id="' + _mdlId + '" class="btn btn-danger btn_rmv">Remove</button><input type="hidden" name="i[]" value="' + _mdlId + '"></div><br>');
                    _nmbr++;
                } else {
                    html += '<div class="col-md-12 ' + _gpid + '">';
                    html += '<card>';
                    html += '<div class="card-header bg-warning hdr_' + _gpid + '">' + _gpname + '<button data-hdr="' + _gpid + '" class="btn btn-danger pull-right hdr_delete" style="height: 38px;margin-left: 177px;">Delete</button><input type="hidden" name="head_i[]" value="' + _gpid + '"></div>';
                    html += '</card>';
                    html += '</div>';
                    $('.p_body').append(html);
                    $('.' + _gpid + '').append('<div class="rmv_' + _mdlId + '"><input type="hidden" name="num_i[]" value="' + _nmbr + '"><input name="mdlz_' + _gpid + '_' + _nmbr + '"  class="form-control" type="text" value="' + _mdlname + '" data-valid="' + _mdlId + '"><button data-rmv_id="' + _mdlId + '" class="btn btn-danger btn_rmv">Remove</button><input type="hidden" name="i[]" value="' + _mdlId + '"></div><br>');
                    _nmbr++;
                }

                // $('.p_body').html();

            });


            $(document).on('click', '.hdr_delete', function(e) {
                e.preventDefault();
                var _btnid = $(this).data('hdr');
                $('.hdr_' + _btnid).remove();
            });

            $(document).on('click', '.btn_rmv', function(e) {
                e.preventDefault();
                var _btnid = $(this).data('rmv_id');
                $('.rmv_' + _btnid).remove();
            });

            $('.btn-success').attr('disabled', true);
            $('.btn-danger').hide();


            $(".groupz").change(function(e) {
                var _grp_val = $(this).val();
                $.ajax({
                    url: '<?= base_url() ?>onsite/getitemsbygroup',
                    method: 'get',
                    data: {
                        _grp_val: _grp_val
                    },
                    dataType: 'json',
                    success: function(response) {
                        var html = '';
                        var _sel = '';
                        if (response != '') {
                            $.each(response, function(keys, vals) {

                                html += '<option style="" data-it_name="' + vals.itemname + '" value="' + vals.item_id + '">' + vals.itemname + '</option>';
                            });
                            $('#itms_grp').html(html);
                        }
                    }
                });
            });

            $(document).on('change', '.usr_dtls', function(e) {
                var _u_id = $(this).val();
                if (_u_id != '') {
                    $.ajax({
                        url: '<?= base_url() ?>onsite/getuserbyid',
                        method: 'get',
                        data: {
                            _u_id: _u_id
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response != '') {

                                $("#addrs").text(response.address);
                                $("#Mobile").val(response.mobile);
                                $("#Phone").val(response.landline);
                                $("#Email").val(response.emailid);
                                $("#landmark").text(response.landmark);
                                $("#Location").val(response.place);
                                if (response.cus_orders != '') {
                                    var html = '';
                                    var _int = 1;
                                    $.each(response.cus_orders, function(k, val) {

                                        html += '<tr>';
                                        html += '<td>' + _int + '</td>';
                                        html += '<td>' + val.orderno + '</td>';
                                        html += '<td>' + val.order_date + '</td>';
                                        html += '<td>' + val.item + '</td>';
                                        html += '<td>' + val.servicedate + '</td>';
                                        html += '<td>' + val.servicetime + '</td>';
                                        html += '<td>' + val.servicetype + '</td>';
                                        // html += '<td>' + val.warranty + '</td>';
                                        html += '<td>' + val.status + '</td>';
                                        html += '</tr>';
                                        _int++;
                                    });

                                    $('table tbody').html(html);
                                }
                            }
                        }
                    });
                }
            });

            // $(document).on('click', '#items', function(e) {
            //     var _ite_val = $(this).val();
            //     if (_ite_val != '') {
            //         $.ajax({
            //             url: '<?= base_url() ?>onsite/getbrandbyitems',
            //             method: 'get',
            //             data: {
            //                 _ite_val: _ite_val
            //             },
            //             dataType: 'json',
            //             success: function(response) {
            //                 var html = '';
            //                 if (response != '') {
            //                     $.each(response, function(keys, vals) {
            //                         html += '<option value="' + vals.branditem_id + '">' + vals.brandname + '</option>';
            //                     });
            //                     $('#brand').html(html);
            //                 }
            //             }
            //         });
            //     }
            // });



            $(document).on('change', '#brand', function(e) {
                var _brand_id = $(this).val();
                if (_brand_id != '') {
                    $.ajax({
                        url: '<?= base_url() ?>onsite/getmodelbyid',
                        method: 'get',
                        data: {
                            _brand_id: _brand_id
                        },
                        dataType: 'json',
                        success: function(response) {
                            var html = '';
                            if (response != '') {
                                $.each(response, function(keys, vals) {
                                    html += '<option value="' + vals.model_id + '">' + vals.modelname + '</option>';
                                });
                                $('#mdl').html(html);
                            }
                        }
                    });
                }
            });
            // form submitiion
            $('.btn-success').click(function(e) {
                e.preventDefault();
                var _form1 = $('#cus_frm').serialize();
                var _form2 = $('#cmplnt_frm').serialize();
                // var _form1 = new FormData($("#cus_frm")[0]);
                var _item = $('.itm:checkbox:checked').val();
                var _s_type = $('.s_type:checkbox:checked').val();
                var _aproxdate = $('.aproxdate').val();
                var _aproxdtime = $('.aproxdtime').val();
                var _prb_cls = $('.prb_cls').val();
                var _usr_dtls = $('.usr_dtls ').val();

                if (_item == undefined) {
                    alert('Select Item');
                    // return FALSE;
                } else if (_usr_dtls == '') {
                    alert('Select User');
                    // return FALSE;
                } else if (_s_type == undefined) {
                    alert('Select Service Type');
                    // return FALSE;
                } else if (_aproxdate == '') {
                    alert('Select Date');
                    //  return FALSE;
                } else if (_aproxdtime == '') {
                    alert('Select Time');
                    // return FALSE;
                } else if (_prb_cls == undefined) {
                    alert('Select Problem');
                    //  return FALSE;
                } else {
                    $.ajax({
                        url: '<?= base_url() ?>onsite/insertinhouse',
                        method: 'post',
                        data: $('#cus_frm').serialize(),
                        // data: $('#cmplnt_frm').serialize(),
                        dataType: 'json',
                        success: function(resp) {
                            if (resp != '') {
                                $("#resp_divz").html('<input type="hidden" name="resp_ord_num" value="' + resp + '" class="form-control">');
                                $.ajax({
                                    url: '<?= base_url() ?>onsite/inhouse_cmplnt',
                                    method: 'post',
                                    data: $('#cmplnt_frm').serialize(),

                                    dataType: 'json',
                                    success: function(resp) {

                                        if (resp != '') {
                                            if (confirm('Do You want to print order?')) {
                                                $.ajax({
                                                    url: '<?= base_url() ?>onsite/print_ordrs',
                                                    method: 'post',
                                                    data: {
                                                        resp: resp,
                                                    },

                                                    success: function(response) {
                                                        Popup(response);
                                                        location.response(true);
                                                    }
                                                });
                                            } else {
                                                alert('Success');
                                                window.location.href = "<?= base_url() ?>onsite/allocation";
                                            }
                                        } else {
                                            alert('Success');
                                            location.reload(true);
                                        }

                                    }
                                });
                            }
                        }
                    });
                }
            });
            $('.sw-btn-next').click(function(e) {
                e.preventDefault();
                $('.btn-success').attr('disabled', false);
            });

            $('.sw-btn-prev').click(function(e) {
                e.preventDefault();
                $('.btn-success').attr('disabled', true);
            });

        });
    </script>
    <?php if ($ecnryptedid) { ?>
        <script>
            $(function() {

                // e.preventDefault();
                var _ref_id = "<?= isset($ecnryptedid) ? $ecnryptedid : ''; ?>";
                if (_ref_id != '') {
                    $.ajax({
                        url: '<?= base_url() ?>onsite/getcustdatabyrefno',
                        method: 'get',
                        data: {
                            _ref_id: _ref_id
                        },
                        dataType: 'json',
                        success: function(response) {

                            var html = '';

                            if (response != '') {
                                console.log(response);
                                var _stz = response.order_dtls.servicetype;

                                var s_replaced = _stz.split(' ').join('_');

                                if (s_replaced == "On_Call") {
                                    $('.out').prop('checked', false);
                                    $('.war').prop('checked', true);
                                    $('.amc').prop('checked', false);
                                } else if (s_replaced == 'AMC') {
                                    $('.out').prop('checked', false);
                                    $('.amc').prop('checked', true);
                                    $('.war').prop('checked', false);
                                } else {
                                    $('.out').prop('checked', true);
                                    $('.amc').prop('checked', false);
                                    $('.war').prop('checked', false);
                                }

                                var _sdate = response.servicedatex;
                                var _stime = response.servicetimex;

                                $('.aproxdate').val(_sdate);
                                $('.aproxdtime').val(_stime);

                                // var ordertypez = response.ordertype.split(' ').join('_');
                                var replaced = response.cus_data.ordertype.split(' ').join('_');
                                var cust_id = response.cus_data.cust_id;

                                if (replaced == 'End_User') {
                                    $('.dealer').prop('checked', false);
                                    $('.end_user').prop('checked', true);
                                } else {
                                    $('.dealer').prop('checked', true);
                                    $('.end_user').prop('checked', false);
                                }
                                // $('#cus_div').remove();


                                $.each(response.complaints, function(cmp_val, cmp_data) {

                                    $('.prb_card').append('<div class="p_' + cmp_data.prob_id + '"><input type="text" name="" value="' + cmp_data.probname + '" class="form-control prb_cls"><input type="hidden" name="prob[]" value="' + cmp_data.prob_id + '" class="form-control"><button data-prb_delete="' + cmp_data.prob_id + '" class="btn btn-danger dlt_prb">Remove</button><br>');
                                });

                                var _sel = '';
                                // html += '<select class = "form-control select2-show-search usr_dtls" name="customer_id">';
                                $.each(response.allcustomers, function(cus_k, cus_val) {

                                    if (cus_val.cust_id == cust_id) {
                                        _sel = 'selected';
                                    } else {
                                        _sel = '';
                                    }
                                    html += '<option value = "' + cus_val.cust_id + '" ' + _sel + ' >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                                });
                                html += '</select>';
                                $(".usr_dtls ").html(html);

                                $("#addrs").text(response.cus_data.address);
                                $("#Mobile").val(response.cus_data.mobile);
                                $("#Phone").val(response.cus_data.landline);
                                $("#Email").val(response.cus_data.emailid);
                                $("#serial_numberz").val(response.order_dtls.serialno);
                                $("#Location").val(response.cus_data.place);


                                var _item = response.order_dtls.item;
                                if (_item == 'Laptop') {

                                    $('.lap').prop('checked', true);
                                    $('.desk').prop('checked', false);
                                    $('.gad').prop('checked', false);

                                    if ($('.lap').prop('checked') == true) {
                                        setotherdataselected(response.order_dtls, _item);
                                    }

                                } else if (_item == 'Desktop') {
                                    $('.lap').prop('checked', false);
                                    $('.desk').prop('checked', true);
                                    $('.gad').prop('checked', false);

                                    if ($('.desk').prop('checked') == true) {
                                        setotherdataselected(response.order_dtls, _item);
                                    }
                                } else {
                                    $('.lap').prop('checked', false);
                                    $('.desk').prop('checked', false);
                                    $('.gad').prop('checked', true);

                                    if ($('.gad').prop('checked') == true) {
                                        setotherdataselected(response.order_dtls, _item);
                                    }
                                }





                                var htmlz = '';
                                var _int = 1;
                                $.each(response.cus_address.cus_orders, function(k, val) {

                                    htmlz += '<tr>';
                                    htmlz += '<td>' + _int + '</td>';
                                    htmlz += '<td>' + val.orderno + '</td>';
                                    htmlz += '<td>' + val.order_date + '</td>';
                                    htmlz += '<td>' + val.item + '</td>';
                                    htmlz += '<td>' + val.servicedate + '</td>';
                                    htmlz += '<td>' + val.servicetime + '</td>';
                                    htmlz += '<td>' + val.servicetype + '</td>';

                                    htmlz += '<td>' + val.status + '</td>';
                                    htmlz += '</tr>';
                                    _int++;
                                });

                                $('table tbody').html(htmlz);


                            }
                        }
                    });
                } else {
                    alert('Reference number Not Found');
                }
            });
        </script>

    <?php } ?>
    <script>
        // get data by refrence no IN01-18/2
        $("#find").click(function(e) {
            e.preventDefault();
            var _ref_id = $("#ref_no").val();
            if (_ref_id != '') {
                $.ajax({
                    url: '<?= base_url() ?>onsite/getcustdatabyrefno',
                    method: 'get',
                    data: {
                        _ref_id: _ref_id
                    },
                    dataType: 'json',
                    success: function(response) {

                        var html = '';

                        if (response != '') {
                            console.log(response);
                            var _stz = response.order_dtls.servicetype;

                            var s_replaced = _stz.split(' ').join('_');

                            if (s_replaced == "On_Call") {
                                $('.out').prop('checked', false);
                                $('.war').prop('checked', true);
                                $('.amc').prop('checked', false);
                            } else if (s_replaced == 'AMC') {
                                $('.out').prop('checked', false);
                                $('.amc').prop('checked', true);
                                $('.war').prop('checked', false);
                            } else {
                                $('.out').prop('checked', true);
                                $('.amc').prop('checked', false);
                                $('.war').prop('checked', false);
                            }
                            // var ordertypez = response.ordertype.split(' ').join('_');
                            var replaced = response.cus_data.ordertype.split(' ').join('_');
                            var cust_id = response.cus_data.cust_id;

                            if (replaced == 'End_User') {
                                $('.dealer').prop('checked', false);
                                $('.end_user').prop('checked', true);
                            } else {
                                $('.dealer').prop('checked', true);
                                $('.end_user').prop('checked', false);
                            }
                            // $('#cus_div').remove();


                            $.each(response.complaints, function(cmp_val, cmp_data) {

                                $('.prb_card').append('<div class="p_' + cmp_data.prob_id + '"><input type="text" name="" value="' + cmp_data.probname + '" class="form-control prb_cls"><input type="hidden" name="prob[]" value="' + cmp_data.prob_id + '" class="form-control"><button data-prb_delete="' + cmp_data.prob_id + '" class="btn btn-danger dlt_prb">Remove</button><br>');
                            });

                            var _sel = '';
                            // html += '<select class = "form-control select2-show-search usr_dtls" name="customer_id">';
                            $.each(response.allcustomers, function(cus_k, cus_val) {

                                if (cus_val.cust_id == cust_id) {
                                    _sel = 'selected';
                                } else {
                                    _sel = '';
                                }
                                html += '<option value = "' + cus_val.cust_id + '" ' + _sel + ' >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                            });
                            html += '</select>';
                            $(".usr_dtls ").html(html);

                            $("#addrs").text(response.cus_data.address);
                            $("#Mobile").val(response.cus_data.mobile);
                            $("#Phone").val(response.cus_data.landline);
                            $("#Email").val(response.cus_data.emailid);
                            $("#serial_numberz").val(response.order_dtls.serialno);
                            $("#Location").val(response.cus_data.place);


                            var _item = response.order_dtls.item;
                            if (_item == 'Laptop') {

                                $('.lap').prop('checked', true);
                                $('.desk').prop('checked', false);
                                $('.gad').prop('checked', false);

                                if ($('.lap').prop('checked') == true) {
                                    setotherdataselected(response.order_dtls, _item);
                                }

                            } else if (_item == 'Desktop') {
                                $('.lap').prop('checked', false);
                                $('.desk').prop('checked', true);
                                $('.gad').prop('checked', false);

                                if ($('.desk').prop('checked') == true) {
                                    setotherdataselected(response.order_dtls, _item);
                                }
                            } else {
                                $('.lap').prop('checked', false);
                                $('.desk').prop('checked', false);
                                $('.gad').prop('checked', true);

                                if ($('.gad').prop('checked') == true) {
                                    setotherdataselected(response.order_dtls, _item);
                                }
                            }

                            var _sdate = response.servicedatex;
                            var _stime = response.servicetimex;
                            $('.aproxdate').val(_sdate);
                            $('.aproxdtime').val(_stime);



                            var htmlz = '';
                            var _int = 1;
                            $.each(response.cus_address.cus_orders, function(k, val) {

                                htmlz += '<tr>';
                                htmlz += '<td>' + _int + '</td>';
                                htmlz += '<td>' + val.orderno + '</td>';
                                htmlz += '<td>' + val.order_date + '</td>';
                                htmlz += '<td>' + val.item + '</td>';
                                htmlz += '<td>' + val.servicedate + '</td>';
                                htmlz += '<td>' + val.servicetime + '</td>';
                                htmlz += '<td>' + val.servicetype + '</td>';

                                htmlz += '<td>' + val.status + '</td>';
                                htmlz += '</tr>';
                                _int++;
                            });

                            $('table tbody').html(htmlz);


                        }
                    }
                });
            } else {
                alert('Reference number Not Found');
            }
        });

        // function setotherdataselected(order_dtls, _item) {

        //     _ite_val = _item;
        //     _brand_id = order_dtls.branditem_id;
        //     _model_id = order_dtls.model_id;
        //     $.ajax({
        //         url: '<?= base_url() ?>onsite/getbrandbyitems',
        //         method: 'get',
        //         data: {
        //             _ite_val: _ite_val
        //         },
        //         dataType: 'json',
        //         success: function(response) {
        //             var html = '';
        //             var _sel = '';
        //             if (response != '') {
        //                 $.each(response, function(keys, vals) {
        //                     if (vals.branditem_id == order_dtls.branditem_id) {
        //                         _sel = 'selected';
        //                     } else {
        //                         _sel = '';
        //                     }
        //                     html += '<option value="' + vals.branditem_id + '" ' + _sel + ' >' + vals.brandname + '</option>';
        //                 });
        //                 $('#brand').html(html);
        //             }
        //         }
        //     });
        //     // model
        //     $.ajax({
        //         url: '<?= base_url() ?>onsite/getmodelbyid',
        //         method: 'get',
        //         data: {
        //             _brand_id: _brand_id
        //         },
        //         dataType: 'json',
        //         success: function(response) {
        //             var html = '';
        //             var _sel = '';
        //             if (response != '') {
        //                 $.each(response, function(keys, vals) {
        //                     if (vals.model_id == order_dtls.model_id) {
        //                         _sel = 'selected';
        //                     } else {
        //                         _sel = '';
        //                     }
        //                     html += '<option value="' + vals.model_id + '">' + vals.modelname + '</option>';
        //                 });
        //                 $('#mdl').html(html);
        //             }
        //         }
        //     });
        // }
    </script>


    <!-- new customer -->
    <script>
        $("#new_customer").click(function(e) {
            e.preventDefault();
            // ordtpe


            if ($('.end_user ').prop('checked') == true) {
                var _ctpe = 'End User';
            } else {
                var _ctpe = 'Dealer';
            }
            $(".ordtpe").html('<input type="hidden" name="_ctpe" value="' + _ctpe + '">');

            $("#myModalx").modal('show');
        });
    </script>


    <!-- Modal -->
    <div id="myModalx" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">New Customer</h4>
                </div>
                <form id="mdl_new_cus">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    Customer Name:<input type="text" name="c_nme" id="c_nme" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    Mobile No:<input type="text" name="m_no" id="m_no" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    Land Line :<input type="text" name="p_no" id="p_no" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    Address:
                                    <textarea name="adrs" id="adrs" class="form-control"></textarea>
                                </div>
                                <div class="col-md-6">
                                    Email Id:<input type="text" name="em_id" id="em_id" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    Place:
                                    <select name="locaId" id="locaId" class="form-control select2-show-search" style="width: 100%;">

                                        <?php foreach ($place as $key => $p_val) { ?>
                                            <option value="<?= $p_val['location'] ?>"><?= $p_val['location'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="ordtpe">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="new_cus_add" class="btn btn-info new_cus_addz">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>






    <script>
        $(".new_cus_addz").click(function(e) {
            e.preventDefault();

            var _c_nme = $("#c_nme").val();
            var m_no = $("#m_no").val();
            if ((_c_nme == '') || (m_no == '')) {
                alert('Name And Mobile Number Required');
            } else {
                // 
                $.ajax({
                    url: '<?= base_url() ?>onsite/insertnewcustmr',
                    method: 'POST',
                    data: $("#mdl_new_cus").serialize(),
                    dataType: 'json',
                    success: function(response) {
                        if (response != '') {
                            $("#myModalx").modal('hide');
                            alert('Success');
                            var replaced = response.split(' ').join('_');
                            var _tpe = replaced;
                            var ld_html = '';
                            var _tpe = _tpe;
                            $.ajax({
                                url: '<?= base_url() ?>onsite/getdelaerlikecus',
                                method: 'get',
                                data: {
                                    _tpe: _tpe,
                                },
                                dataType: 'json',
                                success: function(response) {
                                    ld_html += '<option value = ""  ></option>';
                                    $.each(response, function(cus_k, cus_val) {
                                        ld_html += '<option value = "' + cus_val.cust_id + '"  >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                                    });
                                    $('.usr_dtls ').html(ld_html);
                                }

                            });
                        } else {
                            alert('Failed');
                            $("#myModalx").modal('hide');
                        }
                    }
                });

            }
        });
    </script>



    <script>
        function Popup(data) {
            var base_url = $("#urlid").val();
            var frame1 = $("<iframe />");
            frame1[0].name = "frame1";
            frame1.css({
                position: "absolute",
                top: "-1000000px",
            });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ?
                frame1[0].contentWindow :
                frame1[0].contentDocument.document ?
                frame1[0].contentDocument.document :
                frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write("<html>");
            frameDoc.document.write("<head>");
            frameDoc.document.write("<title></title>");
            frameDoc.document.write(
                '<link rel="stylesheet" href="' +
                base_url +
                'templat/dist/css/adminlte.min.css">'
            );
            frameDoc.document.write("</head>");
            frameDoc.document.write("<body>");
            frameDoc.document.write(data);
            frameDoc.document.write("</body>");
            frameDoc.document.write("</html>");
            frameDoc.document.close();
            setTimeout(function() {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);

            return true;
        }
    </script>