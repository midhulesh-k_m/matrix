<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Inhouse Order Delivery</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            Technician Name:<b><?= $alloted_list['tech_name']; ?></b>
                        </div>
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row">


                            <div class="col-md-6">
                                Order NO:
                                <b><?= $ord_num; ?></b>
                            </div>
                            <div class="col-md-6">
                                Order Type:
                                <b><?= $alloted_list['ordertype']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Customer name:
                                <b><?= $alloted_list['custname']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Mobile Number:
                                <b><?= $alloted_list['mobile']; ?></b>
                            </div>
                            <div class="col-md-6">
                                Item:
                                <b><?= $alloted_list['item']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Brand: <b><?= $alloted_list['brandname']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Model: <b><?= $alloted_list['modelname']; ?></b>
                            </div>

                            <div class="col-md-6">
                                Warranty Status:
                                <b><?= $alloted_list['warranty']; ?></b>
                            </div>
                            <br><br>
                            <div class="col-md-6">
                                Serial No:
                                <b><?= $alloted_list['serialno']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Approx.Date:
                                <b><?= $alloted_list['appx_date']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Rough.Estimate:
                                <b><?= $alloted_list['rough_estimate']; ?></b>
                            </div><br><br>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <center>Repair Report</center>
                                    </div>
                                    <div class="card-body" style="">
                                        <div class="row">
                                            <div class="col-md-<?php if (isset($alloted_orders['spare'][0]['issued_item'])) {
                                                                    echo '6';
                                                                } else {
                                                                    echo '12';
                                                                } ?>">
                                                <div class="card">


                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>Problems</th>
                                                                <th>Status</th>
                                                                <th>Amount</th>
                                                                <th>Remarks</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($alloted_orders) {
                                                                foreach ($alloted_orders['cmplnt'] as $key => $k_val) { ?>
                                                                    <tr>
                                                                        <td><?= $k_val['probname']; ?></td>
                                                                        <td><?= $k_val['status']; ?></td>
                                                                        <td><?= $k_val['amount']; ?></td>
                                                                        <td><?= $k_val['remarks']; ?></td>
                                                                    </tr>

                                                                <?php } ?>


                                                            <?php } ?>
                                                            <tr></tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                            <?php if (isset($alloted_orders['spare'][0]['issued_item'])) { ?>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-primary">

                                                                <th>
                                                                    Spare Replaced
                                                                </th>
                                                                <th>
                                                                    Amount
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($alloted_orders['spare'] as $key => $k_val) { ?>
                                                                <tr>
                                                                    <td><?= $k_val['issued_item']; ?></td>

                                                                    <td><span class="sp_cost"><?= $k_val['price']; ?></span></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
                            <form method="post" id="verifiedmmodal" action="<?= base_url() ?>inhouse/adddelivery">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="col-md-12">
                                                Recieved By:<input type="text" name="recieved_by" class="form-control">
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        Mobile No:<input type="text" name="mobilenum" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Send OTP</button>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        Enter OTP:<input type="text" name="otp" class="form-control">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Check OTP</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                Remarks:
                                                <textarea name="remarks" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">

                                                <div class="card">
                                                    <div class="card-header bg-primary">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    Cost Details
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="col-md-12" style="margin-left: 100px;">
                                                                        <input name="unpaid" style="margin-right: 250px;" value="Unpaid" type="checkbox">Unpaid
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="col-md-12">
                                                                    <input type="hidden" name="ordnumb" value="<?= $ord_num; ?>">
                                                                    Service Charge:
                                                                    <input type="text" readonly="" value="<?= $charge['serviceamt']; ?>" name="servicecharge" class="form-control net">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Other Charge:
                                                                    <input type="text" readonly="" value="<?= $charge['otheramt']; ?>" class="form-control net" name="othercharge">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Tally Amount:
                                                                    <input type="text" readonly="" name="tallycharge" value="<?= $charge['tallyamt']; ?>" class="form-control net">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Spare Cost:
                                                                    <input type="text" name="sparecost" readonly="" class="form-control sp_charge net">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <b> Total</b>:
                                                                    <input type="text" name="totalamount" readonly="" value="<?= $charge['totalamt']; ?>" class="form-control grand">
                                                                </div>


                                                            </div>
                                                            <div class="col-md-6">

                                                                <div class="col-md-12">
                                                                    Paid:
                                                                    <input name="paidamount" id="paid" type="text" class="form-control">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Discount:
                                                                    <input name="discountamount" value="0" id="discount" type="text" class="form-control">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Balance:
                                                                    <input name="balanceamount" id="balance" type="text" class="form-control">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <b> Net Amount:</b>
                                                                            <input name="netamount" type="text" readonly="" class="form-control netamount">
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <br>
                                                                            <button class="btn btn-primary cal_ttl">Calculate</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div style="margin-right: -200px;" class="col-md-6 pull-right">

                                    <!-- <button type="button" class="btn btn-info ">Call</button> -->

                                    <button type="button" data-ord_num="<?= $ord_num; ?>" onclick="recheck()" id="Recheck" class="btn btn-primary Finished">SMS</button>

                                    <button type="button" data-ord_num="<?= $ord_num; ?>" onclick="finishorder()" id="Finished" class="btn btn-primary Finished">Call</button>
                                    <button class="btn btn-primary" id="verified_btn_mdl">Save</button>
                                    <button class="btn btn-primary" id="call_customer">Cancel</button>
                                    <!-- <button type="button" class="btn btn-info" onclick="return  confirm('Cancel Order ?');">Cancel</button> -->
                                </div>
                            </div>

                        </div>



                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<script>
    $(function() {
        var _spcst = 0;
        $('.sp_cost').each(function() {
            var _ccost = $(this).text();

            _spcst += parseFloat(_ccost);
        });

        $('.sp_charge').val(_spcst);
        var _gt = 0;
        $('.net').each(function() {
            var _ttzl = $(this).val();

            _gt += parseFloat(_ttzl);
        });

        $('.grand').val(_gt);


        // 

        $(".cal_ttl").click(function(e) {
            e.preventDefault();
            var _totalamt = $(".grand").val();
            var _discoutamt = $("#discount").val();
            if (_discoutamt != '') {
                var _discoutamt = _discoutamt;
            } else {
                var _discoutamt = 0;
            }
            var three = parseFloat(_totalamt) - parseFloat(_discoutamt);
            $('.netamount').val(three);
        });

    });

    $("#paid").change(function(e) {
        var _paid = $(this).val();
        var nettotals = $('.grand').val();
        var _blnce = parseFloat(nettotals) - parseFloat(_paid);
        $("#balance").val(_blnce);
    });

    $("#discount").change(function(e) {
        var _discount = $(this).val();
        var _paid = $("#paid").val();
        var nettotals = $('.grand').val();
        if (_paid != '') {

            var _blnce12 = parseFloat(nettotals) - parseFloat(_paid);

            var _blnce12 = parseFloat(_blnce12) - parseFloat(_discount);

            // alert(_blnce);
            $("#balance").val(_blnce12);
        } else {
            alert('Amount not paid');
        }
    });
</script>

<!-- Message Modal -->
<script>
    $('.btn_remarks').click(function(e) {
        var _num_ord = $(this).data('ord_num');
        $("#mdl_remark").val(_num_ord);


        $.ajax({
            url: '<?= base_url() ?>inhouse/getcmplntremarks',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    $("#rmrk_data").text(evnt.remarks);
                }
            }
        });


        $("#exampleModal3").modal('show');
    });


    $("#clct").click(function(e) {
        e.preventDefault();
        var _amtz = 0;
        $('.c_amt').each(function(k, valz) {

            var ttl = $(this).val();
            if (ttl == '') {
                ttl == 0;
            }
            _amtz += parseFloat(ttl);
        });
        $("#g_total").val(_amtz);
    });
</script>
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Add Remarks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addtechiremarkz">
                    <div class="col-md-12">
                        Remarks:
                        <textarea name="remarks_bdl" class="form-control" id="rmrk_data"></textarea>
                        <input type="hidden" name="remarks_id" id="mdl_remark">
                        <input type="hidden" name="ord_nuum" id="" value="<?= $ord_num; ?>">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Remark</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- // calling customer  -->

<script>
    $("#call_customer").click(function(e) {
        e.preventDefault();
        var _num_ord = "<?= $ord_num; ?>";
        var _mobile = "<?= $alloted_list['mobile']; ?>";
        $("#ord_num_mobile").val(_mobile);
        $("#ord_num_m").val(_num_ord);

        $.ajax({
            url: '<?= base_url() ?>inhouse/getcallingdetails',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    console.log(evnt);
                    $("#cl_date").val(evnt.date);
                    $("#subj").val(evnt.subject);
                    $("#cl_person").val(evnt.caller);
                    $("#cl_rmrk").text(evnt.reason);
                }
            }
        });

        $("#exampleModal4").modal('show');
    });
</script>


<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addcallingdetails">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_m">
                            </div>
                            <div class="col-md-3">
                                Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="call_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Mobile:
                                <input class="form-control" type="text" readonly="" name="cal_mob" id="ord_num_mobile">
                            </div>
                            <div class="col-md-3">
                                Calling Person:
                                <input class="form-control" type="text" name="cl_person" id="cl_person">
                            </div>
                            <div class="col-md-3">
                                Subject:
                                <input class="form-control" type="text" name="subj" id="subj">
                            </div>
                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="cl_rmrk"></Textarea>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Call Status</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(function() {
        var _stz = '';
        $(".ord_sts option:selected").each(function(k, valz) {
            var _c_sts = $(this).val();

            if (_c_sts === 'Allocated') {
                $(".Finished").prop('disabled', true);
            }
            if (_c_sts === 'Reparing') {
                $(".Finished").prop('disabled', true);
            }
        });

    });
</script>

<script>
    $(".verifyed").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('option:selected', this).data('verifyedby');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatevefificationstatus',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    location.reload(true);
                }
            }
        });
    });
    // amount
    $(".estimate").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $(this).data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiectimate',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // remarks
    $("#rmrks").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('#estimate').data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiremarks',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // recheck
    function recheck(e) {
        // e.preventDefault();
        var _num_ord = $('#Recheck').data('ord_num');

        if (confirm('Confirm ?')) {
            $.ajax({
                url: '<?= base_url() ?>inhouse/recheck',
                method: 'post',
                data: {

                    _num_ord: _num_ord,
                },
                dataType: 'json',
                success: function(evnt) {
                    if (evnt != '') {
                        alert("Success");
                        window.location.href = "<?= base_url() ?>inhouse/verification";
                    }
                }
            });
        }
    }
    //finished
    function finishorder(e) {
        // e.preventDefault();
        var _num_ord = $('#Finished').data('ord_num');
        var _total = $('#g_total').val();
        var _service_charge = $('#service_charge').val();
        var _outs_charge = $('#outsu_charge').val();
        var _tallyamt = $('#tally_amt').val();
        var _other = $('#othe_charge').val();
        var html = '';


        html += '<input type="hidden" name="service_charge" id="service_charge" value="' + _service_charge + '" >';
        html += ' <input type="hidden" name="outsource" id="outsource" value="' + _outs_charge + '" >';
        html += ' <input type="hidden" name="tallyamy" id="tallyamy" value="' + _tallyamt + '"  >';
        html += ' <input type="hidden" name="otherc" id="other" value="' + _other + '" >';
        $(".allother").html(html);
        $("#amtzzz").val(_total);
        $("#ord_num_mz").val(_num_ord);
        $("#exampleModal44").modal('show');
    }
</script>




<script>
    $("#verified_btn_mdl").click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '<?= base_url() ?>inhouse/adddelivery',
            method: 'post',
            data: $("#verifiedmmodal").serialize(),
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    if (confirm('Do You want to send sms?  ?')) {
                        alert('sms send');
                        window.location.href = "<?= base_url() ?>inhouse/delivery";
                    } else {
                        window.location.href = "<?= base_url() ?>inhouse/delivery";
                    }
                }
            }
        });
    })
</script>