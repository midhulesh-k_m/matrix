<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard_theme_dots.css" rel="stylesheet">

<div class="app-content toggle-content">
    <div class="side-app">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div>
                        <h3 class="card-title">Form Wizard</h3>
                    </div>
                    <div class="card-options">
                        <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <span class="fe fe-more-horizontal fs-20"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                            <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                            <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                            <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                            <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div id="smartwizard-3" class="sw-main sw-theme-dots">
                        <ul class="nav nav-tabs step-anchor">
                            <li class="nav-item active"><a href="#step-10" class="nav-link">New Order</a></li>
                            <li class="nav-item done"><a href="#step-11" class="nav-link">New User</a></li>
                            <li class="nav-item done"><a href="#step-12" class="nav-link">End</a></li>
                        </ul>
                        <div class="sw-container tab-content" style="min-height: 223.375px;">
                            <div id="step-10" class="tab-pane step-content" style="display: block;">
                                <form>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                Order NO:<input type="text" readonly="" value="<?= $order_no; ?>" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                Order Date:<input type="date" value="<?= date('Y-m-d') ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">Order From:<input type="text" readonly="" value="INHOUSE" class="form-control"></div>
                                    <div class="col-md-6">Ref No:<input type="text" class="form-control"></div>
                                    <br><br><br><br>
                                    <div class="col-md-12">Order Type:
                                        &nbsp&nbsp&nbsp &nbsp End User
                                        <input class="end_user" value="End User" checked="true" type="checkbox"> &nbsp&nbsp&nbsp
                                        Dealer
                                        <input class="dealer" value="Dealer" type="checkbox"></div> <br><br>
                                    <div class="col-md-8">
                                        Customer Name:
                                        <select class="filter-multi" style="display: none;">
                                            <option>sdfsfsdfds</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        Address:

                                        <textarea class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        Phone:
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        Mobile:
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        Email:
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        Location:
                                        <input class="form-control" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <br> Item:
                                        Laptop<input value="Laptop" type="checkbox">&nbsp &nbsp &nbsp &nbspDesktop<input value="Desktop" type="checkbox">&nbsp &nbsp &nbsp &nbsp Gadget<input value="Gadget" type="checkbox">
                                    </div>

                                    <div class="col-md-6">
                                        Brand:
                                        <select class="form-control select2-show-search ">
                                            <option>asd</option>
                                            <option>asd</option>
                                            <option>asd</option>
                                            <option>asd</option>
                                            <option>asd</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        Model:
                                        <select class="form-control">
                                            <option>asd</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">

                                        Serial Bumber:
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6"> <br>
                                        Warranty Status:

                                        &nbsp &nbsp &nbsp &nbsp AMC:
                                        <input type="checkbox">
                                        &nbsp &nbsp &nbsp &nbsp Warranty:
                                        <input type="checkbox">
                                        &nbsp &nbsp &nbsp &nbsp Without Warranty:
                                        <input type="checkbox">
                                    </div>

                                </form>
                            </div>
                            <div id="step-11" class="tab-pane step-content" style="display: none;">
                                <form>
                                    <div class="form-group">
                                        <label>User Name</label>
                                        <input type="text" class="form-control" id="inputtext" placeholder="Enter User Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail8" placeholder="Enter email address">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword9" placeholder="Password">
                                    </div>
                                    <div class="checkbox">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-3">
                                            <label for="checkbox-3" class="custom-control-label">Check me Out</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-12" class="tab-pane step-content" style="display: none;">
                                <div class="checkbox">
                                    <div class="custom-checkbox custom-control">
                                        <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox2">
                                        <label for="checkbox2" class="custom-control-label">I agree terms &amp; Conditions</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>