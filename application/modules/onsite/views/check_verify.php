<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Alloted Order</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            Technician Name:<b><?= $alloted_list['tech_name']; ?></b>
                        </div>
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row">


                            <div class="col-md-6">
                                Order NO:
                                <b><?= $ord_num; ?></b>
                            </div>
                            <div class="col-md-6">
                                Order Type:
                                <b><?= $alloted_list['ordertype']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Customer name:
                                <b><?= $alloted_list['custname']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Mobile Number:
                                <b><?= $alloted_list['mobile']; ?></b>
                            </div>
                            <div class="col-md-6">
                                Item:
                                <b><?= $alloted_list['item']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Brand: <b><?= $alloted_list['brandname']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Model: <b><?= $alloted_list['modelname']; ?></b>
                            </div>

                            <div class="col-md-6">
                                Warranty Status:
                                <b><?= $alloted_list['warranty']; ?></b>
                            </div>
                            <br><br>
                            <div class="col-md-6">
                                Serial No:
                                <b><?= $alloted_list['serialno']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Approx.Date:
                                <b><?= $alloted_list['appx_date']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Rough.Estimate:
                                <b><?= $alloted_list['rough_estimate']; ?></b>
                            </div><br><br>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <center>Repair Report</center>
                                    </div>
                                    <div class="card-body" style="">
                                        <div class="row">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>

                                                        <th>
                                                            Verified
                                                        </th>
                                                        <th>
                                                            Problem
                                                        </th>
                                                        <th>
                                                            Repaired By
                                                        </th>
                                                        <th>
                                                            Completed Date
                                                        </th>


                                                        <th>
                                                            Status
                                                        </th>
                                                        <th>
                                                            Amount
                                                        </th>
                                                        <th>
                                                            Remarks
                                                        </th>
                                                        <!-- <th>
                                                            Remarks
                                                        </th> -->

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <select class="form-control verifyed">
                                                                    <option data-verifyedby="<?= $cpm_val['complaint_id']; ?>" <?php if ($cpm_val['verified'] == 1) {
                                                                                                                                    echo 'selected';
                                                                                                                                } ?> value="1">Yes</option>
                                                                    <option data-verifyedby="<?= $cpm_val['complaint_id']; ?>" <?php if ($cpm_val['verified'] == 0) {
                                                                                                                                    echo 'selected';
                                                                                                                                } ?> value="0">No</option>
                                                                </select><br>
                                                        <?php  }
                                                        } ?>
                                                    </td>
                                                    <td><?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?> <?= $cpm_val['probname'] . '<br><br>'; ?> <?php }
                                                                                                                                                                } ?></td>


                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <?= $cpm_val['tech_name']; ?><br><br>
                                                        <?php }
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <?= $cpm_val['finisheddate']; ?><br><br>
                                                        <?php }
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <?= $cpm_val['status']; ?><br><br>
                                                        <?php }
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <span class="amts"><?= $cpm_val['amount']; ?></span><br><br>
                                                        <?php }
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <?php if ($alloted_orders) {
                                                            foreach ($alloted_orders['complaints'] as $key => $cpm_val) { ?>
                                                                <textarea readonly="" class="form-control"><?= $cpm_val['remarks']; ?></textarea><br>
                                                        <?php }
                                                        } ?>
                                                    </td>
                                                    <!-- <td>

                                                        <textarea data-ord_num="<?= $ord_num; ?>" id="rmrks" class="form-control"> <?= $alloted_orders['remarks_sts']['tech_remarks']; ?></textarea>
                                                    </td> -->
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header bg-primary">
                                        Cost Details
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Service Charge:<input value="0" type="text" placeholder="0" class="form-control c_amt" id="service_charge">
                                                </div>
                                                <div class="col-md-4">
                                                    Outsource Charge:<input value="0" type="text" placeholder="0" class="form-control c_amt" id="outsu_charge">
                                                </div>
                                                <div class="col-md-4">
                                                    Tally Amount:<input value="0" type="text" placeholder="0" class="form-control c_amt" id="tally_amt">
                                                </div>
                                                <div class="col-md-4">
                                                    Other Charge:<input value="0" type="text" placeholder="0" class="form-control c_amt" id="othe_charge">
                                                </div>
                                                <div class="col-md-4">
                                                    Grand Total:<input type="text" readonly="" placeholder="0" id="g_total" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <br>
                                                    <button id="clct" class="btn btn-info">Calculate</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">

                                <!-- <button type="button" class="btn btn-info ">Call</button> -->

                                <button type="button" data-ord_num="<?= $ord_num; ?>" onclick="recheck()" id="Recheck" class="btn btn-info Finished">Recheck</button>

                                <button type="button" data-ord_num="<?= $ord_num; ?>" onclick="finishorder()" id="Finished" class="btn btn-info Finished">Verified</button>

                                <button class="btn btn-info" id="call_customer">Cancel</button>
                                <!-- <button type="button" class="btn btn-info" onclick="return  confirm('Cancel Order ?');">Cancel</button> -->
                            </div>


                        </div>



                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<!-- Message Modal -->
<script>
    $('.btn_remarks').click(function(e) {
        var _num_ord = $(this).data('ord_num');
        $("#mdl_remark").val(_num_ord);


        $.ajax({
            url: '<?= base_url() ?>inhouse/getcmplntremarks',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    $("#rmrk_data").text(evnt.remarks);
                }
            }
        });


        $("#exampleModal3").modal('show');
    });


    $("#clct").click(function(e) {
        e.preventDefault();
        var _amtz = 0;
        $('.c_amt').each(function(k, valz) {

            var ttl = $(this).val();
            if (ttl == '') {
                ttl == 0;
            }
            _amtz += parseFloat(ttl);
        });
        $("#g_total").val(_amtz);
    });
</script>
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Add Remarks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addtechiremarkz">
                    <div class="col-md-12">
                        Remarks:
                        <textarea name="remarks_bdl" class="form-control" id="rmrk_data"></textarea>
                        <input type="hidden" name="remarks_id" id="mdl_remark">
                        <input type="hidden" name="ord_nuum" id="" value="<?= $ord_num; ?>">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Remark</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- // calling customer  -->

<script>
    $("#call_customer").click(function(e) {
        e.preventDefault();
        var _num_ord = "<?= $ord_num; ?>";
        var _mobile = "<?= $alloted_list['mobile']; ?>";
        $("#ord_num_mobile").val(_mobile);
        $("#ord_num_m").val(_num_ord);

        $.ajax({
            url: '<?= base_url() ?>inhouse/getcallingdetails',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    console.log(evnt);
                    $("#cl_date").val(evnt.date);
                    $("#subj").val(evnt.subject);
                    $("#cl_person").val(evnt.caller);
                    $("#cl_rmrk").text(evnt.reason);
                }
            }
        });

        $("#exampleModal4").modal('show');
    });
</script>


<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addcallingdetails">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_m">
                            </div>
                            <div class="col-md-3">
                                Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="call_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Mobile:
                                <input class="form-control" type="text" readonly="" name="cal_mob" id="ord_num_mobile">
                            </div>
                            <div class="col-md-3">
                                Calling Person:
                                <input class="form-control" type="text" name="cl_person" id="cl_person">
                            </div>
                            <div class="col-md-3">
                                Subject:
                                <input class="form-control" type="text" name="subj" id="subj">
                            </div>
                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="cl_rmrk"></Textarea>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Call Status</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(function() {
        var _stz = '';
        $(".ord_sts option:selected").each(function(k, valz) {
            var _c_sts = $(this).val();

            if (_c_sts === 'Allocated') {
                $(".Finished").prop('disabled', true);
            }
            if (_c_sts === 'Reparing') {
                $(".Finished").prop('disabled', true);
            }
        });

    });
</script>

<script>
    $(".verifyed").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('option:selected', this).data('verifyedby');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatevefificationstatus',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    location.reload(true);
                }
            }
        });
    });
    // amount
    $(".estimate").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $(this).data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiectimate',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // remarks
    $("#rmrks").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('#estimate').data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiremarks',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // recheck
    function recheck(e) {
        // e.preventDefault();
        var _num_ord = $('#Recheck').data('ord_num');

        if (confirm('Confirm ?')) {
            $.ajax({
                url: '<?= base_url() ?>inhouse/recheck',
                method: 'post',
                data: {

                    _num_ord: _num_ord,
                },
                dataType: 'json',
                success: function(evnt) {
                    if (evnt != '') {
                        alert("Success");
                        window.location.href = "<?= base_url() ?>inhouse/verification";
                    }
                }
            });
        }
    }
    //finished
    function finishorder(e) {
        // e.preventDefault();
        var _num_ord = $('#Finished').data('ord_num');
        var _total = $('#g_total').val();
        var _service_charge = $('#service_charge').val();
        var _outs_charge = $('#outsu_charge').val();
        var _tallyamt = $('#tally_amt').val();
        var _other = $('#othe_charge').val();
        var html = '';


        html += '<input type="hidden" name="service_charge" id="service_charge" value="' + _service_charge + '" >';
        html += ' <input type="hidden" name="outsource" id="outsource" value="' + _outs_charge + '" >';
        html += ' <input type="hidden" name="tallyamy" id="tallyamy" value="' + _tallyamt + '"  >';
        html += ' <input type="hidden" name="otherc" id="other" value="' + _other + '" >';
        $(".allother").html(html);
        $("#amtzzz").val(_total);
        $("#ord_num_mz").val(_num_ord);
        $("#exampleModal44").modal('show');
    }
</script>


<div class="modal fade " id="exampleModal44" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Issue Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="verifiedmmodal" method="post" action="<?= base_url() ?>inhouse/addverifiedstatus">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_mz">
                            </div>
                            <div class="col-md-3">
                                Fisnished Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="finished_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Amount:
                                <input class="form-control" type="text" readonly="" name="finished_amt" id="amtzzz">
                            </div>

                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="finished_remark"></Textarea>

                            </div>
                            <div class="allother"></div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="verified_btn_mdl" class="btn btn-primary">Verified</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    $("#verified_btn_mdl").click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '<?= base_url() ?>inhouse/addverifieddata',
            method: 'post',
            data: $("#verifiedmmodal").serialize(),
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    if (confirm('Do You want to send sms?  ?')) {
                        alert('sms send');
                    } else {
                        window.location.href = "<?= base_url() ?>inhouse/verification";
                    }
                }
            }
        });
    })
</script>