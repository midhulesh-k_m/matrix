<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Onsite extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('onsite_model');
        $this->lgid = $_SESSION['login_id'];
        $this->u_type = $_SESSION['user_type'];
        $this->softid = $_SESSION['soft_id'];
    }
    public function getdelaerlikecus()
    {

        $type = $_GET['_tpe'];
        $dealers = $this->onsite_model->getdelaerlikecus($type);
        echo json_encode($dealers);
    }
    public function index($id = null)
    {
        $data = [];
        $data['order_no'] = $this->onsite_model->getOrderNum();

        $data['customer'] = $this->onsite_model->getOnsiteCus();
        $data['group'] = $this->onsite_model->getAllGroups();
        $data['problems'] = $this->onsite_model->getproblems();
        $data['location'] = $this->onsite_model->getlocation();
        $data['place'] = $this->onsite_model->getPlaces();
        if ($id) {
            $id = base64_decode($id);
            $data['ecnryptedid'] = $id;
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('order', $data);
            $this->load->view('layout/footer');
        } else {
            $data['ecnryptedid'] = '';
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('order', $data);
            $this->load->view('layout/footer');
        }
    }
    public function allocation()
    {
        $data = [];
        $data['orders'] = $this->onsite_model->getPendingOnsiteOrders();

        $data['tech'] = $this->onsite_model->getOnsiteTechnician();

        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allocation_list', $data);
        $this->load->view('layout/footer');
    }
    public function allote()
    {
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getAllotedOrders();

        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote_list', $data);
        $this->load->view('layout/footer');
    }
    public function verification()
    {
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderTOVerify();

        $data['orders'] = $this->onsite_model->getFinishedInhouseOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('verification_list', $data);
        $this->load->view('layout/footer');
    }
    public function getuserbyid()
    {
        $data = [];
        $uid = $_GET['_u_id'];
        $ref_no = $this->input->get('ref_no');
        $customer = $this->onsite_model->getUserDtlsById($ref_no, $uid);
        echo json_encode($customer);
    }
    // public function getmodelbyid()
    // {
    //     $data = [];
    //     $brand_id = $_GET['_brand_id'];
    //     $ref_no = $this->input->get('ref_no');
    //     $brand_id = $this->onsite_model->getModelById($brand_id);
    //     echo json_encode($brand_id);
    // }
    // public function getbrandbyitems()
    // {
    //     $data = [];
    //     $item = $_GET['_ite_val'];
    //     $ref_no = $this->input->get('ref_no');
    //     $brand_id = $this->onsite_model->getBrandByItem($item);
    //     echo json_encode($brand_id);
    // }
    public function getcustdatabyrefno()
    {
        $data = [];
        $ref_no = $_GET['_ref_id'];
        $ref = $this->onsite_model->getCusDataByRefNo($ref_no);
        echo json_encode($ref);
    }
    // public function getitemsbygroup()
    // {
    //     $data = [];
    //     $grp_val = $_GET['_grp_val'];
    //     $grp_val = $this->onsite_model->getitemsbygroup($grp_val);
    //     echo json_encode($grp_val);
    // }
    public function insertinhouse()
    {

        $ord_id =  $this->onsite_model->selectmax_ordId();
        $mob =  isset($_POST['mobile']) ? $_POST['mobile'] : '';

        if ((isset($_POST['updateId'])) && !empty($_POST['updateId'])) {
            $curtomer = array(
                'onsiteorder_id' =>   $ord_id['ord_id'],
                'onsiteorderno' =>   $_POST['updateId'],
                'order_date' =>   $_POST['ord_date'],
                'orderform' =>   $_POST['ord_from'],
                'ordertype' =>   $_POST['order_type'],
                'cust_id' =>   $_POST['customer_id'],
                'emp_id' =>   $this->lgid,
                'item' =>   $_POST['item'],
                'servicedate' =>   $_POST['aproxdate'],
                'servicetime' =>   $_POST['aproxdtime'],
                'servicedatetime' =>  date('Y-m-d'),
                'servicetype' =>   $_POST['sts'],
                'description' =>   $_POST['cmp_remarks'],
                // 'location' =>   $_POST['ref_no'],
                'locationid' =>   $_POST['location'],
                'refno' =>   $_POST['ref_no'],
                'soft_id' =>  $this->softid,
            );
            $this->db->where('onsiteorderno', $_POST['updateId'])->update('onsiteorders', $curtomer);
            $id =   $_POST['updateId'];

            echo json_encode($id);
        } else {



            $curtomer = array(
                'onsiteorder_id' =>   $ord_id['ord_id'],
                'onsiteorderno' =>   $_POST['order_no'],
                'order_date' =>   $_POST['ord_date'],
                'orderform' =>   $_POST['ord_from'],
                'ordertype' =>   $_POST['order_type'],
                'cust_id' =>   $_POST['customer_id'],
                'emp_id' =>   $this->lgid,
                'item' =>   $_POST['item'],
                'servicedate' =>   $_POST['aproxdate'],
                'servicetime' =>   $_POST['aproxdtime'],
                'servicedatetime' =>  date('Y-m-d'),
                'servicetype' =>   $_POST['sts'],
                'description' =>   $_POST['cmp_remarks'],
                // 'location' =>   $_POST['ref_no'],
                'locationid' =>   $_POST['location'],
                'refno' =>   $_POST['ref_no'],
                'soft_id' =>  $this->softid,
            );
            $id =  $this->onsite_model->newOrser($curtomer, $_POST['order_no']);

            $dte = date('Y-m-d');
            $item = $_POST['item'];

            $ornum = $_POST['order_no'];

            $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
            // Message details
            $numbers = array($mob);
            $sender = urlencode('TXTLCL');
            $message = rawurlencode('Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte . ' For Item :' . $item . ' Ph :' . $mob . '');

            $numbers = implode(',', $numbers);

            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            // Process your response here
            //$response = array("balance"=>29,"status"=>"success");

            $decode =  json_decode($response);
            $ord_id =  $this->onsite_model->selectmax_smsId();
            if ($decode->status) {

                $sms = array(
                    'smsId' => $ord_id['ord_id'],
                    'mobile' =>  $mob,
                    'message' => 'Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte . ' For Item :' . $item . ' Ph :' . $mob . '',
                    'orderno' => $ornum,
                    'smstype' => 'New Onsite Order ',
                    'mgr_id' =>  $this->softid,

                    'status' => $decode->status,
                    'soft_id' =>  $this->softid,
                );
                $this->db->insert('sendsms', $sms);
            }

            echo json_encode($id);
        }
    }
    public function inhouse_cmplnt()
    {
        $ord_id =  $this->onsite_model->selectmax_cmpId();

        $_POST['resp_ord_num'];
        // $headid =   $_POST['head_i'];
        // $num =   $_POST['num_i'];
        $problms =  $_POST['prob'];
        // $_POST['rough'];
        // $_POST['aproxdate'];
        // $_POST['cmp_remarks'];

        if ((isset($_POST['updateId'])) && !empty($_POST['updateId'])) {
            $this->db->where('orderno', $_POST['updateId'])->delete('onsitecomplaints');
            foreach ($problms as $key => $val) {
                $ord_id =  $this->onsite_model->selectmax_cmpId();
                $cmplnt = array(
                    'complaint_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['updateId'],
                    'prob_id' =>  $val,
                    'osdate' => Date('Y-m-d'),
                    'remarks' => '',
                    'emp_id' => $this->lgid,

                    'soft_id' =>  $this->softid,
                );
                $id =  $this->onsite_model->newOrderCmplnt($cmplnt);
            }
            $idz =  $_POST['updateId'];
            echo json_encode($idz);
        } else {

            foreach ($problms as $key => $val) {
                $ord_id =  $this->onsite_model->selectmax_cmpId();
                $cmplnt = array(
                    'complaint_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['resp_ord_num'],
                    'prob_id' =>  $val,
                    'osdate' => Date('Y-m-d'),
                    'remarks' => '',
                    'emp_id' => $this->lgid,

                    'soft_id' =>  $this->softid,
                );
                $id =  $this->onsite_model->newOrderCmplnt($cmplnt);
            }
            $idz =  $_POST['resp_ord_num'];
            echo json_encode($idz);
        }
    }
    // //allocation 
    public function alocatetechi()
    {

        $ord_id =  $this->onsite_model->getMaxOfTechId();
        $servicedate =   $_POST['_approxdate'];
        $ord_num =   $_POST['_ord_num'];
        $servicetime =   $_POST['_estimate'];
        $tech_id =   $_POST['_technician_id'];

        $allocate = array(
            'allocatetech_id' => $ord_id['ord_id'],
            'allocate_date' => date('Y-m-d'),
            'appx_date' => $servicedate,
            'techn_id' => $tech_id,
            'orderno' => $ord_num,
            'create_date' => date('Y-m-d'),
            'mgr_id' => $this->lgid,
            'soft_id' => $this->softid,
            'status' => "Allocated",
        );
        $id =  $this->onsite_model->allocateNewOrder($allocate, $ord_num, $tech_id);
        echo json_encode('success');
    }
    public function getproblems()
    {

        $probs = $this->onsite_model->getProbLists($_POST['_ord_num']);
        echo json_encode($probs);
    }
    public function verify_alloted($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderDtlsForTechi($id);

        $data['cal_sts'] = $this->onsite_model->getCallSts($id);

        $data['alloted_orders'] = $this->onsite_model->getAlottedOrdersForTechi($id);

        $data['issueditem'] = $this->onsite_model->getIssuedItemsTechi($id);
        $data['receiptnos'] = $this->onsite_model->getReceiptNo();

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote_verify', $data);
        $this->load->view('layout/footer');
    }
    public function view_alloted($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderDtlsForTechi($id);

        $data['cal_sts'] = $this->onsite_model->getCallSts($id);

        $data['alloted_orders'] = $this->onsite_model->getAlottedOrdersForTechi($id);

        $data['issueditem'] = $this->onsite_model->getIssuedItemsTechi($id);
        $data['receiptnos'] = $this->onsite_model->getReceiptNo();

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote', $data);
        $this->load->view('layout/footer');
    }
    public function savereport()
    {
        $ord_id =  $this->onsite_model->getMaxOfRprtId();
        $report = array(
            'servcallsId' => $ord_id,
            'orderno' => $_POST['ordnumber'],
            'attended_date' => $_POST['attnd_dte'],
            'modelno' => $_POST['machinemodelnum'],
            'serialno' => $_POST['serialnum'],
            'mechine' => $_POST['machine_dtls'],
            'callstartat' => $_POST['started_at'],
            'callcompletedat' => $_POST['completed_at'],
            'mechine_status' =>  $_POST['machinestatus'],
            'eng_analysis' => $_POST['Diagnostics'],
            'mgr_id' => $this->lgid,
            'soft_id' => $this->softid,
        );
        $this->db->insert('servicecallsentry', $report);
        $idn = $this->db->insert_id();
        $this->db->set('status', 'Finished')->where('orderno', $_POST['ordnumber'])->update('onsiteallocatetechinician');
        $this->db->set('status', 'Finished')->where('onsiteorderno', $_POST['ordnumber'])->update('onsiteorders');
        echo json_encode('success');
    }
    public function updatereceipt()
    {
        $ord_id =  $this->onsite_model->getMaxOfReceiptId();
        $receipt = array(
            'receipt_id' => $ord_id,
            'receiptno' => $_POST['Receipt_no'],
            'orderno' => $_POST['ordnumber'],
            'receipttype' => 'ONSITE',
            'receipt_date' => $_POST['receipt_dtae'],
            'service_amt' => $_POST['service_charge'],
            'incured_amt' => $_POST['Expense_inc'],
            'spare_amt' => $_POST['replacement_cst'],
            'paid_amt' => $_POST['paid_amt'],
            'net_amt' =>  $_POST['total_amt'],
            'bal_amt' => $_POST['balance_amt'],
            'refno' => '',
            'remarks' => '',
            'paid_date' => date('Y-m-d'),
            'tallyamt' => $_POST['tallyamount'],
            'emp_id' => $this->lgid,
            'soft_id' => $this->softid,
        );
        // $ord_techi  =  $this->onsite_model->getOnsiteRepTechi($_POST['ordnumber']);
        $this->db->where('orderno', $_POST['ordnumber'])->update('onsitereceipts', $receipt);
        echo json_encode('success');
    }
    public function receipt()
    {

        $ord_id =  $this->onsite_model->getMaxOfReceiptId();
        $receipt = array(
            'receipt_id' => $ord_id,
            'receiptno' => $_POST['Receipt_no'],
            'orderno' => $_POST['ordnumber'],
            'receipttype' => 'ONSITE',
            'receipt_date' => $_POST['receipt_dtae'],
            'service_amt' => $_POST['service_charge'],
            'incured_amt' => $_POST['Expense_inc'],
            'spare_amt' => $_POST['replacement_cst'],
            'paid_amt' => $_POST['paid_amt'],
            'net_amt' =>  $_POST['total_amt'],
            'bal_amt' => $_POST['balance_amt'],
            'refno' => '',
            'remarks' => '',
            'paid_date' => date('Y-m-d'),
            'tallyamt' => $_POST['tallyamount'],
            'emp_id' => $this->lgid,
            'soft_id' => $this->softid,
        );
        $ord_techi  =  $this->onsite_model->getOnsiteRepTechi($_POST['ordnumber']);
        $rcpts = $this->db->select('orderno')->where('orderno', $_POST['ordnumber'])->get('onsitereceipts')->row_array();
        if ($rcpts) {
            $this->db->where('orderno', $_POST['ordnumber'])->update('onsitereceipts', $receipt);
        } else {
            $this->db->insert('onsitereceipts', $receipt);
            $idn = $this->db->insert_id();
            $this->db->set('status', 'Delivered')->where('orderno', $_POST['ordnumber'])->update('onsiteallocatetechinician');
            $this->db->set('status', 'Delivered')->where('onsiteorderno', $_POST['ordnumber'])->update('onsiteorders');
        }

        $id = $_POST['ordnumber'];


        $ord_id =  $this->onsite_model->selectmax_smsId();
        $smsdata =  $this->db->where('orderno', $id)->get('sendsms')->row_array();
        $dte = date('Y-m-d');


        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($smsdata['mobile']);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Dear Customer, Your order has been Delivered Order No. : .' . $id . ' Date:' . $dte . '');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        $decode =  json_decode($response);
        // if ($decode->status) {
        $sms = array(
            'smsId' => $ord_id['ord_id'],
            'mobile' =>  $smsdata['mobile'],
            'message' => 'Dear Customer, Your order has been Delivered Order No. : .' . $id . ' Date:' . $dte . '',
            'orderno' => $id,
            'smstype' => 'Onsite Delivery',
            'mgr_id' =>  $this->softid,

            'status' => $decode->status,
            'soft_id' =>  $this->softid,
        );
        $this->db->insert('sendsms', $sms);



        echo json_encode('success');
    }
    public function cancelservcall()
    {

        $this->db->set('status', 'Cancelled')->where('orderno', $_POST['_num_ord'])->update('onsiteallocatetechinician');
        $this->db->set('status', 'Cancelled')->where('onsiteorderno', $_POST['_num_ord'])->update('onsiteorders');
        $this->db->set('status', 'Cancelled')->where('orderno', $_POST['_num_ord'])->update('onsitecomplaints');
        echo json_encode('success');
    }
    public function cancelbyId($id)
    {
        $id = base64_decode($id);

        $this->db->query("UPDATE `onsiteorders` SET `status` = 'Cancelled' WHERE `onsiteorderno` = '$id'");

        $sts =  $this->db->affected_rows();

        if ($sts >= 0) {
            redirect('onsite/allocation');
        }
    }
    public function changecmplntsts()
    {
        $cmpid = $_POST['_cmpId'];
        $amtz = $_POST['_amtz'];
        $rmrk = $_POST['_rmrk'];
        $this->db->set('status', 'Completed')->set('amount', $amtz)->set('remarks', $rmrk)->set('repairby_technid',  $this->lgid)->set('finisheddate', date('Y-m-d'))->where('complaint_id', $cmpid)->update('onsitecomplaints');
        echo json_encode('success');
    }
    public function setstatusverified()
    {
        $cmpid = $_POST['_cmpId'];
        $prc = $_POST['_prc'];
        $this->db->set('amount', $prc)->set('status', 'Verified')->set('verified', 1)->set('verifiedby',  $this->lgid)->set('finisheddate', date('Y-m-d'))->where('complaint_id', $cmpid)->update('onsitecomplaints');
        echo json_encode('success');
    }
    public function saveverifications()
    {
        $id =  $_POST['_num_ord'];



        $ord_id =  $this->onsite_model->selectmax_smsId();
        $smsdata =  $this->db->where('orderno', $id)->get('sendsms')->row_array();
        $dte = date('Y-m-d');


        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($smsdata['mobile']);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Dear Customer, Your order has been Verified Order No. : .' . $id . ' Date:' . $dte . '');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        $decode =  json_decode($response);
        // if ($decode->status) {
        $sms = array(
            'smsId' => $ord_id['ord_id'],
            'mobile' =>  $smsdata['mobile'],
            'message' => 'Dear Customer, Your order has been Verified Order No. : .' . $id . ' Date:' . $dte . '',
            'orderno' => $id,
            'smstype' => 'Onsite verify',
            'mgr_id' =>  $this->softid,

            'status' => $decode->status,
            'soft_id' =>  $this->softid,
        );
        $this->db->insert('sendsms', $sms);





        $this->db->set('status', 'Verified')->where('orderno', $_POST['_num_ord'])->update('onsiteallocatetechinician');
        $this->db->set('status', 'Verified')->where('onsiteorderno', $_POST['_num_ord'])->update('onsiteorders');
        echo json_encode('success');
    }
    public function insertnewcustmr()
    {

        $_POST['c_nme'];
        $_POST['m_no'];
        $_POST['p_no'];
        $_POST['adrs'];
        $_POST['em_id'];
        $_POST['locaId'];
        $max_id =  $this->onsite_model->getMaxOfCusId();
        $cus = array(
            'cust_id' => $max_id['ord_id'],
            'custname' => $_POST['c_nme'],
            'address' => $_POST['adrs'],
            'emailid' => $_POST['em_id'],
            'mobile' => $_POST['m_no'],
            'landline' => $_POST['p_no'],
            'mgr_id' =>  $this->lgid,
            'ordertype' => $_POST['_ctpe'],
            'place' => $_POST['locaId'],
            'worktype' => 'ONSITE',
            'soft_id' => $this->softid,
        );
        $id =  $this->db->insert('customers', $cus);

        if ($id >= 0) {
            echo json_encode($_POST['_ctpe']);
        }
    }
    public function delivery()
    {
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getVerifiedOrderListsOnsite();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('view_delivery', $data);
        $this->load->view('layout/footer');
    }
    public function delivery_item($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderDtlsForTechi($id);
        $ord_id =  $this->onsite_model->getMaxOfReceiptId();
        $data['ord_id'] = $ord_id;
        $data['cal_sts'] = $this->onsite_model->getCallSts($id);

        $data['alloted_orders'] = $this->onsite_model->getAlottedOrdersForTechi($id);

        $data['issueditem'] = $this->onsite_model->getIssuedItemsTechi($id);
        $data['receiptnos'] = $this->onsite_model->getReceiptNo();

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote_delivery', $data);
        $this->load->view('layout/footer');
    }
    public function delivered()
    {
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOnsiteDeliveredOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('delivered', $data);
        $this->load->view('layout/footer');
    }
    public function delivered_item($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderDtlsForTechi($id);
        $ord_id =  $this->onsite_model->getMaxOfReceiptId();
        $data['ord_id'] = $ord_id;
        $data['cal_sts'] = $this->onsite_model->getCallSts($id);

        $data['alloted_orders'] = $this->onsite_model->getAlottedOrdersForTechi($id);

        $data['issueditem'] = $this->onsite_model->getIssuedItemsTechi($id);
        $data['receiptnos'] = $this->onsite_model->getReceiptNo();

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote_delivery', $data);
        $this->load->view('layout/footer');
    }
    public function delivered_order($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getOrderDtlsForTechi($id);
        $data['receipts'] = $this->onsite_model->getReceiptsDataById($id);
        $ord_id =  $this->onsite_model->getMaxOfReceiptId();
        $data['ord_id'] = $ord_id;
        $data['cal_sts'] = $this->onsite_model->getCallSts($id);

        $data['alloted_orders'] = $this->onsite_model->getAlottedOrdersForTechi($id);

        $data['issueditem'] = $this->onsite_model->getIssuedItemsTechi($id);
        $data['receiptnos'] = $this->onsite_model->getReceiptNo();

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('v_delivered', $data);
        $this->load->view('layout/footer');
    }
    public function cancelled()
    {
        $data = [];
        $data['alloted_list'] = $this->onsite_model->getCancelledLists();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('cancelled', $data);
        $this->load->view('layout/footer');
    }
    public function print_ordrs()
    {
        $id = $_POST['resp'];

        $data['cusdata'] =  $this->onsite_model->getDatatoprint($id);

        $data['problems'] =  $this->onsite_model->getComplaintsToPrint($id);
        $this->load->view('onsiteone', $data);
    }
    public function printonsitereceipt()
    {
        $id = $_POST['resp'];

        $data['cusdata'] =  $this->onsite_model->getDatatoprint($id);

        $data['service'] =  $this->onsite_model->getServicedata($id);
        $this->load->view('onsite_receipt', $data);
    }
}
