<?php
class Inhouse_model extends CI_Model
{
    public function __construct()
    {
        $this->lgid = $_SESSION['login_id'];
        $this->u_type = $_SESSION['user_type'];
        $this->softid = $_SESSION['soft_id'];
    }
    public function getOrderNum()
    {
        $date = date('Y');
        $array = str_split($date, 1);
        $int = $array[3] + 1;
        $yr =  $array[2] . $array[3];

        // $order_id =   $this->db->select('orderno')->where('soft_id',  $this->softid)->order_by('order_id', 'desc')->get('orders')->row_array();
        $order_id = $this->db->query("SELECT `orderno`   FROM `orders` WHERE `soft_id` = '$this->softid' ORDER BY order_id DESC")->row_array();

        if ($order_id) {

            $id = explode('/', $order_id['orderno']);

            $id = $id[1] + 1;
            return  'IN0' . $int . '-' . $yr . '/' . $id;
        } else {
            return  'IN0' . $int . '-' . $yr . '/1';
        }
    }
    public function getInhouseCustomers()
    {
        return   $this->db->where('soft_id', $this->softid)->where('worktype', 'INHOUSE')->get('customers')->result_array();
    }
    public function getUserDtlsById($refno = null, $uid = null)
    {

        if (!($refno)) {

            $customer =   $this->db->select('customers.custname,customers.address,customers.emailid,customers.mobile,customers.landline,customers.place')->where('cust_id', $uid)->where('soft_id',   $this->softid)->get('customers')->row_array();

            if ($customer) {
                $xus = [];
                $xus['custname'] = $customer['custname'];
                $xus['address'] = $customer['address'];
                $xus['emailid'] = $customer['emailid'];
                $xus['mobile'] = $customer['mobile'];
                $xus['landline'] = $customer['landline'];
                $xus['place'] = $customer['place'];
                $xus['cus_orders'] = $this->getCusOrders($uid);
                return  $xus;
            }
        } else {
        }
    }
    public function getCusOrders($id)
    {
        $orders =  $this->db->select('orderno,item,branditem_id,model_id,serialno,warranty,create_date,status')->where('cust_id', $id)->where('soft_id',  $this->softid)->order_by('order_id', 'desc')->get('orders')->result_array();

        if ($orders) {
            $odrs = [];
            foreach ($orders as $key => $ord_vl) {
                $odrs[$key]['orderno'] = $ord_vl['orderno'];
                $odrs[$key]['item'] = $ord_vl['item'];
                $odrs[$key]['branditem_id'] = $ord_vl['branditem_id'];
                $odrs[$key]['model_id'] = $ord_vl['model_id'];
                $odrs[$key]['serialno'] = $ord_vl['serialno'];
                $odrs[$key]['warranty'] = $ord_vl['warranty'];
                $odrs[$key]['create_date'] = $ord_vl['create_date'];
                $odrs[$key]['status'] = $ord_vl['status'];
                $odrs[$key]['brand'] = $this->getBrandByBrandId($ord_vl['branditem_id']);
                $odrs[$key]['model'] = $this->getModelByModelId($ord_vl['model_id']);
            }
            return  $odrs;
        }
    }
    public function getBrandByBrandId($bid)
    {
        return $this->db->select('brands.brandname,branditems.brand_id')->join('brands', 'brands.brand_id = branditems.brand_id', 'inner')->where('branditems.branditem_id', $bid)->where('brands.brand_id = branditems.brand_id')->where('branditems.soft_id',  $this->softid)->get('branditems')->row_array();
    }
    public function getModelByModelId($bid)
    {
        return $this->db->select('modelname')->where('model_id', $bid)->where('soft_id', $this->softid)->get('models')->row_array();
    }
    public function getBrandByItem($item)
    {
        // return $this->db->select('brand_id,brandname,')->where('soft_id', 0)->get('brands')->result_array();

        return $this->db->select('branditems.brand_id,branditems.branditem_id,branditems.item,brands.brand_id,brands.brandname')->join('brands', 'brands.brand_id = branditems.brand_id', 'inner')->where('branditems.item', $item)->where('branditems.soft_id',  $this->softid)->get('branditems')->result_array();
    }
    public function getModelById($brand_id)
    {

        return $this->db->select('model_id,modelname,')->where('branditem_id', $brand_id)->where('soft_id', $this->softid)->get('models')->result_array();
    }
    public function getCusDataByRefNo($ref_no)
    {
        $ref_data = [];
        $cusid =  $this->db->select('cust_id,item,branditem_id,model_id,serialno,warranty,rough_estimate,appx_date')->where('orderno', $ref_no)->where('soft_id',  $this->softid)->get('orders')->row_array();
        $ref_data['order_dtls'] =  $cusid;

        $ref_data['order_dtls'] =  $cusid;

        $approx =  $this->db->select('appx_date')->where('orderno', $ref_no)->where('soft_id',  $this->softid)->get('orders')->row_array();
        $approx = strtotime($approx['appx_date']);
        $approx = date('Y-m-d', $approx);
        $ref_data['approx'] = $approx;
        $customer =   $this->db->select('customers.cust_id,customers.ordertype,customers.custname,customers.address,customers.emailid,customers.mobile,customers.landline,customers.place')->where('cust_id', $cusid['cust_id'])->where('soft_id',  $this->softid)->get('customers')->row_array();
        $ref_data['cus_data'] =  $customer;

        $ref_data['allcustomers'] =  $this->db->get('customers')->result_array();




        $customerx =   $this->db->select('customers.custname,customers.address,customers.emailid,customers.mobile,customers.landline,customers.place')->where('cust_id', $cusid['cust_id'])->where('soft_id',  $this->softid)->get('customers')->row_array();


        $xus = [];
        $xus['custname'] = $customerx['custname'];
        $xus['address'] = $customerx['address'];
        $xus['emailid'] = $customerx['emailid'];
        $xus['mobile'] = $customerx['mobile'];
        $xus['landline'] = $customerx['landline'];
        $xus['place'] = $customerx['place'];
        $xus['cus_orders'] = $this->getCusOrdersz($cusid['cust_id']);
        $ref_data['cus_address'] =  $xus;


        // complaints and priliminary

        $ref_data['problems'] = $this->db->query("SELECT complaints.prob_id,problems.probname FROM complaints INNER JOIN problems ON problems.prob_id = complaints.prob_id WHERE  complaints.orderno = '$ref_no' AND complaints.status ='Pending' ")->result_array();

        $grps = $this->db->select('recieveitems.item_id,items.group_id,groups.groupname')->join('items', 'items.item_id = recieveitems.item_id', 'inner')->join('groups', 'groups.group_id = items.group_id', 'inner')->where('recieveitems.orderno', $ref_no)->group_by('items.group_id')->get('recieveitems')->result_array();
        if ($grps) {
            $gp = [];
            foreach ($grps as $key => $grps_val) {
                $gb_obj = new stdClass();
                $gb_obj->group_id = $grps_val['group_id'];
                $gb_obj->group_name = $grps_val['groupname'];
                $gb_obj->rec_item = $this->getRecievedItem($grps_val['group_id'], $grps_val['item_id'], $ref_no);
                $gp[] = $gb_obj;
            }
            $ref_data['recieved'] = $gp;
        }



        return  $ref_data;
    }
    public function getCusOrdersz($id)
    {
        $orders =  $this->db->select('orderno,item,branditem_id,model_id,serialno,warranty,create_date,status')->where('cust_id', $id)->where('soft_id',  $this->softid)->order_by('orderno', 'desc')->get('orders')->result_array();
        if ($orders) {
            $odrs = [];
            foreach ($orders as $key => $ord_vl) {
                $ref_data_res = new stdClass();
                $ref_data_res->orderno = $ord_vl['orderno'];
                $ref_data_res->item = $ord_vl['item'];
                $ref_data_res->branditem_id = $ord_vl['branditem_id'];
                $ref_data_res->model_id = $ord_vl['model_id'];
                $ref_data_res->serialno = $ord_vl['serialno'];
                $ref_data_res->warranty = $ord_vl['warranty'];
                $ref_data_res->create_date = $ord_vl['create_date'];
                $ref_data_res->status = $ord_vl['status'];
                $ref_data_res->brand = $this->getBrandByBrandId($ord_vl['branditem_id']);
                $ref_data_res->model = $this->getModelByModelId($ord_vl['model_id']);
                $odrs[]  =   $ref_data_res;
            }
            return  $odrs;
        }
    }
    public function getAllGroups()
    {
        return  $this->db->select('group_id,groupname')->where('soft_id',  $this->softid)->get('groups')->result_array();
    }
    public function getitemsbygroup($grp_val)
    {
        return  $this->db->select('item_id,itemname')->where('group_id', $grp_val)->where('soft_id',  $this->softid)->get('items')->result_array();
    }
    public function getproblems()
    {
        return  $this->db->select('probname,prob_id')->where('soft_id',  $this->softid)->get('problems')->result_array();
    }
    public function newOrser($curtomer, $ord_num)
    {
        try {
            // $this->db->trans_start(FALSE);
            $this->db->insert('orders', $curtomer);
            // $this->db->trans_complete();
            $db_error = $this->db->error();
            if (!empty($db_error['message'])) {
                throw new Exception('Failed to insert complaint => Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false;
            } else {
                return $ord_num;
            }
        } catch (Exception $e) {
            return  log_message('error', $e->getMessage());
        }
    }
    public function selectmax_ordId()
    {
        return  $this->db->query("SELECT IFNULL(max(`order_id` + 1),1)  as ord_id FROM orders")->row_array();
    }
    public function selectmax_cmpId()
    {
        return  $this->db->query("SELECT IFNULL(max(`complaint_id` + 1),1)  as ord_id FROM complaints")->row_array();
    }
    public function setApproxPrc($ord_num,   $rough_amt, $date)
    {
        $this->db->set('rough_estimate', $rough_amt)->set('appx_date', $date)->where('orderno', $ord_num)->update('orders');
    }
    public function newOrderCmplnt($cmplnt)
    {
        $this->db->insert('complaints', $cmplnt);
    }
    public function selectmax_recieved()
    {
        return  $this->db->query("SELECT IFNULL(max(`revitem_id` + 1),1)  as ord_id FROM recieveitems")->row_array();
    }
    public function newrecievedItem($item)
    {
        $this->db->insert('recieveitems', $item);
    }
    public function getPendingInhouseOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Pending' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getFinishedInhouseOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Finished' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getDeliveredInhouseOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Delivered' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getCancelledInhouseOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Cancelled' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getreturnedorders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Return' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getAllorders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.soft_id =   $this->softid")->result_array();
    }
    public function getrefundedlists()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Refund' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getUnpaidorders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Unpaid' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getVerifiedOrders()
    {
        return  $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.finished_date,orderverify.serviceamt FROM orderverify INNER JOIN orders ON orders.orderno = orderverify.orderno INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.status = 'Verified' AND orders.soft_id =   $this->softid")->result_array();
    }
    public function getInouseTechnician()
    {
        return  $this->db->query("SELECT techn_id,tech_name FROM technicians WHERE `inhouse` = 1 AND status = 'Valid' AND soft_id = $this->softid")->result_array();
    }
    public function getMaxOfTechId()
    {
        return  $this->db->query("SELECT IFNULL(max(`allocatetech_id` + 1),1)  as ord_id FROM allocatetechinician")->row_array();
    }
    public function allocateNewOrder($allocate, $ord_num, $tech_id)
    {


        $one =  $this->db->set('status', "Allocated")->where('orderno', $ord_num)->update('orders');
        if ($one) {
            $two =   $this->db->set('status', "Allocated")->set('repairby_technid', $tech_id)->set('allocator_id',  $this->lgid)->where('orderno', $ord_num)->update('complaints');
            if ($two) {
                $three  =  $this->db->insert('allocatetechinician', $allocate);
                return $this->db->insert_id();
            }
        }
    }

    public function getProbLists($id)
    {
        $prob = [];
        $prob['problems'] = $this->db->query("SELECT complaints.prob_id,problems.probname FROM complaints INNER JOIN problems ON problems.prob_id = complaints.prob_id WHERE  complaints.orderno = '$id' AND complaints.status ='Pending' ")->result_array();

        $grps = $this->db->select('recieveitems.item_id,items.group_id,groups.groupname')->join('items', 'items.item_id = recieveitems.item_id', 'inner')->join('groups', 'groups.group_id = items.group_id', 'inner')->where('recieveitems.orderno', $id)->group_by('items.group_id')->get('recieveitems')->result_array();
        if ($grps) {
            $gp = [];
            foreach ($grps as $key => $grps_val) {
                $gb_obj = new stdClass();
                // $gb_obj->group_id = $grps_val['group_id'];
                $gb_obj->group_name = $grps_val['groupname'];
                $gb_obj->rec_item = $this->getRecievedItem($grps_val['group_id'], $grps_val['item_id'], $id);
                $gp[] = $gb_obj;
            }
            $prob['recieved'] = $gp;
        }
        return $prob;
    }
    public function getreceiptdata($id)
    {
        return   $this->db->select('receiptno,receipt_date,paid_amt as net_amt')->where('orderno', $id)->get('inhousereceipts')->row_array();
    }
    public function getRecievedItem($g_id, $item, $id)
    {
        return  $this->db->select('recieveitems.item_id,items.itemname')->join('recieveitems', 'recieveitems.item_id = items.item_id', 'inner')->where('items.group_id', $g_id)->where('recieveitems.orderno', $id)->get('items')->result_array();
    }
    public function getAllotedOrders()
    {

        // $this->db->select('allocatetech_id,allocate_date,appx_date,validate_date,orderno,techn_id,rough_estimate,status')->where('soft_id', $this->softid)->where('status', 'Recheck')->get('allocatetechinician')->result_array();

        return     $this->db->query("SELECT `allocatetech_id`, `allocate_date`, `appx_date`, `validate_date`, `orderno`, `techn_id`, `rough_estimate`, `status` FROM `allocatetechinician` WHERE (`status` = 'Recheck' OR `status` = 'Allocated') AND  `soft_id` = '1' GROUP By orderno ")->result_array();
    }
    public function getOrderDtlsForTechi($id)
    {
        return   $this->db->query("SELECT brands.brandname,branditems.branditem_id,models.modelname,customers.custname,customers.mobile,orders.ordertype,orders.model_id,orders.item,orders.warranty,orders.serialno,technicians.tech_name,orders.appx_date,orders.rough_estimate FROM allocatetechinician INNER JOIN orders on orders.orderno = allocatetechinician.orderno INNER JOIN customers on customers.cust_id = orders.cust_id INNER JOIN models on models.model_id = orders.model_id INNER JOIN technicians on technicians.techn_id = allocatetechinician.techn_id INNER JOIN branditems on branditems.branditem_id = orders.branditem_id INNER JOIN brands on brands.brand_id = branditems.brand_id WHERE allocatetechinician.soft_id = $this->softid AND allocatetechinician.orderno = '$id' ")->row_array();
    }
    public function getIssuedItemsTechi($id)
    {
        // return   $this->db->query("SELECT issue_item.issued_item,issue_item.serial_number,issue_item.warranty_status, issue_item.orderno,brands.brandname,models.modelname,branditems.brand_id FROM issue_item INNER JOIN branditems on branditems.branditem_id = issue_item.branditem_id INNER JOIN brands on brands.brand_id = branditems.brand_id INNER JOIN models on models.model_id = issue_item.model WHERE issue_item.orderno = '$id' AND  issue_item.soft_id = $this->softid ")->result_array();
        return   $this->db->where('orderno', $id)->get('issue_item')->result_array();
    }
    public function getAlottedOrdersForTechi($id)
    {

        $alocate['remarks_sts'] =  $this->db->query("SELECT IFNULL(complaints.remarks,complaints.remarks) as remarks,allocatetechinician.status,allocatetechinician.rough_estimate,allocatetechinician.remarks as tech_remarks FROM complaints INNER JOIN allocatetechinician on allocatetechinician.orderno = complaints.orderno WHERE complaints.orderno = '$id' AND complaints.remarks != 'NULL'  AND complaints.remarks != ''  AND complaints.soft_id = $this->softid")->row_array();

        $alocate['complaints'] =  $this->db->query("SELECT  complaints.status,complaints.amount,complaints.complaint_id,complaints.prob_id,problems.probname FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id WHERE complaints.orderno = '$id' AND  complaints.soft_id = $this->softid")->result_array();
        return   $alocate;
    }
    public function getAlottedOrdersForVerify($id)
    {
        $alocate['remarks_sts'] =  $this->db->query("SELECT IFNULL(complaints.remarks,complaints.remarks) as remarks,allocatetechinician.status,allocatetechinician.rough_estimate,allocatetechinician.remarks as tech_remarks FROM complaints INNER JOIN allocatetechinician on allocatetechinician.orderno = complaints.orderno WHERE complaints.orderno = '$id' AND complaints.remarks != 'NULL'  AND complaints.remarks != ''  AND complaints.soft_id = $this->softid")->row_array();

        $alocate['complaints'] =  $this->db->query("SELECT technicians.tech_name,complaints.status,complaints.amount,complaints.complaint_id,complaints.prob_id,problems.probname,complaints.repairby_technid,complaints.finisheddate,complaints.remarks,complaints.verified FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id   INNER JOIN technicians on technicians.techn_id = complaints.repairby_technid  WHERE complaints.orderno = '$id' AND  complaints.soft_id = $this->softid")->result_array();

        return   $alocate;
    }
    public function getDlryOrdrs($id)
    {
        $dlry = [];
        $dlry['cmplnt'] = $this->db->query("SELECT complaints.prob_id,complaints.status,complaints.amount,complaints.remarks,problems.probname FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id WHERE complaints.soft_id = $this->softid AND complaints.orderno = '$id' ")->result_array();

        $dlry['spare'] = $this->db->query("SELECT issue_item.issued_item,issue_item.price FROM issue_item WHERE orderno = '$id' AND soft_id = $this->softid")->result_array();
        return  $dlry;
    }
    public function updatevefificationstatus($sts, $ord_num)
    {
        $this->db->set('verified', $sts)->set('verifiedby', $this->lgid)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function updatetechistatus($sts, $ord_num, $order_number)
    {

        $one =   $this->db->set('status', $sts)->set('finisheddate', date('Y-m-d'))->set('repairby_technid', $this->lgid)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');
        if ($one) {
            $two =  $this->db->set('validate_date', date('Y-m-d'))->where('orderno', $order_number)->where('soft_id', $this->softid)->update(' allocatetechinician');

            return $this->db->affected_rows();
        }
    }
    public function updatetechiectimate($sts, $ord_num)
    {

        $this->db->set('amount', $sts)->where('complaint_id', $ord_num)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function updatetechiremarks($sts, $ord_num)
    {
        $this->db->set('remarks', $sts)->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        return $this->db->affected_rows();
    }
    public function addNewTechiRemarkz($data, $id)
    {
        $this->db->set('remarks', $data)->where('complaint_id', $id)->where('soft_id', $this->softid)->update('complaints');
        return $this->db->affected_rows();
    }
    public function finishtechiworks($ord_num)
    {
        $one =  $this->db->set('finished_date', date('Y-m-d'))->set('status', "Finished")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        if ($one) {
            $two =  $this->db->set('finished_date', date('Y-m-d'))->set('status', "Finished")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('orders');
            if ($two >= 0) {
                return $this->db->affected_rows();
            }
        }
    }
    public function getUserDetails($ordnum)
    {
        $type =  $this->db->query("SELECT customers.ordertype,orders.cust_id FROM orders INNER JOIN customers on customers.cust_id = orders.cust_id WHERE orders.orderno = '$ordnum' AND orders.soft_id = $this->softid")->row_array();
        return $type['ordertype'];
    }
    public function getCallMAxID()
    {
        return  $this->db->query("SELECT IFNULL(max(`call_id` + 1),1)  as ord_id FROM callstatus")->row_array();
    }
    public function getusertypebyId($id)
    {
        return   $this->db->query("SELECT orders.cust_id,customers.ordertype FROM orders INNER JOIN customers on customers.cust_id = orders.cust_id WHERE orders.orderno = '$id'")->row_array();
    }
    public function recheckworks($ord_num)
    {
        $this->db->set('status', "Recheck")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status', "Recheck")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('orders');
        return $this->db->affected_rows();
    }
    public function cancl($ord_num)
    {
        $this->db->set('status', "Cancelled")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status', "Cancelled")->where('orderno', $ord_num)->where('soft_id', $this->softid)->update('orders');
        return $this->db->affected_rows();
    }
    public function getMaxVerifiedId()
    {
        return  $this->db->query("SELECT IFNULL(max(`orderverifyId` + 1),1)  as ord_id FROM  orderverify")->row_array();
    }
    public function getIssuedItems()
    {
        return  $this->db->query("SELECT * FROM `issue_item_name` WHERE `soft_id` = $this->softid")->result_array();
    }
    public function getissueditemname($id)
    {
        return   $this->db->query("SELECT `issued_item` FROM issue_item WHERE issued_item LIKE '%$id%' AND soft_id = $this->softid")->result_array();
    }
    public function getDeliveryCharge($id)
    {
        return   $this->db->query("SELECT `serviceamt`,`otheramt`,`totalamt`,`outsourceamt`,`tallyamt` FROM orderverify WHERE orderno = '$id' ")->row_array();
    }
    public function getMaxDeliveryId()
    {
        return  $this->db->query("SELECT IFNULL(max(`deliveryId` + 1),1)  as ord_id FROM  orderdelivery")->row_array();
    }
    public function getdelaerlikecus($id)
    {
        if ($id == 'Dealer') {
            $wre = "ordertype = 'Dealer'";
        } else {
            $wre = "ordertype != 'Dealer'";
        }
        return   $this->db->where('soft_id', $this->softid)->where('worktype', 'INHOUSE')->where($wre)->get('customers')->result_array();
    }
    public function getPlaces()
    {
        return   $this->db->select('loc_id,location')->where('soft_id', $this->softid)->get('locations')->result_array();
    }
    public function getMaxOfCusId()
    {
        return  $this->db->query("SELECT IFNULL(max(`cust_id` + 1),1)  as ord_id FROM  customers")->row_array();
    }
    public function getMaxOfBrandId()
    {
        return  $this->db->query("SELECT IFNULL(max(`brand_id` + 1),1)  as ord_id FROM  brands")->row_array();
    }
    public function  getMaxOfBrandItemId()
    {
        return  $this->db->query("SELECT IFNULL(max(`branditem_id` + 1),1)  as ord_id FROM  branditems")->row_array();
    }
    public function getMaxOFmodelID()
    {
        return  $this->db->query("SELECT IFNULL(max(`model_id` + 1),1)  as ord_id FROM  models")->row_array();
    }
    public function getDeliveredData($id)
    {
        return   $this->db->where('orderno', $id)->get('orderdelivery')->row_array();
    }
    public function getrefundAmountById($ordnum)
    {
        return  $this->db->select('orderdelivery.netamt')->where('orderdelivery.orderno', $ordnum)->get('orderdelivery')->row_array();
    }
    public function selectmax_refundid()
    {
        return  $this->db->query("SELECT IFNULL(max(`refund_id` + 1),1) as ord_id FROM refundorder")->row_array();
    }
    public function getusertypes($ordnum)
    {
        return    $this->db->query("SELECT customers.ordertype FROM orders INNER JOIN customers on customers.cust_id = orders.cust_id where orders.orderno = '$ordnum'")->row_array();
    }
    public function getaddeditems()
    {
        return  $this->db->get('add_issue_item')->result_array();
    }
    public function getdataFrIssue($id)
    {
        $data =  $this->db->where('orderno', $id)->get('orders')->row_array();
        if ($data) {
            $itms = [];
            $itms['orderno'] =  $data['orderno'];
            $itms['item'] =  $data['item'];
            $itms['customer'] = $this->getcustmrs($data['cust_id']);
            $itms['complaints'] = $this->getcomplaints($data['orderno']);
            return   $itms;
        } else {
            $data =  $this->db->where('onsiteorderno', $id)->get('onsiteorders')->row_array();
            if ($data) {
                $itms = [];
                $itms['orderno'] =  $data['onsiteorderno'];
                $itms['item'] = '';
                $itms['customer'] = $this->getcustmrs($data['cust_id']);
                $itms['complaints'] = $this->getcomplaintsonsite($data['orderno']);
                return   $itms;
            } else {
                return 'False';
            }
        }
    }
    public function getissueItemByItem($itm = null)
    {
        if ($itm) {
            return  $this->db->select('issued_id,issued_name,price')->where('item', $itm)->get('add_issue_item')->result_array();
        } else {
            return  $this->db->select('issued_id,issued_name,price')->get('add_issue_item')->result_array();
        }
    }
    public function getcustmrs($id)
    {
        return  $this->db->select('custname,mobile')->where('cust_id', $id)->get('customers')->row_array();
    }
    public function getcomplaints($orderno)
    {
        return   $this->db->query("SELECT complaints.orderno,complaints.prob_id,problems.probname,complaints.status FROM complaints INNER JOIN problems on problems.prob_id = complaints.prob_id WHERE complaints.orderno='$orderno'")->result_array();
    }
    public function getcomplaintsonsite($id)
    {
        return   $this->db->query("SELECT onsitecomplaints.orderno,onsitecomplaints.prob_id,problems.probname,onsitecomplaints.status FROM onsitecomplaints INNER JOIN problems on problems.prob_id = onsitecomplaints.prob_id WHERE onsitecomplaints.orderno='$id'")->result_array();
    }
    public function getissuedItemNameById($id)
    {
        $dta =  $this->db->select('issued_id,issued_name,price')->where('issued_id', $id)->get('add_issue_item')->row_array();
        return  $dta['issued_name'];
    }
    public function getMaxOfreceiptId()
    {
        return  $this->db->query("SELECT IFNULL(max(`receipt_id` + 1),1)  as ord_id FROM inhousereceipts")->row_array();
    }
    public function getMaxOfreceiptNum()
    {
        return  $this->db->query("SELECT IFNULL(max(`receiptno` + 1),1)  as ord_id FROM inhousereceipts")->row_array();
    }
    public function getAdditionalOrddtls($id)
    {
        return   $this->db->query("SELECT customers.custname,customers.mobile,brands.brandname,branditems.branditem_id,models.modelname,orders.orderno,orders.order_date,orders.ordertype,orders.item,orders.warranty,orders.remarks,orders.appx_date,orders.rough_estimate,orders.status,orders.serialno FROM orders INNER JOIN models ON models.model_id = orders.model_id INNER JOIN branditems ON branditems.branditem_id = models.branditem_id INNER JOIN brands ON brands.brand_id = branditems.brand_id INNER JOIN customers ON customers.cust_id = orders.cust_id WHERE orders.orderno = '$id'  AND orders.soft_id =   $this->softid")->row_array();
    }
    public function selectmax_smsId()
    {
        return  $this->db->query("SELECT IFNULL(max(`smsId` + 1),1)  as ord_id FROM sendsms")->row_array();
    }
}
