<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inhouse extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('inhouse_model');
        $this->lgid = $_SESSION['login_id'];
        $this->u_type = $_SESSION['user_type'];
        $this->softid = $_SESSION['soft_id'];
    }
    public function index($id = null)
    {
        $data = [];
        $data['order_no'] = $this->inhouse_model->getOrderNum();
        $data['customer'] = $this->inhouse_model->getInhouseCustomers();
        $data['group'] = $this->inhouse_model->getAllGroups();
        $data['problems'] = $this->inhouse_model->getproblems();
        $data['place'] = $this->inhouse_model->getPlaces();
        if ($id) {
            $data['orderz_idz'] = base64_decode($id);
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('order', $data);
            $this->load->view('layout/footer');
        } else {
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('order', $data);
            $this->load->view('layout/footer');
        }
    }
    public function cancel_neworder($id)
    {
        $id = base64_decode($id);
        $this->db->where('orderno', $id)->set('status', 'Cancelled')->update('orders');
        redirect('inhouse/allocation');
    }
    public function allocation()
    {
        $data = [];
        $data['orders'] = $this->inhouse_model->getPendingInhouseOrders();
        $data['tech'] = $this->inhouse_model->getInouseTechnician();

        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allocation_list', $data);
        // $this->load->view('allocation', $data);
        $this->load->view('layout/footer');
    }
    public function allote()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote_list', $data);
        // $this->load->view('allote', $data);
        $this->load->view('layout/footer');
    }
    public function getuserbyid()
    {
        $data = [];
        $uid = $_GET['_u_id'];
        $ref_no = $this->input->get('ref_no');
        $customer = $this->inhouse_model->getUserDtlsById($ref_no, $uid);
        echo json_encode($customer);
    }
    public function getmodelbyid()
    {
        $data = [];
        $brand_id = $_GET['_brand_id'];
        $ref_no = $this->input->get('ref_no');
        $brand_id = $this->inhouse_model->getModelById($brand_id);
        echo json_encode($brand_id);
    }
    public function getbrandbyitems()
    {
        $data = [];
        $item = $_GET['_ite_val'];
        $ref_no = $this->input->get('ref_no');
        $brand_id = $this->inhouse_model->getBrandByItem($item);
        echo json_encode($brand_id);
    }
    public function getcustdatabyrefno()
    {
        $data = [];
        $ref_no = $_GET['_ref_id'];
        $ref = $this->inhouse_model->getCusDataByRefNo($ref_no);
        echo json_encode($ref);
    }
    public function getitemsbygroup()
    {
        $data = [];
        $grp_val = $_GET['_grp_val'];
        $grp_val = $this->inhouse_model->getitemsbygroup($grp_val);
        echo json_encode($grp_val);
    }
    public function insertinhouse()
    {
        $mob = isset($_POST['mobile']) ? $_POST['mobile'] : '';


        $ord_id =  $this->inhouse_model->selectmax_ordId();
        if (isset($_POST['edited'])) {
            $curtomer = array(
                'order_id' =>   $ord_id['ord_id'],
                'orderno' =>  $_POST['edited'],
                'order_date' =>   $_POST['ord_date'],
                'orderform' =>   $_POST['ord_from'],
                'ordertype' =>   $_POST['order_type'],
                'cust_id' =>   $_POST['customer_id'],
                'emp_id' =>   $this->lgid,
                'item' =>   $_POST['item'],
                'branditem_id' =>   $_POST['brand_id'],
                'model_id' =>   $_POST['model_id'],
                'serialno' =>   $_POST['serial_num'],
                'warranty' =>   $_POST['sts'],
                'refno' =>   $_POST['ref_no'],
                'soft_id' =>  $this->softid,
            );
            $this->db->where('orderno', $_POST['edited'])->update('orders', $curtomer);
            $id = $_POST['edited'];
            $this->db->affected_rows();
            echo json_encode($id);
        } else {
            $curtomer = array(
                'order_id' =>   $ord_id['ord_id'],
                'orderno' =>   $_POST['order_no'],
                'order_date' =>   $_POST['ord_date'],
                'orderform' =>   $_POST['ord_from'],
                'ordertype' =>   $_POST['order_type'],
                'cust_id' =>   $_POST['customer_id'],
                'emp_id' =>   $this->lgid,
                'item' =>   $_POST['item'],
                'branditem_id' =>   $_POST['brand_id'],
                'model_id' =>   $_POST['model_id'],
                'serialno' =>   $_POST['serial_num'],
                'warranty' =>   $_POST['sts'],
                'refno' =>   $_POST['ref_no'],
                'soft_id' =>  $this->softid,
            );
            $id =  $this->inhouse_model->newOrser($curtomer, $_POST['order_no']);
            $ornum = $_POST['order_no'];
            $ord_id =  $this->inhouse_model->selectmax_smsId();
            $dte = date('Y-m-d');
            $item = $_POST['item'];


            $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
            // Message details
            $numbers = array($mob);
            $sender = urlencode('TXTLCL');
            $message = rawurlencode('Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte . ' For Item :' . $item . ' Ph :' . $mob . '');

            $numbers = implode(',', $numbers);

            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            // Process your response here
            //$response = array("balance"=>29,"status"=>"success");
            $decode =  json_decode($response);
            if ($decode->status) {

                $sms = array(
                    'smsId' => $ord_id['ord_id'],
                    'mobile' =>  $mob,
                    'message' => 'Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte . ' For Item :' . $item . ' Ph :' . $mob . '',
                    'orderno' => $ornum,
                    'smstype' => 'New Order',
                    'mgr_id' =>  $this->softid,

                    'status' => $decode->status,
                    'soft_id' =>  $this->softid,
                );
                $this->db->insert('sendsms', $sms);
            }

            echo json_encode($id);
        }
    }
    public function inhouse_cmplnt()
    {

        $ord_id =  $this->inhouse_model->selectmax_cmpId();

        if (isset($_POST['edited'])) {

            $impId =  $_POST['edited'];
            $_POST['edited'];
            $headid =   $_POST['head_i'];
            $num =   $_POST['num_i'];
            $problms =  $_POST['prob'];
            $_POST['rough'];
            $_POST['aproxdate'];
            $_POST['cmp_remarks'];
            $this->db->set('remarks', $_POST['cmp_remarks'])->where('orderno', $_POST['edited'])->update('orders');
            $i = $_POST['i'];

            $this->inhouse_model->setApproxPrc($_POST['edited'],   $_POST['rough'], $_POST['aproxdate']);


            $this->db->where('orderno', $_POST['edited'])->delete('complaints');
            foreach ($problms as $key => $val) {
                $ord_id =  $this->inhouse_model->selectmax_cmpId();
                $cmplnt = array(
                    'complaint_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['edited'],
                    'prob_id' =>  $val,
                    'osdate' => Date('Y-m-d'),
                    'remarks' => '',
                    'emp_id' => $this->lgid,
                    'soft_id' =>  $this->softid,
                );
                $id =  $this->inhouse_model->newOrderCmplnt($cmplnt);
            }

            $this->db->where('orderno', $_POST['edited'])->delete('recieveitems');
            foreach ($i as $key => $i_val) {
                $ord_id =  $this->inhouse_model->selectmax_recieved();
                $items = array(
                    'revitem_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['edited'],
                    'item_id' =>  $i_val,
                    'remarks' => '',
                    'emp_id' => $this->lgid,
                    'soft_id' =>  $this->softid,
                );
                $id =  $this->inhouse_model->newrecievedItem($items);
            }
            echo json_encode($impId);
        } else {
            $_POST['resp_ord_num'];
            $headid =   $_POST['head_i'];
            $num =   $_POST['num_i'];
            $problms =  $_POST['prob'];
            $_POST['rough'];
            $_POST['aproxdate'];
            $_POST['cmp_remarks'];
            $this->db->set('remarks', $_POST['cmp_remarks'])->where('orderno', $_POST['resp_ord_num'])->update('orders');
            $i = $_POST['i'];

            $this->inhouse_model->setApproxPrc($_POST['resp_ord_num'],   $_POST['rough'], $_POST['aproxdate']);
            foreach ($problms as $key => $val) {
                $ord_id =  $this->inhouse_model->selectmax_cmpId();
                $cmplnt = array(
                    'complaint_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['resp_ord_num'],
                    'prob_id' =>  $val,
                    'osdate' => Date('Y-m-d'),
                    'remarks' => '',
                    'emp_id' => $this->lgid,
                    'soft_id' =>  $this->softid,
                );
                $id =  $this->inhouse_model->newOrderCmplnt($cmplnt);
            }


            foreach ($i as $key => $i_val) {
                $ord_id =  $this->inhouse_model->selectmax_recieved();
                $items = array(
                    'revitem_id' => $ord_id['ord_id'],
                    'orderno' =>  $_POST['resp_ord_num'],
                    'item_id' =>  $i_val,
                    'remarks' => '',
                    'emp_id' => $this->lgid,
                    'soft_id' =>  $this->softid,
                );
                $id =  $this->inhouse_model->newrecievedItem($items);
            }
            $impId =  $_POST['resp_ord_num'];
            echo json_encode($impId);
        }
    }
    //allocation 
    public function alocatetechi()
    {

        $ord_id =  $this->inhouse_model->getMaxOfTechId();
        $approx_date =   $_POST['_approxdate'];
        $ord_num =   $_POST['_ord_num'];
        $approx_amt =   $_POST['_estimate'];
        $tech_id =   $_POST['_technician_id'];

        $allocate = array(
            'allocatetech_id' => $ord_id['ord_id'],
            'allocate_date' => date('Y-m-d'),
            'appx_date' => $approx_date,
            'techn_id' => $tech_id,
            'orderno' => $ord_num,
            'rough_estimate' => $approx_amt,
            'mgr_id' => $this->lgid,
            'soft_id' => $this->softid,
            'status' => "Allocated",
        );
        $id =  $this->inhouse_model->allocateNewOrder($allocate, $ord_num, $tech_id);
        echo json_encode('success');
    }
    public function getproblems()
    {

        $probs = $this->inhouse_model->getProbLists($_POST['_ord_num']);
        echo json_encode($probs);
    }
    public function view_alloted($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getAlottedOrdersForTechi($id);
        $data['issueditem'] = $this->inhouse_model->getIssuedItemsTechi($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allote', $data);
        $this->load->view('layout/footer');
    }
    public function updatetechistatus()
    {
        $sts =    $_POST['_status'];
        $ord_num =  $_POST['_num_ord'];
        $order_number =  $_POST['_order_number'];
        $resp =  $this->inhouse_model->updatetechistatus($sts, $ord_num, $order_number);
        echo json_encode($resp);
    }
    public function updatetechiectimate()
    {
        $sts =    $_POST['_status'];
        $ord_num =  $_POST['_num_ord'];
        $resp =  $this->inhouse_model->updatetechiectimate($sts, $ord_num);
        echo json_encode($resp);
    }
    public function updatetechiremarks()
    {
        $sts =    $_POST['_status'];
        $ord_num =  $_POST['_num_ord'];
        $resp =  $this->inhouse_model->updatetechiremarks($sts, $ord_num);
        echo json_encode($resp);
    }
    public function addtechiremarkz()
    {
        $_POST['remarks_bdl'];
        $_POST['remarks_id'];
        $ordnum = $_POST['ord_nuum'];
        $id =  $this->inhouse_model->addNewTechiRemarkz($_POST['remarks_bdl'],  $_POST['remarks_id']);
        if ($id) {
            redirect('inhouse/view_alloted/' . base64_encode($ordnum));
        }
    }
    public function getcmplntremarks()
    {
        $id = $_POST['_num_ord'];
        $cmplnt = $this->db->select('remarks')->where('complaint_id', $id)->where('soft_id', $this->softid)->get('complaints')->row_array();
        echo json_encode($cmplnt);
    }
    public function finishtechiworks()
    {
        $ord_num =  $_POST['_num_ord'];
        $id = $this->inhouse_model->finishtechiworks($ord_num);
        echo json_encode($id);
    }
    public function addcallingdetails()
    {

        $mob =   $_POST['cal_mob'];
        $ordnum =   $_POST['ord_num'];
        $date =    $_POST['call_date'];
        $calling_person = $_POST['cl_person'];
        $sub =    $_POST['subj'];
        $remrk =    $_POST['cl_rmrk'];
        $cus_type =  $this->inhouse_model->getUserDetails($ordnum);
        $max_id =  $this->inhouse_model->getCallMAxID();
        $this->db->where('orderno', $ordnum)->delete('callstatus');
        $call = array(
            'call_id' =>  $max_id['ord_id'],
            'call_date' => $date,
            'caller' => $calling_person,
            'orderno' => $ordnum,
            'mobileno' => $mob,
            'type' => $cus_type,
            'subject' => $sub,
            'reason' => $remrk,
            'cur_status' => '',
            'soft_id' => $this->softid,
        );
        $this->db->insert('callstatus', $call);

        $id =  $this->db->insert_id();
        if ($id >= 0) {
            redirect('inhouse/view_alloted/' . base64_encode($ordnum));
        }
    }
    public function getcallingdetails()
    {
        $id = $_POST['_num_ord'];

        $cmplnt = $this->db->select('caller,subject,reason,call_date')->where('orderno', $id)->where('soft_id', $this->softid)->get('callstatus')->row_array();
        if ($cmplnt) {
            $dte = strtotime($cmplnt['call_date']);
            $dte = date('Y-m-d', $dte);
            echo json_encode(array('caller' => $cmplnt['caller'], 'subject' => $cmplnt['subject'], 'reason' => $cmplnt['reason'], 'date' => $dte));
        }
    }

    public function verification()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getFinishedInhouseOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('verification_list', $data);
        $this->load->view('layout/footer');
    }
    public function check_verification($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getAlottedOrdersForVerify($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('check_verify', $data);
        $this->load->view('layout/footer');
    }
    public function updatevefificationstatus()
    {
        $sts =    $_POST['_status'];
        $ord_num =  $_POST['_num_ord'];

        $resp =  $this->inhouse_model->updatevefificationstatus($sts, $ord_num);
        echo json_encode($resp);
    }
    public function recheck()
    {
        $ord_num =  $_POST['_num_ord'];
        $id = $this->inhouse_model->recheckworks($ord_num);
        echo json_encode($id);
    }
    public function delivered()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getDeliveredInhouseOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('delivered_lists', $data);
        $this->load->view('layout/footer');
    }
    public function unpaid()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getUnpaidorders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('unpaid_list', $data);
        $this->load->view('layout/footer');
    }
    public function cancl()
    {
        $ord_num =  $_POST['_num_ord'];

        $id = $this->inhouse_model->cancl($ord_num);
        echo json_encode($id);
    }
    public function addverifieddata()
    {
        $max_id =  $this->inhouse_model->getMaxVerifiedId();
        $num = $_POST['ord_num'];
        $finished_date =  $_POST['finished_date'];
        // $finished_amt =  $_POST['finished_amt'];
        $finished_remark =  $_POST['finished_remark'];
        $service_charge =  $_POST['service_charge'];
        $outsource =  $_POST['outsource'];
        $tallyamy =  $_POST['tallyamy'];
        $otherc =  $_POST['otherc'];

        $verified = array(
            'orderverifyId' => $max_id['ord_id'],
            'orderno' =>  $num,
            'serviceamt' => '',
            'otheramt' =>  $otherc,
            'outsourceamt' => '',
            'totalamt' => '',
            'discountamt' => 0,
            'netamt' => 0,
            'validate_date' => date('Y-m-d'),
            'remarks' => $finished_remark,
            'status' => 'Verified',
            'tallyamt' => '',
            'soft_id' =>  $this->softid,
        );
        $this->db->insert('orderverify', $verified);
        $this->db->set('status', 'Verified')->where('orderno', $num)->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status', 'Verified')->where('orderno', $num)->where('soft_id', $this->softid)->update(' orders');
        $id = $this->db->affected_rows();
        $id =  $this->db->insert_id();
        if ($id >= 0) {
            echo json_encode($num);
        }
    }
    public function sendsms()
    {
        $num = $_POST['evnt'];
        $ord_id =  $this->inhouse_model->selectmax_smsId();
        $smsdata =  $this->db->where('orderno', $num)->get('sendsms')->row_array();
        $dte = date('Y-m-d');


        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($smsdata['mobile']);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Dear Customer, Your order has been Verified Order No. : .' . $num . ' Date:' . $dte . '');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        $decode =  json_decode($response);
        if ($decode->status) {
            $sms = array(
                'smsId' => $ord_id['ord_id'],
                'mobile' =>  $smsdata['mobile'],
                'message' => 'Dear Customer, Your order has been Verified Order No. : .' . $num . ' Date:' . $dte . '',
                'orderno' => $num,
                'smstype' => 'Inhouse verify',
                'mgr_id' =>  $this->softid,

                'status' => $decode->status,
                'soft_id' =>  $this->softid,
            );
            $this->db->insert('sendsms', $sms);

            echo json_encode('success');
        }
    }
    public function issueItem()
    {
        $data = [];
        $data['order_no'] = $this->inhouse_model->getOrderNum();
        $data['customer'] = $this->inhouse_model->getInhouseCustomers();
        $data['group'] = $this->inhouse_model->getAllGroups();
        $data['problems'] = $this->inhouse_model->getproblems();
        $data['issued'] = $this->inhouse_model->getIssuedItems();

        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('issueitem', $data);
        $this->load->view('layout/footer');
    }
    public function getissueditemname()
    {
        $id =  $_POST['_brid'];
        $issued = $this->inhouse_model->getissueditemname($id);
        echo json_encode($issued);
    }
    public function add_issue_item()
    {

        $ordnum =  isset($_POST['Order']) ? $_POST['Order'] : '';
        $item =  isset($_POST['item']) ? $_POST['item'] : '';
        $id = isset($_POST['issued_item']) ? $_POST['issued_item'] : '';
        $price =  isset($_POST['price']) ? $_POST['price'] : '';

        $itemname = $this->inhouse_model->getissuedItemNameById($id);

        $issued = array(
            'item' =>  $item,
            'issueitem_id' => $id,
            'quantity' =>  isset($_POST['qty']) ? $_POST['qty'] : '',
            'issued_item' =>  $itemname,
            'serial_number' => '',
            'warranty_status' => '',
            'price' =>  $price,
            'issued_by' => $this->lgid,
            'issued_date' => $_POST['date'],
            'soft_id' => $this->softid,
            'orderno' =>  $ordnum,
        );
        $this->db->insert('issue_item', $issued);
        $id =  $this->db->insert_id();

        if ($id) {
            $this->session->set_flashdata('app_success', 'Added successfully');
            redirect('inhouse/issueItem');
        } else {
            $this->session->set_flashdata('app_error', 'Failed to add');
            redirect('inhouse/issueItem');
        }
    }
    public function addnewitems()
    {
        $issu['item_name'] = $_POST['itemname'];
        $issu['created_by'] =  $this->lgid;
        $issu['soft_id'] =  $this->softid;
        $this->db->insert('issue_item_name', $issu);
        $id =  $this->db->insert_id();
        if ($id) {
            redirect('inhouse/issueItem');
        }
    }
    public function delivery()
    {
        $data = [];
        $data['order_no'] = $this->inhouse_model->getOrderNum();
        $data['customer'] = $this->inhouse_model->getInhouseCustomers();
        $data['group'] = $this->inhouse_model->getAllGroups();
        $data['problems'] = $this->inhouse_model->getproblems();
        $data['issued'] = $this->inhouse_model->getIssuedItems();
        $data['orders'] = $this->inhouse_model->getVerifiedOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('delivery_list', $data);
        $this->load->view('layout/footer');
    }
    public function check_delivery($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('check_delivery', $data);
        $this->load->view('layout/footer');
    }
    public function return()
    {
        $ordnum = $_POST['_num_ord'];
        $data = $this->db->set('status',  'Return')->where('orderno',  $ordnum)->where('soft_id', $this->softid)->update(' orders');
        $rslt = $this->db->affected_rows();
        if ($rslt >= 0) {
            echo json_encode('success');
        }
    }
    public function adddelivery()
    {

        $max_id =  $this->inhouse_model->getMaxDeliveryId();
        $recievedby = $_POST['recieved_by'];
        $mobilenum =  $_POST['mobilenum'];
        $remarks =  $_POST['remarks'];
        $servicecharge =  $_POST['servicecharge'];
        $othercharge =  $_POST['othercharge'];
        $tallycharge =  $_POST['tallycharge'];
        $sparecost =  $_POST['sparecost'];
        $totalamount =  $_POST['totalamount'];
        $paidamount =  $_POST['paidamount'];
        if ($paidamount) {
            $paidamount =  $paidamount;
        } else {
            $paidamount = 0;
        }
        $discountamount =  $_POST['discountamount'];
        $balanceamount =  $_POST['balanceamount'];
        $netamount =  $_POST['netamount'];
        if (isset($_POST['unpaid'])) {
            $sts = 'Unpaid';
        } else {
            $sts = 'Delivered';
        }



        $delivery = array(
            'deliveryId' => $max_id['ord_id'],
            'orderno' => $_POST['ordnumb'],
            'receivedby' => $recievedby,
            'receivedmob' => $mobilenum,
            'serviceamt' => $servicecharge,
            'otheramt' => $othercharge,
            'totalamt' => $totalamount,
            'discountamt' => $discountamount,
            'paidamt' => $paidamount,
            'balamt' => $balanceamount,
            'spareamt' => $sparecost,
            'userid' => $this->lgid,
            'remarks' => $remarks,
            'soft_id' => $this->softid,
            'status' => $sts,
            'netamt' => $netamount,
            'tallyamount' => $tallycharge,


        );
        $maxodrc_id =  $this->inhouse_model->getMaxOfreceiptId();
        $maxorc_num =  $this->inhouse_model->getMaxOfreceiptNum();
        $delivery_rcpt = array(
            'receipt_id ' =>  $maxodrc_id['ord_id'],
            'receiptno' =>   $maxodrc_id['ord_id'],
            'orderno' => $_POST['ordnumb'],
            'receipttype' => 'INHOUSE',
            'outsource_amt' => '',
            'receipt_date' => date('Y-m-d'),
            'service_amt' => $servicecharge,
            'other_amt' => $othercharge,
            'grandtotal' => $totalamount,
            'disc_amt' => $discountamount,
            'paid_amt' => $paidamount,
            'bal_amt' => $balanceamount,
            'spare_amt' => $sparecost,
            'remarks' => $remarks,
            'soft_id' => $this->softid,
            'status' => $sts,
            'net_amt' => $netamount,
            'tallyamt' => $tallycharge,
            'emp_id' => $this->lgid,
            'soft_id' =>  $this->softid,
        );



        $num =  $_POST['ordnumb'];
        // sms

        $smsdata =  $this->db->where('orderno', $num)->get('sendsms')->row_array();
        $dte = date('Y-m-d');


        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($smsdata['mobile']);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Dear Customer, Your Product has  delivered. Order No. :' . $num . '');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        $decode =  json_decode($response);
        $ord_id =  $this->inhouse_model->selectmax_smsId();
        if ($decode->status) {
            $sms = array(
                'smsId' => $ord_id['ord_id'],
                'mobile' =>  $smsdata['mobile'],
                'message' => 'Dear Customer, Your Product has  delivered. Order No. :' . $num . '',
                // 'message' => 'Dear Customer, Your order has been Verified Order No. : .' . $num . ' Date:' . $dte . '',
                'orderno' => $num,
                'smstype' => 'Inhouse Delivery',
                'mgr_id' =>  $this->softid,

                'status' => $decode->status,
                'soft_id' =>  $this->softid,
            );
            $this->db->insert('sendsms', $sms);
        }


        // emd sms

        $dataz =  $this->db->select('orderno')->where('orderno', $_POST['ordnumb'])->get('inhousereceipts')->row_array();
        if ($dataz) {
            $this->db->where('orderno', $_POST['ordnumb'])->update('inhousereceipts', $delivery_rcpt);
            $this->db->where('orderno', $_POST['ordnumb'])->update('orderdelivery', $delivery);
        } else {
            $this->db->insert('inhousereceipts', $delivery_rcpt);
            $this->db->insert('orderdelivery', $delivery);
        }


        $this->db->set('status',  $sts)->where('orderno',  $_POST['ordnumb'])->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status',  $sts)->where('orderno',  $_POST['ordnumb'])->where('soft_id', $this->softid)->update(' orders');
        $id = $this->db->affected_rows();
        $onum = $_POST['ordnumb'];
        if ($id >= 0) {
            echo json_encode($onum);
        }
    }
    public function addotp()
    {
        $mobs =  $this->input->post('_mobs');
        $rnum =  $this->input->post('_rnum');
        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($mobs);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Dear Customer, Your OTP IS :' . $rnum . '');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        $decode =  json_decode($response);
        echo json_encode(array('status' => $decode->status, 'otp' => $rnum));
    }
    public function print_receipt()
    {
        $id = $_POST['resp'];
        $data['customer'] = $this->inhouse_model->getAdditionalOrddtls($id);
        $data['recpt'] = $this->inhouse_model->getreceiptdata($id);
        $this->load->view('inhouse_receipt', $data);
    }
    public function updateunpaiddelivery()
    {
        $max_id =  $this->inhouse_model->getMaxDeliveryId();
        $recievedby = $_POST['recieved_by'];
        $mobilenum =  $_POST['mobilenum'];
        $remarks =  $_POST['remarks'];
        $servicecharge =  $_POST['servicecharge'];
        $othercharge =  $_POST['othercharge'];
        $tallycharge =  $_POST['tallycharge'];
        $sparecost =  $_POST['sparecost'];
        $totalamount =  $_POST['totalamount'];
        $paidamount =  $_POST['paidamount'];
        if ($paidamount) {
            $paidamount =  $paidamount;
        } else {
            $paidamount = 0;
        }
        $discountamount =  $_POST['discountamount'];
        $balanceamount =  $_POST['balanceamount'];
        $netamount =  $_POST['netamount'];

        $delivery = array(
            // 'deliveryId' => $max_id['ord_id'],
            'orderno' => $_POST['ordnumb'],
            'receivedby' => $recievedby,
            'receivedmob' => $mobilenum,
            'serviceamt' => $servicecharge,
            'otheramt' => $othercharge,
            'totalamt' => $totalamount,
            'discountamt' => $discountamount,
            'paidamt' => $paidamount,
            'balamt' => $balanceamount,
            'spareamt' => $sparecost,
            'userid' => $this->lgid,
            'remarks' => $remarks,
            'soft_id' => $this->softid,
            'status' => 'Delivered',
            'netamt' => $netamount,
            'tallyamount' => $tallycharge,
        );

        $maxodrc_id =  $this->inhouse_model->getMaxOfreceiptId();
        $maxorc_num =  $this->inhouse_model->getMaxOfreceiptNum();
        $delivery_rcpt = array(
            'receipt_id ' =>  $maxodrc_id['ord_id'],
            'receiptno' =>   $maxodrc_id['ord_id'],
            'orderno' => $_POST['ordnumb'],
            'receipttype' => 'INHOUSE',
            'outsource_amt' => '',
            'receipt_date' => date('Y-m-d'),
            'service_amt' => $servicecharge,
            'other_amt' => $othercharge,
            'grandtotal' => $totalamount,
            'disc_amt' => $discountamount,
            'paid_amt' => $paidamount,
            'bal_amt' => $balanceamount,
            'spare_amt' => $sparecost,
            'remarks' => $remarks,
            'soft_id' => $this->softid,
            'status' => 'Delivered',
            'net_amt' => $netamount,
            'tallyamt' => $tallycharge,
            'emp_id' => $this->lgid,
            'soft_id' =>  $this->softid,
        );
        $this->db->where('orderno', $_POST['ordnumb'])->update('inhousereceipts', $delivery_rcpt);

        $this->db->where('orderno', $_POST['ordnumb'])->update('orderdelivery', $delivery);
        $this->db->set('status', 'Delivered')->where('orderno',  $_POST['ordnumb'])->where('soft_id', $this->softid)->update('allocatetechinician');
        $this->db->set('status', 'Delivered')->where('orderno',  $_POST['ordnumb'])->where('soft_id', $this->softid)->update(' orders');
        $id = $this->db->affected_rows();
        if ($id >= 0) {
            echo json_encode('Success');
        }
    }
    public function getdelaerlikecus()
    {
        $type = $_GET['_tpe'];
        $dealers = $this->inhouse_model->getdelaerlikecus($type);
        echo json_encode($dealers);
    }
    public function insertnewcustmr()
    {

        $_POST['c_nme'];
        $_POST['m_no'];
        $_POST['p_no'];
        $_POST['adrs'];
        $_POST['em_id'];
        $_POST['locaId'];
        $max_id =  $this->inhouse_model->getMaxOfCusId();
        $cus = array(
            'cust_id' => $max_id['ord_id'],
            'custname' => $_POST['c_nme'],
            'address' => $_POST['adrs'],
            'emailid' => $_POST['em_id'],
            'mobile' => $_POST['m_no'],
            'landline' => $_POST['p_no'],
            'mgr_id' =>  $this->lgid,
            'ordertype' => $_POST['_ctpe'],
            'place' => $_POST['locaId'],
            'worktype' => 'INHOUSE',
            'soft_id' => $this->softid,
        );

        $id =  $this->db->insert('customers', $cus);

        $this->db->insert_id();
        if ($id >= 0) {
            echo json_encode($_POST['_ctpe']);
        }
    }
    public function addbrandnew()
    {

        $_POST['brandname'];

        $max_id =  $this->inhouse_model->getMaxOfBrandId();
        $max_id_item =  $this->inhouse_model->getMaxOfBrandItemId();
        $brnd = array(
            'brand_id' => $max_id['ord_id'],
            'brandname' => $_POST['brandname'],
            'mgr_id' =>  $this->lgid,
            'soft_id' => $this->softid,
        );

        $brnditem = array(
            'branditem_id' => $max_id_item['ord_id'],
            'brand_id' => $max_id['ord_id'],
            'item' => $_POST['branditmz'],
            'mgr_id' =>  $this->lgid,
            'soft_id' => $this->softid,
        );


        $this->db->insert('brands', $brnd);
        $id = $this->db->insert_id();
        if ($id >= 0) {
            $this->db->insert('branditems', $brnditem);
            $id = $this->db->insert_id();
            if ($id >= 0) {
                echo  json_encode($_POST['branditmz']);
            } else {
                $this->db->where('brand_id', $max_id['ord_id'])->delete('brands');
            }
        }
    }
    public function addmodelnew()
    {
        $max_id =  $this->inhouse_model->getMaxOFmodelID();
        $model = array(
            'model_id' => $max_id['ord_id'],
            'modelname' => $_POST['brandname'],
            'branditem_id' => $_POST['branditmz'],
            'mgr_id' =>  $this->lgid,
            'soft_id' => $this->softid,
        );
        $this->db->insert('models', $model);
        echo  json_encode($_POST['branditmz']);
    }
    public function check_cancelled($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['delivered_ord'] = $this->inhouse_model->getDeliveredData($id);

        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('cancld', $data);
        $this->load->view('layout/footer');
    }
    public function check_delivered($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['delivered_ord'] = $this->inhouse_model->getDeliveredData($id);

        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('dlrvd', $data);
        $this->load->view('layout/footer');
    }
    public function addnewreturs()
    {
        $ord_id =  $this->inhouse_model->selectmax_refundid();
        $ordnum =   $_POST['_ordId'];
        $reason =  $_POST['_rtnid'];
        $amount =  $this->inhouse_model->getrefundAmountById($ordnum);
        $type =  $this->inhouse_model->getusertypes($ordnum);

        $refund = array(
            'refund_id ' => $ord_id['ord_id'],
            'refund_date ' => date('Y-m-d'),
            'orderno ' => $ordnum,
            'ordertype ' => $type['ordertype'],
            'refund_amt ' => $amount['netamt'],
            'emp_id ' =>  $this->lgid,
            'remarks ' => $reason,
            'soft_id ' =>  $this->softid,
        );
        $this->db->insert('refundorder', $refund);
        $this->db->where('orderno', $ordnum)->set('status', 'Refund')->update('orders');
        echo json_encode('success');
    }
    public function check_unpaid($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);
        $data['delivered_ord'] = $this->inhouse_model->getDeliveredData($id);
        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('unpd_view', $data);
        $this->load->view('layout/footer');
    }
    public function cancelled()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getCancelledInhouseOrders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('cancelled_lists', $data);
        $this->load->view('layout/footer');
    }
    public function returned()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getreturnedorders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('returnnedlists', $data);
        $this->load->view('layout/footer');
    }
    public function refund()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getrefundedlists();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('refundedlist', $data);
        $this->load->view('layout/footer');
    }
    public function check_refunded($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['delivered_ord'] = $this->inhouse_model->getDeliveredData($id);

        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('refunded', $data);
        $this->load->view('layout/footer');
    }
    public function check_retuened($id)
    {
        $id = base64_decode($id);
        $data = [];
        $data['delivered_ord'] = $this->inhouse_model->getDeliveredData($id);

        $data['alloted_list'] = $this->inhouse_model->getOrderDtlsForTechi($id);

        $data['alloted_orders'] = $this->inhouse_model->getDlryOrdrs($id);
        $data['charge'] = $this->inhouse_model->getDeliveryCharge($id);

        $data['ord_num'] = $id;
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('rtnd', $data);
        $this->load->view('layout/footer');
    }
    public function allorders()
    {
        $data = [];
        $data['alloted_list'] = $this->inhouse_model->getAllotedOrders();
        $data['orders'] = $this->inhouse_model->getAllorders();
        $this->load->view('layout/header');
        $this->load->view('layout/menu');
        $this->load->view('allorders', $data);
        $this->load->view('layout/footer');
    }
    public function addissue($id = null)
    {
        $data = [];

        $data['orders'] = $this->inhouse_model->getaddeditems();
        if ($id) {
            $data['added'] =  $this->db->where('issued_id', $id)->get('add_issue_item')->row_array();
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('addissue', $data);
            $this->load->view('layout/footer');
        } else {
            $data['added'] =  '';
            $this->load->view('layout/header');
            $this->load->view('layout/menu');
            $this->load->view('addissue', $data);
            $this->load->view('layout/footer');
        }
    }
    public function addissue_item()
    {
        $pname =  $this->input->post('productname');
        $price =  $this->input->post('price');
        $item =  $this->input->post('item');
        $itm['issued_name'] = $pname;
        $itm['price'] = $price;
        $itm['item'] = $item;
        $this->db->insert('add_issue_item', $itm);
        $ids =  $this->db->insert_id();
        if ($ids) {
            $this->session->set_flashdata('app_success', 'Added successfully');
            redirect('inhouse/addissue');
        } else {
            $this->session->set_flashdata('app_error', 'Failed to add');
            redirect('inhouse/addissue');
        }
    }
    public function update_item()
    {
        $pname =  $this->input->post('productname');
        $price =  $this->input->post('price');
        $impId =  $this->input->post('impId');
        $item =  $this->input->post('item');
        $itm['issued_name'] = $pname;
        $itm['price'] = $price;
        $itm['item'] = $item;
        $this->db->where('issued_id', $impId)->update('add_issue_item', $itm);
        $this->session->set_flashdata('app_success', 'Updated successfully');
        redirect('inhouse/addissue');
    }
    public function deleteissue($id)
    {
        $this->db->where('issued_id', $id)->delete('add_issue_item');
        $this->session->set_flashdata('app_success', 'Deleted successfully');
        redirect('inhouse/addissue');
    }
    public function getdatasforissue()
    {
        $id = $_GET['_ordnum'];
        $items = $this->inhouse_model->getdataFrIssue($id);
        echo json_encode($items);
    }
    public function getitemsitembyitem()
    {
        $itm = $_GET['_ordnum'];

        $itmz =   $this->inhouse_model->getissueItemByItem($itm);
        echo json_encode($itmz);
    }
    public function getpricesbyid()
    {
        $itm = $_GET['_ordnum'];
        $data =  $this->db->select('issued_id,issued_name,price')->where('issued_id', $itm)->get('add_issue_item')->row_array();
        echo json_encode($data);
    }
    public function addissuecalling()
    {
        $ordnum =  isset($_POST['ord_num']) ? $_POST['ord_num'] : '';

        $max_id =  $this->inhouse_model->getCallMAxID($ordnum);
        $usertpe =  $this->inhouse_model->getusertypebyId($ordnum);
        $call_date =  isset($_POST['call_date']) ? $_POST['call_date'] : '';
        $cal_mob =  isset($_POST['cal_mob']) ? $_POST['cal_mob'] : '';
        $cl_person =  isset($_POST['cl_person']) ? $_POST['cl_person'] : '';
        $subj = isset($_POST['subj']) ? $_POST['subj'] : '';
        $cl_rmrk = isset($_POST['cl_rmrk']) ? $_POST['cl_rmrk'] : '';

        $call = array(
            'call_id' =>  $max_id['ord_id'],
            'call_date' => $call_date,
            'caller' => $cl_person,
            'orderno' => $ordnum,
            'mobileno' => $cal_mob,
            'type' =>  $usertpe['ordertype'],
            'subject' => $subj,
            'reason' => $cl_rmrk,
            'cur_status' => 'Issue',
            'soft_id' => $this->softid,
        );
        $this->db->insert('callstatus', $call);

        $id =  $this->db->insert_id();
        if ($id >= 0) {
            $this->session->set_flashdata('app_success', 'Call added  successfully');
            redirect('inhouse/issueItem');
        }
    }
    public function print_ordrs()
    {
        $id = $_POST['resp'];

        $data['itemz'] = $this->inhouse_model->getProbLists($id);
        $data['customer'] = $this->inhouse_model->getAdditionalOrddtls($id);
        $this->load->view('inhouseone', $data);
    }
    public function checkorderexists()
    {
        $id = $_POST['_ordnum'];
        $sts = $this->db->where('orderno', $id)->where('soft_id',  $this->softid)->get('orders')->row_array();
        if ($sts) {
            echo json_encode('success');
        } else {
            echo json_encode('failed');
        }
    }
    public function addnewsms()
    {
        $num = $_POST['_mob'];
        $ordnum = $_POST['_ordnum'];

        $apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
        // Message details
        $numbers = array($num);
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('This is your message');

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // Process your response here
        //$response = array("balance"=>29,"status"=>"success");
        echo ($response);


        //  "{\"balance\":29,\"batch_id\":1378194848,\"cost\":1,\"num_messages\":1,\"message\":{\"num_parts\":1,\"sender\":\"TXTLCL\",\"content\":\"This is your message\"},\"receipt_url\":\"\",\"custom\":\"\",\"messages\":[{\"id\":\"11954858878\",\"recipient\":919746492136}],\"status\":\"success\"}"
    }
}
