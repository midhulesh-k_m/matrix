<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Inhouse Order Delivery</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            Technician Name:<b><?= $alloted_list['tech_name']; ?></b>
                        </div>
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row">


                            <div class="col-md-6">
                                Order NO:
                                <b><?= $ord_num; ?></b>
                            </div>
                            <div class="col-md-6">
                                Order Type:
                                <b><?= $alloted_list['ordertype']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Customer name:
                                <b><?= $alloted_list['custname']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Mobile Number:
                                <b><?= $alloted_list['mobile']; ?></b>
                            </div>
                            <div class="col-md-6">
                                Item:
                                <b><?= $alloted_list['item']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Brand: <b><?= $alloted_list['brandname']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Model: <b><?= $alloted_list['modelname']; ?></b>
                            </div>

                            <div class="col-md-6">
                                Warranty Status:
                                <b><?= $alloted_list['warranty']; ?></b>
                            </div>
                            <br><br>
                            <div class="col-md-6">
                                Serial No:
                                <b><?= $alloted_list['serialno']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Approx.Date:
                                <b><?= $alloted_list['appx_date']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Rough.Estimate:
                                <b><?= $alloted_list['rough_estimate']; ?></b>
                            </div><br><br>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <center>Repair Report</center>
                                    </div>
                                    <div class="card-body" style="">
                                        <div class="row">
                                            <div class="col-md-<?php if (isset($alloted_orders['spare'][0]['issued_item'])) {
                                                                    echo '6';
                                                                } else {
                                                                    echo '12';
                                                                } ?>">
                                                <div class="card">


                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>Problems</th>
                                                                <th>Status</th>
                                                                <th>Amount</th>
                                                                <th>Remarks</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($alloted_orders) {
                                                                foreach ($alloted_orders['cmplnt'] as $key => $k_val) { ?>
                                                                    <tr>
                                                                        <td><?= $k_val['probname']; ?></td>
                                                                        <td><?= $k_val['status']; ?></td>
                                                                        <td><span class="sp_servce"><?= $k_val['amount']; ?></span></td>
                                                                        <td><?= $k_val['remarks']; ?></td>
                                                                    </tr>

                                                                <?php } ?>


                                                            <?php } ?>
                                                            <tr></tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                            <?php if (isset($alloted_orders['spare'][0]['issued_item'])) { ?>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-primary">

                                                                <th>
                                                                    Spare Replaced
                                                                </th>
                                                                <th>
                                                                    Amount
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($alloted_orders['spare'] as $key => $k_val) { ?>
                                                                <tr>
                                                                    <td><?= $k_val['issued_item']; ?></td>

                                                                    <td><span class="sp_cost"><?= $k_val['price']; ?></span></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
                            <form method="post" id="verifiedmmodal" action="<?= base_url() ?>inhouse/adddelivery">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="col-md-12">
                                                Recieved By:<input disabled="" type="text" value="<?= $delivered_ord['receivedby']; ?>" name="recieved_by" class="form-control">
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        Mobile No:<input disabled="" type="text" value="<?= $delivered_ord['receivedmob']; ?>" name="mobilenum" class="form-control">
                                                    </div>
                                                    <!-- <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Send OTP</button>
                                                    </div> -->
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="row">
                                                    <!-- <div class="col-md-8">
                                                        Enter OTP:<input type="text" name="otp" class="form-control">
                                                    </div> -->
                                                    <!-- <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Check OTP</button>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                Remarks:
                                                <textarea disabled="" name="remarks" class="form-control"><?= $delivered_ord['remarks']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">

                                                <div class="card">
                                                    <div class="card-header bg-primary">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    Cost Details
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="col-md-12" style="margin-left: 100px;">
                                                                        <!-- <input id="Unpaid" name="unpaid" style="margin-right: 250px;" value="Unpaid" type="checkbox">Unpaid -->
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="col-md-12">
                                                                    <input type="hidden" name="ordnumb" value="<?= $ord_num; ?>">
                                                                    Service Charge:
                                                                    <input id="service_charge" type="text" readonly="" value="" name="servicecharge" class="form-control net">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Other Charge:
                                                                    <input disabled value="<?= $delivered_ord['otheramt']; ?>" id="" type="text" value="0" class="form-control " name="othercharge">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Tally Amount:
                                                                    <input type="text" disabled name="tallycharge" value="<?= $delivered_ord['tallyamount']; ?>" class="form-control ">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <b> Total</b>:
                                                                    <input type="text" name="totalamount" readonly="" value="" class="form-control grand">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Spare Cost:
                                                                    <input type="text" name="sparecost" readonly="0" class="form-control sp_charge ">
                                                                </div>



                                                            </div>
                                                            <div class="col-md-6">

                                                                <div class="col-md-12">
                                                                    Paid:
                                                                    <input disabled name="paidamount" id="" type="text" class="form-control " value="<?= $delivered_ord['paidamt']; ?>">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Discount:
                                                                    <input disabled name="discountamount" value="<?= $delivered_ord['discountamt']; ?>" id="" type="text" class="form-control">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    Balance:
                                                                    <input readonly="" value="<?= $delivered_ord['balamt']; ?>" name="balanceamount" id="" type="text" class="form-control">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <b> Net Amount:</b>
                                                                            <input name="netamount" type="text" readonly="" class="form-control" disabled value="<?= $delivered_ord['netamt']; ?>">
                                                                        </div>
                                                                        <!-- <div class="col-md-4">
                                                                            <br>
                                                                            <button class="btn btn-primary cal_ttl">Calculate</button>
                                                                        </div> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-12">
                                <div style="margin-right: -200px;" class="col-md-6 pull-right">

                                    <button class="btn btn-primary" data-ord_id="<?= $ord_num; ?>" id="call_customer_receipt">print</button>
                                    <button class="btn btn-primary refund" data-rfnd="<?= $ord_num; ?>" id="call_customer">Refund</button>

                                </div>
                            </div>

                        </div>



                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<script>
    // $("#paid").change(function(e) {
    //     var _pval = $(this).val();
    //     var _netamount = $('#netamount').val();
    //     var balc = parseFloat(_netamount) - parsefloat(_pval);
    //     $("#balance").val(balc);
    // });

    $(".paid").change(function(e) {
        var _paid = $(this).val();
        var nettotals = $('.netamount').val();

        var _blnce = parseFloat(nettotals) - parseFloat(_paid);
        $("#balance").val(_blnce);
        if (_paid == '' || _paid == 0) {
            $("#balance").val('');
        }
    });
    $(function() {


        // total service charge
        var _srvcst = 0;
        $('.sp_servce').each(function() {
            var _srvcsts = $(this).text();
            _srvcst += parseFloat(_srvcsts);
        });

        $('#service_charge').val(_srvcst);


        var _spcst = 0;
        $('.sp_cost').each(function() {
            var _ccost = $(this).text();

            _spcst += parseFloat(_ccost);
        });

        $('.sp_charge').val(_spcst);
        var _gt = 0;
        $('.net').each(function() {
            var _ttzl = $(this).val();

            _gt += parseFloat(_ttzl);
        });

        $('.grand').val(_gt);
        var _nttl = parseFloat(_spcst) + parseFloat(_gt);
        $('.netamount').val(_nttl);

        // 
        $("#othercharge").keyup(function() {
            var _otvrge = $(this).val();
            var _nttamts = $('.netamount').val();
            var _grand = $('#service_charge').val();

            var _ttlover = parseFloat(_grand) + parseFloat(_otvrge);
            $('.grand').val(_ttlover);

            var _spcrg = $('.sp_charge ').val();
            var _gdc = $('.grand ').val();

            var _tlnet = parseFloat(_spcrg) + parseFloat(_gdc);

            var _ttlznet = parseFloat(_nttamts) + parseFloat(_otvrge);
            $('.netamount').val(_tlnet);


            if (_otvrge == '' || _otvrge == 0) {
                $('.grand').val(_grand);
                var _spcrg = $('.sp_charge ').val();
                var _gdc = $('.grand ').val();

                var _tlnet = parseFloat(_spcrg) + parseFloat(_gdc);
                $('.netamount').val(_tlnet);
            }
        });


        $(".cal_ttl").click(function(e) {
            e.preventDefault();
            var _totalamt = $(".grand").val();
            var _discoutamt = $("#discount").val();
            if (_discoutamt != '') {
                var _discoutamt = _discoutamt;
            } else {
                var _discoutamt = 0;
            }
            var three = parseFloat(_totalamt) - parseFloat(_discoutamt);

            $('.netamount').val(three);
        });

    });



    $("#discount").change(function(e) {
        var _discount = $(this).val();
        var _paid = $("#paid").val();
        var nettotals = $('#balance').val();


        var _blnce12 = parseFloat(nettotals) - parseFloat(_discount);

        // alert(_blnce);
        $("#balance").val(_blnce12);
        if (_discount == 0 || _discount == '') {
            var _paid = $("#paid").val();
            var _ntamt = $('.netamount').val();
            var _chng_blc = parseFloat(_ntamt) - parseFloat(_paid);
            $("#balance").val(_chng_blc);
        }
    });
</script>

<!-- Message Modal -->
<script>
    $('.refund').click(function(e) {
        var _rfnd = $(this).data('rfnd');
        // $("#mdl_remark").val(_num_ord);





        $("#exampleModal3").modal('show');
    });


    $("#clct").click(function(e) {
        e.preventDefault();
        var _amtz = 0;
        $('.c_amt').each(function(k, valz) {

            var ttl = $(this).val();
            if (ttl == '') {
                ttl == 0;
            }
            _amtz += parseFloat(ttl);
        });
        $("#g_total").val(_amtz);
    });
</script>
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Refund</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="col-md-12">
                    Remarks:
                    <textarea name="remarks_bdl" class="form-control" id="rtnrmrk"></textarea>

                    <input type="hidden" name="ord_nuum" id="rtnordid" value="<?= $ord_num; ?>">
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary refndbtnszzz">Refund</button>
            </div>

        </div>
    </div>
</div>
<script>
    $(".refndbtnszzz").click(function(e) {
        // e.preventDefault();
        var _rtnid = $("#rtnrmrk").val();
        var _ordId = $("#rtnordid").val();
        if (_rtnid != '') {
            _rtnid = _rtnid;
        } else {
            _rtnid = '';
        }
        $.ajax({
            url: '<?= base_url() ?>inhouse/addnewreturs',
            method: 'post',
            data: {
                _ordId: _ordId,
                _rtnid: _rtnid,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert('success');
                    window.location.href = "<?= base_url() ?>inhouse/delivered";
                }
            }
        });
    });
</script>

<!-- // calling customer  -->

<script>
    $("#call_customer").click(function(e) {
        e.preventDefault();
        var _num_ord = "<?= $ord_num; ?>";
        var _mobile = "<?= $alloted_list['mobile']; ?>";
        $("#ord_num_mobile").val(_mobile);
        $("#ord_num_m").val(_num_ord);

        $.ajax({
            url: '<?= base_url() ?>inhouse/getcallingdetails',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    console.log(evnt);
                    $("#cl_date").val(evnt.date);
                    $("#subj").val(evnt.subject);
                    $("#cl_person").val(evnt.caller);
                    $("#cl_rmrk").text(evnt.reason);
                }
            }
        });

        $("#exampleModal4").modal('show');
    });
</script>


<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addcallingdetails">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_m">
                            </div>
                            <div class="col-md-3">
                                Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="call_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Mobile:
                                <input class="form-control" type="text" readonly="" name="cal_mob" id="ord_num_mobile">
                            </div>
                            <div class="col-md-3">
                                Calling Person:
                                <input class="form-control" type="text" name="cl_person" id="cl_person">
                            </div>
                            <div class="col-md-3">
                                Subject:
                                <input class="form-control" type="text" name="subj" id="subj">
                            </div>
                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="cl_rmrk"></Textarea>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Call Status</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
    //unpaid

    $("#Unpaid").click(function() {
        if ($(this).prop('checked') == true) {
            var _ntamt = $('.netamount').val();

            $('.paid').val(0);
            $('#balance').val(_ntamt);
            $('#discount').val(0);

        } else {

            $('#balance').val('');
        }
    })


    $(function() {
        var _stz = '';
        $(".ord_sts option:selected").each(function(k, valz) {
            var _c_sts = $(this).val();

            if (_c_sts === 'Allocated') {
                $(".Finished").prop('disabled', true);
            }
            if (_c_sts === 'Reparing') {
                $(".Finished").prop('disabled', true);
            }
        });

    });
</script>

<script>
    $(".verifyed").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('option:selected', this).data('verifyedby');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatevefificationstatus',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    location.reload(true);
                }
            }
        });
    });
    // amount
    $(".estimate").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $(this).data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiectimate',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // remarks
    $("#rmrks").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('#estimate').data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiremarks',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // recheck
    function recheck(e) {
        // e.preventDefault();
        var _num_ord = $('#Recheck').data('ord_num');

        if (confirm('Confirm ?')) {
            $.ajax({
                url: '<?= base_url() ?>inhouse/recheck',
                method: 'post',
                data: {

                    _num_ord: _num_ord,
                },
                dataType: 'json',
                success: function(evnt) {
                    if (evnt != '') {
                        alert("Success");
                        window.location.href = "<?= base_url() ?>inhouse/verification";
                    }
                }
            });
        }
    }
    //finished
    function finishorder(e) {
        // e.preventDefault();
        var _num_ord = $('#Finished').data('ord_num');
        var _total = $('#g_total').val();
        var _service_charge = $('#service_charge').val();
        var _outs_charge = $('#outsu_charge').val();
        var _tallyamt = $('#tally_amt').val();
        var _other = $('#othe_charge').val();
        var html = '';


        html += '<input type="hidden" name="service_charge" id="service_charge" value="' + _service_charge + '" >';
        html += ' <input type="hidden" name="outsource" id="outsource" value="' + _outs_charge + '" >';
        html += ' <input type="hidden" name="tallyamy" id="tallyamy" value="' + _tallyamt + '"  >';
        html += ' <input type="hidden" name="otherc" id="other" value="' + _other + '" >';
        $(".allother").html(html);
        $("#amtzzz").val(_total);
        $("#ord_num_mz").val(_num_ord);
        $("#exampleModal44").modal('show');
    }
</script>




<script>
    $("#verified_btn_mdl").click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '<?= base_url() ?>inhouse/adddelivery',
            method: 'post',
            data: $("#verifiedmmodal").serialize(),
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    if (confirm('Do You want to send sms?  ?')) {
                        alert('sms send');
                        window.location.href = "<?= base_url() ?>inhouse/delivery";
                    } else {
                        window.location.href = "<?= base_url() ?>inhouse/delivery";
                    }
                }
            }
        });
    })
</script>

<script>
    $("#call_customer_receipt").click(function(e) {
        e.preventDefault();
        var resp = $(this).data('ord_id');

        $.ajax({
            url: '<?= base_url() ?>inhouse/print_receipt',
            method: 'post',
            data: {
                resp: resp,
            },
            success: function(response) {
                Popup(response);
            }
        });
    });
</script>

<script>
    function Popup(data) {
        var base_url = $("#urlid").val();
        var frame1 = $("<iframe />");
        frame1[0].name = "frame1";
        frame1.css({
            position: "absolute",
            top: "-1000000px",
        });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ?
            frame1[0].contentWindow :
            frame1[0].contentDocument.document ?
            frame1[0].contentDocument.document :
            frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write("<html>");
        frameDoc.document.write("<head>");
        frameDoc.document.write("<title></title>");
        frameDoc.document.write(
            '<link rel="stylesheet" href="' +
            base_url +
            'templat/dist/css/adminlte.min.css">'
        );
        frameDoc.document.write("</head>");
        frameDoc.document.write("<body>");
        frameDoc.document.write(data);
        frameDoc.document.write("</body>");
        frameDoc.document.write("</html>");
        frameDoc.document.close();
        setTimeout(function() {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);

        return true;
    }
</script>