<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">
<link href="<?= base_url() ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Allote Technician</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <?php if ($success = $this->session->flashdata('app_error')) : ?>
                    <div class="alert alert-danger alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <h6> <?php echo $success ?></h6>
                        <strong></strong>
                    </div>
                <?php endif ?>
                <?php if ($success = $this->session->flashdata('app_success')) : ?>
                    <div class="alert alert-success alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <?php echo $success ?>
                        <strong></strong>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Add Item
                    </div>
                    <?php if ($added) { ?>
                        <form method="post" action="<?= base_url() ?>inhouse/update_item">
                        <?php } else { ?>
                            <form method="post" action="<?= base_url() ?>inhouse/addissue_item">
                            <?php } ?>

                            <div class="crad-body" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-md-4" style="margin-left:10px;">
                                        Product Name:
                                        <input required="" type="text" value="<?= isset($added['issued_name']) ? $added['issued_name'] : ''; ?>" name="productname" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        Price:
                                        <input required="" type="text" value="<?= isset($added['price']) ? $added['price'] : ''; ?>" name="price" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <br> Item:
                                        Laptop<input name="item" id="items" <?php if (($added['item']) && ($added['item'] == 'Laptop')) {
                                                                                echo 'checked';
                                                                            } ?> value="Laptop" type="checkbox" class="lap items">&nbsp &nbsp &nbsp &nbspDesktop<input id="items" name="item" value="Desktop" <?php if (($added['item']) && ($added['item'] == 'Desktop')) {
                                                                                                                                                                                                                    echo 'checked';
                                                                                                                                                                                                                } else {

                                                                                                                                                                                                                    echo '';
                                                                                                                                                                                                                }  ?> type="checkbox" class="desk items">&nbsp &nbsp &nbsp &nbsp Gadget<input id="items" <?php if (($added['item']) && ($added['item'] == 'Gadget')) {
                                                                                                                                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                                                                                                                                            } else {

                                                                                                                                                                                                                                                                                                                echo '';
                                                                                                                                                                                                                                                                                                            }  ?> value="Gadget" name="item" type="checkbox" class="gad items">
                                    </div>
                                    <div class="col-md-1">
                                        <br>
                                        <?php if ($added) { ?>
                                            <input type="hidden" name="impId" value="<?= $added['issued_id']; ?>">
                                            <button class="btn btn-info">Update</button>
                                        <?php } else { ?>
                                            <button class="btn btn-info">Add</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            </form>
                </div>

                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Slno</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>item</th>
                                <th>Action</th>
                            </tr>

                            <?php if ($orders) {
                                $i = 1;
                                foreach ($orders as $keys => $ord_val) { ?>
                                    <tr>
                                        <td>
                                            <?= $i++ ?>
                                        </td>
                                        <td>
                                            <?= $ord_val['issued_name'] ?>
                                        </td>
                                        <td>
                                            <?= $ord_val['price'] ?>
                                        </td>
                                        <td>
                                            <?= $ord_val['item'] ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>inhouse/addissue/<?= $ord_val['issued_id']; ?>" class="btn btn-info">Edit</a>
                                            <a href="<?= base_url() ?>inhouse/deleteissue/<?= $ord_val['issued_id']; ?>" class="btn btn-info">Delete</a>
                                        </td>
                                    </tr>
                            <?php  }
                            } ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>


<script>
    $('.lap').click(function() {
        $(this).prop('checked', true);


        $('.desk').prop('checked', false);
        $('.gad').prop('checked', false);
    });
    $('.desk').click(function() {
        $(this).prop('checked', true);


        $('.lap').prop('checked', false);
        $('.gad').prop('checked', false);
    });
    $('.gad').click(function() {
        $(this).prop('checked', true);


        $('.lap').prop('checked', false);
        $('.desk').prop('checked', false);
    });
</script>