<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">
<link href="<?= base_url() ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Allote Technician</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#exampleModal3">Add User</button>
                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="row">
                        <div class="col-xl-12">



                            <div class="table-responsive">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="example1" class="table table-striped table-bordered w-100 text-nowrap display dataTable no-footer" role="grid" aria-describedby="example1_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th>Customer Name</th>
                                                        <!-- <th>Brand</th> -->
                                                        <!-- <th>Model</th> -->
                                                        <th>Order No</th>
                                                        <th>Order Date</th>
                                                        <th>Order Type</th>
                                                        <th>Item</th>
                                                        <!-- <th>Warranty</th>
                                                        <th>Remarks</th> -->
                                                        <th>Approx.Date</th>
                                                        <th>Finished.Date</th>
                                                        <th>Estimate</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php if ($orders) foreach ($orders as $keys => $ord_val) { ?>

                                                        <tr>
                                                            <td><?= $ord_val['custname']; ?> </td>
                                                            <!-- <td><?= $ord_val['brandname']; ?> </td>
                                                            <td><?= $ord_val['modelname']; ?> </td> -->
                                                            <td><?= $ord_val['orderno']; ?> </td>
                                                            <td><?php $fte =  $ord_val['order_date'];
                                                                $fte = strtotime($fte);
                                                                echo date('Y-m-d', $fte); ?> </td>
                                                            <td><?= $ord_val['ordertype']; ?> </td>
                                                            <td><?= $ord_val['item']; ?> </td>
                                                            <!-- <td><?= $ord_val['warranty']; ?> </td>
                                                            <td><?= $ord_val['remarks']; ?> </td> -->
                                                            <td><?php $fte = $ord_val['appx_date'];
                                                                $fte = strtotime($fte);
                                                                echo date('Y-m-d', $fte); ?> </td>
                                                            <td><?php $fte = $ord_val['finished_date'];
                                                                $fte = strtotime($fte);
                                                                echo date('Y-m-d', $fte); ?> </td>
                                                            <td><?= $ord_val['rough_estimate']; ?> </td>
                                                            <td><?= $ord_val['status']; ?> </td>
                                                            <td><a href="<?= base_url() ?>inhouse/check_delivered/<?= base64_encode($ord_val['orderno']); ?>" class="btn btn-info btn-sm">View</a> </td>

                                                        </tr>
                                                    <?php } ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">

                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                                <ul class="pagination">


                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>

<script>
    $('.btn_assgn').click(function(e) {
        e.preventDefault();
        var prob = new Array();
        var _probHtml = ''
        var html = '';
        _brandname = $(this).data('brand_name');
        _modelname = $(this).data('model_name');
        _warranty = $(this).data('warranty');
        _approxdate = $(this).data('approxdate');
        _ord_num = $(this).data('ord_num');
        _estimate = $(this).data('estimate');

        //getproblems
        $.ajax({
            url: '<?= base_url() ?>inhouse/getproblems',
            method: 'post',
            data: {
                _ord_num: _ord_num,
            },
            dataType: 'json',
            success: function(response) {
                responsedata(response);

            }
        });

        function responsedata(data) {
            $.each(data, function(ks, vls) {
                html += '<span style="color:red";><h5>' + vls.probname + ',</h5></span><br>';
            });
            $('.cmplnts').html(html);
        }

        $('.modal-title').text(_ord_num);
        $('.brnad').text(_brandname);
        $('.model').text(_modelname);
        $('.warranty').text(_warranty);
        $('.approx').text(_approxdate);
        $('.estimate').text(_estimate);

        $("#exampleModal3").modal('show');

    });
</script>
<!-- Message Modal -->
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">brand Name:</label>
                                    <h4 class="brnad"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Model Name:</label>
                                    <h4 class="model"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Warranty:</label>
                                    <h4 class="warranty"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Approx Date:</label>
                                    <h4 class="approx"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Estimate:</label>
                                    <h4 class="estimate"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Complaints:</label>
                                    <h4 class="cmplnts"></h4>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Select Technician:</label>
                                    <select name="technician_id" class="form-control select2-show-search technician_id">
                                        <option value="">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</option>
                                        <?php if ($tech) {
                                            foreach ($tech as $key5 => $t_val) { ?>
                                                <option value="<?= $t_val['techn_id']; ?>"><?= $t_val['tech_name']; ?></option>
                                        <?php }
                                        } ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="assign_tech" type="button" class="btn btn-primary">Assign Technician</button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- allocation -->

<script>
    $("#assign_tech").click(function(e) {
        e.preventDefault();
        _approxdate = $('.approx').text();
        _ord_num = $('.modal-title').text();
        _estimate = $('.estimate').text();
        _technician_id = $('.technician_id').val();

        $.ajax({
            url: '<?= base_url() ?>inhouse/alocatetechi',
            method: 'post',
            data: {
                _approxdate: _approxdate,
                _ord_num: _ord_num,
                _estimate: _estimate,
                _technician_id: _technician_id,
            },
            dataType: 'json',
            success: function(response) {
                var html = '';
                if (response != '') {
                    $("#exampleModal3").modal('hide');
                }
            }
        });

    });
</script>