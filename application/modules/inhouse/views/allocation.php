<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Order Allocation</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <!-- <div class="col-md-12">
                        <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#exampleModal3">Add Technitian</button>
                    </div> -->
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row  table-responsive">

                            <table class="table table-responsive">
                                <tr>
                                    <td>Order NO:
                                        <input type="text" class="form-control"></td>
                                    <td>Order From:
                                        <input type="text" class="form-control"></td>

                                </tr>
                                <tr>
                                    <td>Order Date:
                                        <input type="date" class="form-control"></td>
                                    <td>Customer name:
                                        <input style="width: 500px;" type="text" class="form-control"></td>


                                </tr>
                                <tr>
                                    <td>Order Type:
                                        <input type="text" class="form-control"></td>
                                    <td>Address:

                                        <textarea class="form-control"></textarea></td>


                                </tr>
                                <tr>
                                    <td>Mobile Number:
                                        <input class="form-control" type="text"></td>
                                    <td>Item:
                                        <input class="form-control" type="text"></td>
                                </tr>
                                <tr>


                                    <td>Phone:
                                        <input type="text" class="form-control"></td>
                                    <td>Brand: <input type="text" class="form-control"></td>
                                </tr>
                                <tr>

                                    <td>Model: <input type="text" class="form-control"></td>
                                    <td>Email:
                                        <input type="text" class="form-control"></td>

                                </tr>

                                <tr>
                                    <td>Landine: <input type="text" class="form-control"></td>
                                    <td>Warranty Status:
                                        <input class="form-control" type="text"></td>

                                </tr>
                                <tr>
                                    <td>Serial No:
                                        <input class="form-control" type="text"></td>
                                </tr>
                                <tr>

                                    <td>
                                        <div class="card" style="width: 500px;">
                                            <div class="card-header bg-info">
                                                Preliminary Inspection
                                            </div>
                                            <div class="card-body" style="min-height: 50px;border:1px solid black;"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="card" style="width: 500px;">
                                            <div class="card-header bg-success">
                                                Complaints/Problems
                                            </div>
                                            <div class="card-body" style="min-height: 50px;border:1px solid black;"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Remarks:
                                        <textarea class="form-control"></textarea>
                                    </td>
                                    <td>
                                        Allotment for technician<br>
                                        <select class="form-control">
                                            <option>texh 1</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rough Estimate For Repair/Replacement:<input type="text" class="form-control">
                                    </td>
                                    <td>
                                        Approx date For Collection:<input type="date" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td> <button type="button" class="btn btn-info ">Reject</button>
                                        <button type="button" class="btn btn-info ">Call</button>
                                        <button type="button" class="btn btn-info ">Save</button>
                                        <button type="button" class="btn btn-info ">Cancel</button>
                                    </td>

                                </tr>

                            </table>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<!-- Message Modal -->
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">New Technician</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Technician name:</label>
                                    <input type="text" name="tech_name" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Technician Address:</label>
                                    <textarea name=tech_address" class="form-control" id="message-text"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Date of birth:</label>
                                    <input name="tech_dob" type="date" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Gender:</label>
                                    <input name="tech_grnder" type="radio" value="male" class="" id="">Male <input name="emp_grnder" type="radio" value="female" class="" id="">Female
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Email:</label>
                                    <input name="tech_email" type="email" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Mobile No:</label>
                                    <input name="tech_mob" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Landline No:</label>
                                    <input type="text" name="tech_landline" class="form-control" id="recipient-name">
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Place:</label>
                                    <input name="tech_place" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">User Name:</label>
                                    <input name="emp_username" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Password:</label>
                                    <input name="password" type="text" class="form-control" id="recipient-name">
                                </div>
                                <div class="form-group">
                                    <!-- <label for="recipient-name" class="form-control-label">User Type:</label>
                                    <select name="tech_type" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                        foreach ($u_types as $key => $u_val) { ?>
                                            <option value="<?= $u_val['utype_id']; ?>_<?= $u_val['mgr_id']; ?>"><?= $u_val['user_type']; ?></option>
                                        <?php }
                                        ?>

                                    </select>
                                </div> -->
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Category:</label>
                                        <select name="tech_cat" class="form-control">
                                            <option value="">Select</option>
                                            <option value="inhouse">In House</option>
                                            <option value="onsite">On Site</option>
                                            <option value="outsource">Out Source</option>
                                            <option value="refill">Refil</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">Remarks:</label>
                                        <textarea name="remarks" class="form-control" id="message-text"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Add Technician</button>
            </div>
        </div>
    </div>
</div>