<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Inhouse Order Delivery</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">


            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            Technician Name:<b><?= $alloted_list['tech_name']; ?></b>
                        </div>
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row">


                            <div class="col-md-6">
                                Order NO:
                                <b><?= $ord_num; ?></b>
                            </div>
                            <div class="col-md-6">
                                Order Type:
                                <b><?= $alloted_list['ordertype']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Customer name:
                                <b><?= $alloted_list['custname']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Mobile Number:
                                <b><?= $alloted_list['mobile']; ?></b>
                            </div>
                            <div class="col-md-6">
                                Item:
                                <b><?= $alloted_list['item']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Brand: <b><?= $alloted_list['brandname']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Model: <b><?= $alloted_list['modelname']; ?></b>
                            </div>

                            <div class="col-md-6">
                                Warranty Status:
                                <b><?= $alloted_list['warranty']; ?></b>
                            </div>
                            <br><br>
                            <div class="col-md-6">
                                Serial No:
                                <b><?= $alloted_list['serialno']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Approx.Date:
                                <b><?= $alloted_list['appx_date']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Rough.Estimate:
                                <b><?= $alloted_list['rough_estimate']; ?></b>
                            </div><br><br>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <center>Repair Report</center>
                                    </div>
                                    <div class="card-body" style="">
                                        <div class="row">
                                            <div class="col-md-<?php if (isset($alloted_orders['spare'][0]['issued_item'])) {
                                                                    echo '6';
                                                                } else {
                                                                    echo '12';
                                                                } ?>">
                                                <div class="card">


                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-info">
                                                                <th>Problems</th>
                                                                <th>Status</th>
                                                                <th>Amount</th>
                                                                <th>Remarks</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($alloted_orders) {
                                                                foreach ($alloted_orders['cmplnt'] as $key => $k_val) { ?>
                                                                    <tr>
                                                                        <td><?= $k_val['probname']; ?></td>
                                                                        <td><?= $k_val['status']; ?></td>
                                                                        <td><span class="sp_servce"><?= $k_val['amount']; ?></span></td>
                                                                        <td><?= $k_val['remarks']; ?></td>
                                                                    </tr>

                                                                <?php } ?>


                                                            <?php } ?>
                                                            <tr></tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                            <?php if (isset($alloted_orders['spare'][0]['issued_item'])) { ?>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr class="bg-primary">

                                                                <th>
                                                                    Spare Replaced
                                                                </th>
                                                                <th>
                                                                    Amount
                                                                </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($alloted_orders['spare'] as $key => $k_val) { ?>
                                                                <tr>
                                                                    <td><?= $k_val['issued_item']; ?></td>

                                                                    <td><span class="sp_cost"><?= $k_val['price']; ?></span></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
                            <form method="post" id="verifiedmmodal" action="<?= base_url() ?>inhouse/adddelivery">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <!-- <div class="col-md-12">
                                                Recieved By:<input disabled="" type="text" value="<?= isset($delivered_ord['receivedby']) ? $delivered_ord['receivedby'] : ''; ?>" name="recieved_by" class="form-control">
                                            </div> -->
                                            <!-- <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        Mobile No:<input disabled="" type="text" value="<?= $delivered_ord['receivedmob']; ?>" name="mobilenum" class="form-control">
                                                    </div>
                                                    <!-- <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Send OTP</button>
                                                    </div> -->
                                        </div>
                                    </div>


                                    <!-- <div class="col-md-12">
                                                <div class="row">
                                                    <!-- <div class="col-md-8">
                                                        Enter OTP:<input type="text" name="otp" class="form-control">
                                                    </div> -->
                                    <!-- <div class="col-md-4">
                                                        <br>
                                                        <button class="btn btn-info">Check OTP</button>
                                                    </div> -->
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>