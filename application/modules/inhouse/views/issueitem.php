<!-- app-content-->
<!--Select2 css -->
<link href="<?= base_url() ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/plugins/formwizard/smart_wizard_theme_dots.css" rel="stylesheet">
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Issue Item</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <?php if ($success = $this->session->flashdata('app_error')) : ?>
                    <div class="alert alert-danger alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <h6> <?php echo $success ?></h6>
                        <strong></strong>
                    </div>
                <?php endif ?>
                <?php if ($success = $this->session->flashdata('app_success')) : ?>
                    <div class="alert alert-success alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <?php echo $success ?>
                        <strong></strong>
                    </div>
                <?php endif ?>
            </div>
            <div class="card">

                <div class="card-body">

                    <div class="sw-container tab-content" style="min-height: 223.375px;">
                        <div id="step-10" class="tab-pane step-content" style="display: block;">
                            <form id="" method="post" action="<?= base_url() ?>inhouse/add_issue_item">

                                <div class="row">



                                    <div class="col-md-3">
                                        Order num:
                                        <input type="text" name="Order" id="Order" value="" class="form-control">
                                    </div>
                                    <div class="col-md-3">
                                        <br> Item:
                                        Laptop<input name="item" id="items" value="Laptop" type="checkbox" class="lap items">&nbsp &nbsp &nbsp &nbspDesktop<input id="items" name="item" value="Desktop" type="checkbox" class="desk items">&nbsp &nbsp &nbsp &nbsp Gadget<input id="items" value="Gadget" name="item" type="checkbox" class="gad items">
                                    </div>


                                    <div class="col-md-3">

                                        Issue Item:

                                        <select style="width: 100%;" name="issued_item" class="form-control  issued_itemxyz" id="issued_item">


                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        Quantity:
                                        <input type="text" name="qty" id="qty" value="1" require="" class="form-control">
                                        <input type="hidden" name="qty_price" id="qty_price" value="" class="form-control">
                                    </div>
                                    <div class="col-md-3">
                                        Price:
                                        <input require="" type="text" name="price" id="price" value="" class="form-control">
                                    </div>

                                    <div class="col-md-3">
                                        Customer name:
                                        <input name="cname" id="cname" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-3">
                                        Mobile:
                                        <input name="mobile" id="mobile" type="text" class="form-control ord_num_mobile">
                                    </div>

                                    <div class="col-md-3">
                                        Issue Date:
                                        <input type="date" name="date" value="<?= date('Y-m-d') ?>" class="form-control">
                                    </div>

                                </div>
                                <hr>
                                <div class="col-md-12 complaints">

                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-info" id="call_customer">Call</button>
                                    <button class="btn btn-info" id="call_customer">Call</but <button class="btn btn-info pull-right mt-4" type="submit">Issue Item</button>
                                </div>
                            </form>
                        </div>

                        <div id="step-12" class="tab-pane step-content" style="display: none;">
                            <div class="checkbox">
                                <div class="custom-checkbox custom-control">
                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox2">
                                    <label for="checkbox2" class="custom-control-label">I agree terms &amp; Conditions</label>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js">
            </script> -->
<!--Select2 js -->



<!-- Message Modal -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">New Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addnewitems">
                    <div class="col-md-12">
                        Item Name:
                        <input type="text" name="itemname" class="form-control">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Item</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>

<script>
    $('.dealer').click(function() {
        if ($('.end_user').prop('checked') == true) {
            $('.end_user').prop('checked', false);
            $(this).prop('checked', true);

        } else {

            $(this).prop('checked', true);
        }
    });
</script>

<script>
    $('.end_user').click(function() {
        if ($('.dealer').prop('checked') == true) {
            $('.dealer').prop('checked', false);
        } else {
            $(this).prop('checked', true);
        }
    });
</script>

<script>
    $('.lap').click(function() {
        $(this).prop('checked', true);


        $('.desk').prop('checked', false);
        $('.gad').prop('checked', false);
    });
    $('.desk').click(function() {
        $(this).prop('checked', true);


        $('.lap').prop('checked', false);
        $('.gad').prop('checked', false);
    });
    $('.gad').click(function() {
        $(this).prop('checked', true);


        $('.lap').prop('checked', false);
        $('.desk').prop('checked', false);
    });

    $('.amc').click(function() {
        $(this).prop('checked', true);
        $('.war').prop('checked', false);
        $('.out').prop('checked', false);
    });
    $('.war').click(function() {
        $(this).prop('checked', true);
        $('.amc').prop('checked', false);
        $('.out').prop('checked', false);
    });
    $('.out').click(function() {
        $(this).prop('checked', true);
        $('.amc').prop('checked', false);
        $('.war').prop('checked', false);
    });
</script>

<!-- user details   -->
<script>
    $(function() {
        // problems adding
        $("#prob_btn").click(function(e) {
            e.preventDefault();
            var _prid = $('.problemz option:selected').val();
            var _prname = $('.problemz option:selected').data('prob_name');
            // alert(_prid);
            // alert(_prname);
            $('.prb_card').append('<div class="p_' + _prid + '"><input type="text" name="" value="' + _prname + '" class="form-control"><input type="hidden" name="prob[]" value="' + _prid + '" class="form-control"><button data-prb_delete="' + _prid + '" class="btn btn-danger dlt_prb">Remove</button><br>');
        });


        $(document).on('click', '.dlt_prb', function(e) {
            e.preventDefault();
            var _btnid = $(this).data('prb_delete');
            $('.p_' + _btnid).remove();
        });

        // priliminary button ckick
        var _nmbr = 1;
        $(".btn_priliminary").click(function(e) {
            e.preventDefault();
            var html = '';
            var _gpid = $('.groupz  option:selected').val();

            var _gpname = $('.groupz  option:selected').data('gp_nae');

            var _mdlId = $('.grp_item  option:selected').val();

            var _mdlname = $('.grp_item  option:selected').data('it_name');

            var _pdiv = $('#p_body').find('.' + _gpid + '');
            var _divlngth = _pdiv.length;
            var _num = 1;
            if (_divlngth > 0) {
                $('.' + _gpid + '').append('<div class="rmv_' + _mdlId + '"><input type="hidden" name="num_i[]" value="' + _nmbr + '"><input name="mdlz_' + _gpid + '_' + _nmbr + '"  class="form-control" type="text" value="' + _mdlname + '" data-valid="' + _mdlId + '"><button data-rmv_id="' + _mdlId + '" class="btn btn-danger btn_rmv">Remove</button><input type="hidden" name="i[]" value="' + _mdlId + '"></div><br>');
                _nmbr++;
            } else {
                html += '<div class="col-md-12 ' + _gpid + '">';
                html += '<card>';
                html += '<div class="card-header bg-warning hdr_' + _gpid + '">' + _gpname + '<button data-hdr="' + _gpid + '" class="btn btn-danger pull-right hdr_delete" style="height: 38px;margin-left: 177px;">Delete</button><input type="hidden" name="head_i[]" value="' + _gpid + '"></div>';
                html += '</card>';
                html += '</div>';
                $('.p_body').append(html);
                $('.' + _gpid + '').append('<div class="rmv_' + _mdlId + '"><input type="hidden" name="num_i[]" value="' + _nmbr + '"><input name="mdlz_' + _gpid + '_' + _nmbr + '"  class="form-control" type="text" value="' + _mdlname + '" data-valid="' + _mdlId + '"><button data-rmv_id="' + _mdlId + '" class="btn btn-danger btn_rmv">Remove</button><input type="hidden" name="i[]" value="' + _mdlId + '"></div><br>');
                _nmbr++;
            }

            // $('.p_body').html();

        });


        $(document).on('click', '.hdr_delete', function(e) {
            e.preventDefault();
            var _btnid = $(this).data('hdr');
            $('.hdr_' + _btnid).remove();
        });

        $(document).on('click', '.btn_rmv', function(e) {
            e.preventDefault();
            var _btnid = $(this).data('rmv_id');
            $('.rmv_' + _btnid).remove();
        });

        $('.btn-success').attr('disabled', true);
        $('.btn-danger').hide();


        $(".groupz").change(function(e) {
            var _grp_val = $(this).val();
            $.ajax({
                url: '<?= base_url() ?>inhouse/getitemsbygroup',
                method: 'get',
                data: {
                    _grp_val: _grp_val
                },
                dataType: 'json',
                success: function(response) {
                    var html = '';
                    var _sel = '';
                    if (response != '') {
                        $.each(response, function(keys, vals) {

                            html += '<option style="" data-it_name="' + vals.itemname + '" value="' + vals.item_id + '">' + vals.itemname + '</option>';
                        });
                        $('#itms_grp').html(html);
                    }
                }
            });
        });

        $(document).on('change', '.usr_dtls', function(e) {
            var _u_id = $(this).val();
            if (_u_id != '') {
                $.ajax({
                    url: '<?= base_url() ?>inhouse/getuserbyid',
                    method: 'get',
                    data: {
                        _u_id: _u_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (response != '') {
                            // console.log(response);
                            $("#addrs").text(response.address);
                            $("#Mobile").val(response.mobile);
                            $("#Phone").val(response.landline);
                            $("#Email").val(response.emailid);
                            // $("#").text(response.custname);
                            $("#Location").val(response.place);
                            if (response.cus_orders != '') {
                                var html = '';
                                var _int = 1;
                                $.each(response.cus_orders, function(k, val) {
                                    console.log(val);
                                    html += '<tr>';
                                    html += '<td>' + _int + '</td>';
                                    html += '<td>' + val.orderno + '</td>';
                                    html += '<td>' + val.create_date + '</td>';
                                    html += '<td>' + val.item + '</td>';
                                    html += '<td>' + val.brand.brandname + '</td>';
                                    html += '<td>' + val.model.modelname + '</td>';
                                    html += '<td>' + val.serialno + '</td>';
                                    html += '<td>' + val.warranty + '</td>';
                                    html += '<td>' + val.status + '</td>';
                                    html += '</tr>';
                                    _int++;
                                });

                                $('table tbody').html(html);
                            }
                        }
                    }
                });
            }
        });

        $(document).on('click', '#items', function(e) {
            var _ite_val = $(this).val();
            if (_ite_val != '') {
                $.ajax({
                    url: '<?= base_url() ?>inhouse/getbrandbyitems',
                    method: 'get',
                    data: {
                        _ite_val: _ite_val
                    },
                    dataType: 'json',
                    success: function(response) {
                        var html = '';
                        if (response != '') {
                            html += '<option value=""></option>';
                            $.each(response, function(keys, vals) {
                                html += '<option value="' + vals.branditem_id + '">' + vals.brandname + '</option>';
                            });
                            $('#brand').html(html);
                        }
                    }
                });
            }
        });



        $(document).on('change', '#brand', function(e) {
            var _brand_id = $(this).val();
            if (_brand_id != '') {
                $.ajax({
                    url: '<?= base_url() ?>inhouse/getmodelbyid',
                    method: 'get',
                    data: {
                        _brand_id: _brand_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        var html = '';
                        if (response != '') {
                            $.each(response, function(keys, vals) {
                                html += '<option value="' + vals.model_id + '">' + vals.modelname + '</option>';
                            });
                            $('#mdl').html(html);
                        }
                    }
                });
            }
        });
        // form submitiion
        $('.btn-success').click(function(e) {
            e.preventDefault();
            var _form1 = $('#cus_frm').serialize();
            var _form2 = $('#cmplnt_frm').serialize();
            // var _form1 = new FormData($("#cus_frm")[0]);

            $.ajax({
                url: '<?= base_url() ?>inhouse/insertinhouse',
                method: 'post',
                data: $('#cus_frm').serialize(),
                // data: $('#cmplnt_frm').serialize(),
                dataType: 'json',
                success: function(resp) {
                    if (resp != '') {
                        $("#resp_divz").html('<input type="hidden" name="resp_ord_num" value="' + resp + '" class="form-control">');
                        $.ajax({
                            url: '<?= base_url() ?>inhouse/inhouse_cmplnt',
                            method: 'post',
                            data: $('#cmplnt_frm').serialize(),

                            dataType: 'json',
                            success: function(resp) {
                                location.reload(true);
                            }
                        });
                    }
                }
            });
        });
        $('.sw-btn-next').click(function(e) {
            e.preventDefault();
            $('.btn-success').attr('disabled', false);
        });

        $('.sw-btn-prev').click(function(e) {
            e.preventDefault();
            $('.btn-success').attr('disabled', true);
        });

    });


    // get data by refrence no IN01-18/2
    $("#find").click(function(e) {
        e.preventDefault();
        var _ref_id = $("#ref_no").val();
        if (_ref_id != '') {
            $.ajax({
                url: '<?= base_url() ?>inhouse/getcustdatabyrefno',
                method: 'get',
                data: {
                    _ref_id: _ref_id
                },
                dataType: 'json',
                success: function(response) {
                    var html = '';

                    if (response != '') {
                        // var ordertypez = response.ordertype.split(' ').join('_');
                        var replaced = response.cus_data.ordertype.split(' ').join('_');
                        var cust_id = response.cus_data.cust_id;

                        if (replaced == 'End_User') {
                            $('.dealer').prop('checked', false);
                            $('.end_user').prop('checked', true);
                        } else {
                            $('.dealer').prop('checked', true);
                            $('.end_user').prop('checked', false);
                        }
                        $('#cus_div').remove();

                        var _sel = '';
                        html += '<select class = "form-control select2-show-search usr_dtls" name="customer_id">';
                        $.each(response.allcustomers, function(cus_k, cus_val) {

                            if (cus_val.cust_id == cust_id) {
                                _sel = 'selected';
                            } else {
                                _sel = '';
                            }
                            html += '<option value = "' + cus_val.cust_id + '" ' + _sel + ' >' + cus_val.custname + ' - ' + cus_val.mobile + '</option>';

                        });
                        html += '</select>';

                        $("#addrs").text(response.cus_data.address);
                        $("#Mobile").val(response.cus_data.mobile);
                        $("#Phone").val(response.cus_data.landline);
                        $("#Email").val(response.cus_data.emailid);
                        $("#serial_numberz").val(response.order_dtls.serialno);
                        $("#Location").val(response.cus_data.place);


                        var _item = response.order_dtls.item;
                        if (_item == 'Laptop') {

                            $('.lap').prop('checked', true);
                            $('.desk').prop('checked', false);
                            $('.gad').prop('checked', false);

                            if ($('.lap').prop('checked') == true) {
                                setotherdataselected(response.order_dtls, _item);
                            }

                        } else if (_item == 'Desktop') {
                            $('.lap').prop('checked', false);
                            $('.desk').prop('checked', true);
                            $('.gad').prop('checked', false);

                            if ($('.desk').prop('checked') == true) {
                                setotherdataselected(response.order_dtls, _item);
                            }
                        } else {
                            $('.lap').prop('checked', false);
                            $('.desk').prop('checked', false);
                            $('.gad').prop('checked', true);

                            if ($('.gad').prop('checked') == true) {
                                setotherdataselected(response.order_dtls, _item);
                            }
                        }
                        var _stz = response.order_dtls.warranty
                        var s_replaced = _stz.split(' ').join('_');

                        if (s_replaced == "Without_Warranty") {
                            $('.out').prop('checked', true);
                            $('.amc').prop('checked', false);
                            $('.war').prop('checked', false);
                        } else if (s_replaced == 'AMC') {
                            $('.out').prop('checked', false);
                            $('.amc').prop('checked', true);
                            $('.war').prop('checked', false);
                        } else {
                            $('.out').prop('checked', false);
                            $('.amc').prop('checked', false);
                            $('.war').prop('checked', true);
                        }


                        console.log(response.cus_address);


                        var htmlz = '';
                        var _int = 1;
                        $.each(response.cus_address.cus_orders, function(k, val) {

                            htmlz += '<tr>';
                            htmlz += '<td>' + _int + '</td>';
                            htmlz += '<td>' + val.orderno + '</td>';
                            htmlz += '<td>' + val.create_date + '</td>';
                            htmlz += '<td>' + val.item + '</td>';
                            htmlz += '<td>' + val.brand.brandname + '</td>';
                            htmlz += '<td>' + val.model.modelname + '</td>';
                            htmlz += '<td>' + val.serialno + '</td>';
                            htmlz += '<td>' + val.warranty + '</td>';
                            htmlz += '<td>' + val.status + '</td>';
                            htmlz += '</tr>';
                            _int++;
                        });

                        $('table tbody').html(htmlz);

                        $("#ref_cus").html(html);
                    }
                }
            });
        } else {
            alert('Reference number Not Found');
        }
    });

    function setotherdataselected(order_dtls, _item) {

        _ite_val = _item;
        _brand_id = order_dtls.branditem_id;
        _model_id = order_dtls.model_id;
        $.ajax({
            url: '<?= base_url() ?>inhouse/getbrandbyitems',
            method: 'get',
            data: {
                _ite_val: _ite_val
            },
            dataType: 'json',
            success: function(response) {
                var html = '';
                var _sel = '';
                if (response != '') {
                    $.each(response, function(keys, vals) {
                        if (vals.branditem_id == order_dtls.branditem_id) {
                            _sel = 'selected';
                        } else {
                            _sel = '';
                        }
                        html += '<option value="' + vals.branditem_id + ' ' + _sel + ' ">' + vals.brandname + '</option>';
                    });
                    $('#brand').html(html);
                }
            }
        });
        // model
        $.ajax({
            url: '<?= base_url() ?>inhouse/getmodelbyid',
            method: 'get',
            data: {
                _brand_id: _brand_id
            },
            dataType: 'json',
            success: function(response) {
                var html = '';
                var _sel = '';
                if (response != '') {
                    $.each(response, function(keys, vals) {
                        if (vals.model_id == order_dtls.model_id) {
                            _sel = 'selected';
                        } else {
                            _sel = '';
                        }
                        html += '<option value="' + vals.model_id + '">' + vals.modelname + '</option>';
                    });
                    $('#mdl').html(html);
                }
            }
        });
    }
</script>



<script>
    $("#Order").change(function(e) {
        var _ordnum = $(this).val();

        if (_ordnum != '') {
            $.ajax({
                url: '<?= base_url() ?>inhouse/getdatasforissue',
                method: 'get',
                data: {
                    _ordnum: _ordnum
                },
                dataType: 'json',
                success: function(response) {
                    var html = '';
                    var _sel = '';
                    if (response != '') {
                        console.log(response);
                        var cmplnts = '';
                        $.each(response.complaints, function(c, val) {
                            cmplnts += '<b>Complaints</b>';
                            cmplnts += '<p>' + val.probname + '</p><br>';
                        });
                        $('.complaints').html(cmplnts);
                        $('.items').prop('checked', false);
                        if (response.item == 'Desktop') {
                            $('.desk').prop('checked', true);
                            var _ordnum = 'Desktop';
                            $.ajax({
                                url: '<?= base_url() ?>inhouse/getitemsitembyitem',
                                method: 'get',
                                data: {
                                    _ordnum: _ordnum
                                },
                                dataType: 'json',
                                success: function(rsps) {
                                    var _selhtml = '';
                                    _selhtml += '<option value="">Select</option>';
                                    $.each(rsps, function(k, vl) {
                                        _selhtml += '<option value="' + vl.issued_id + '" data-pricezs="' + vl.price + '">' + vl.issued_name + '</option>';
                                    });
                                    $("#issued_item").html(_selhtml);
                                }
                            });
                        } else if (response.item == 'Laptop') {
                            $('.lap').prop('checked', true);
                            var _ordnum = 'Laptop';
                            $.ajax({
                                url: '<?= base_url() ?>inhouse/getitemsitembyitem',
                                method: 'get',
                                data: {
                                    _ordnum: _ordnum
                                },
                                dataType: 'json',
                                success: function(rsps) {
                                    var _selhtml = '';
                                    _selhtml += '<option value="">Select</option>';
                                    $.each(rsps, function(k, vl) {
                                        _selhtml += '<option value="' + vl.issued_id + '" data-pricezs="' + vl.price + '">' + vl.issued_name + '</option>';
                                    });
                                    $("#issued_item").html(_selhtml);
                                }
                            });
                        } else if (response.item == 'Gadget') {
                            $('.gad').prop('checked', true);
                            var _ordnum = 'Gadget';
                            $.ajax({
                                url: '<?= base_url() ?>inhouse/getitemsitembyitem',
                                method: 'get',
                                data: {
                                    _ordnum: _ordnum
                                },
                                dataType: 'json',
                                success: function(rsps) {
                                    var _selhtml = '';
                                    _selhtml += '<option value="">Select</option>';
                                    $.each(rsps, function(k, vl) {
                                        _selhtml += '<option value="' + vl.issued_id + '" data-pricezs="' + vl.price + '">' + vl.issued_name + '</option>';
                                    });
                                    $("#issued_item").html(_selhtml);
                                }
                            });
                        } else {
                            '';
                            var _ordnum = '';
                            $.ajax({
                                url: '<?= base_url() ?>inhouse/getitemsitembyitem',
                                method: 'get',
                                data: {
                                    _ordnum: _ordnum
                                },
                                dataType: 'json',
                                success: function(rsps) {
                                    var _selhtml = '';
                                    _selhtml += '<option value="">Select</option>';
                                    $.each(rsps, function(k, vl) {
                                        _selhtml += '<option value="' + vl.issued_id + '" data-pricezs="' + vl.price + '">' + vl.issued_name + '</option>';
                                    });
                                    $("#issued_item").html(_selhtml);
                                }
                            });
                        }
                        $("#cname").val(response.customer.custname);
                        $("#mobile").val(response.customer.mobile);
                        // $.each(response, function(keys, vals) {

                        // });

                    } else {
                        alert('Order number not found');
                    }
                }
            });
        } else {
            alert('ordr number not found');
        }
    });
</script>

<script>
    $(document).on('change', '.issued_itemxyz', function(e) {
        var _ordnum = $(this).val();
        $.ajax({
            url: '<?= base_url() ?>inhouse/getpricesbyid',
            method: 'get',
            data: {
                _ordnum: _ordnum
            },
            dataType: 'json',
            success: function(rsps) {
                $("#price").val(rsps.price);
                $("#qty_price").val(rsps.price);

            }
        });
    });
</script>

<script>
    $(document).on('keyup', '#qty', function(e) {
        var _ordnum = $("#qty_price").val();
        var _qty = $(this).val();
        var _tprs = parseFloat(_ordnum) * parseFloat(_qty);
        if (_ordnum != '') {
            $("#price").val(_tprs);

        }
    });
</script>


<script>
    $("#call_customer").click(function(e) {
        e.preventDefault();
        var _num_ord = $("#Order").val();
        var _mobile = $(".ord_num_mobile").val();
        $("#ord_num_m").val(_num_ord);
        $("#ord_num_mobile").val(_mobile);
        if (_num_ord == '') {
            alert('order number not found');
        } else {
            $("#exampleModal4").modal('show');
        }

    });
</script>

<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addissuecalling">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_m">
                            </div>
                            <div class="col-md-3">
                                Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="call_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Mobile:
                                <input class="form-control" type="text" readonly="" name="cal_mob" id="ord_num_mobile">
                            </div>
                            <div class="col-md-3">
                                Calling Person:
                                <input class="form-control" type="text" name="cl_person" id="cl_person">
                            </div>
                            <div class="col-md-3">
                                Subject:
                                <input class="form-control" type="text" name="subj" id="subj">
                            </div>
                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="cl_rmrk"></Textarea>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Call Status</button>
            </div>
            </form>
        </div>
    </div>
</div>