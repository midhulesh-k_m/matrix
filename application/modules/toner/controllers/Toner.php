<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Toner extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('toner_model');
		$this->lgid = $_SESSION['login_id'];
		$this->u_type = $_SESSION['user_type'];
		$this->softid = $_SESSION['soft_id'];
	}
	public function index($id = null)
	{
		$data = [];
		$data['ord_id']	= $this->toner_model->getMaxOFOrdNum();
		$data['orders']	= $this->toner_model->getAllTonerOrders();
		$data['tech'] = $this->toner_model->getInouseTechnician();

		if ($id) {
			$id = base64_decode($id);
			$data['edit'] = $this->toner_model->getOrderById($id);

			$this->load->view('layout/header');
			$this->load->view('layout/menu');
			$this->load->view('toner', $data);
			$this->load->view('layout/footer');
		} else {
			$this->load->view('layout/header');
			$this->load->view('layout/menu');
			$this->load->view('toner', $data);
			$this->load->view('layout/footer');
		}
	}
	public function neworder()
	{
		$data = $this->toner_model->getMaxOfId();
		$toner['toner_id'] = $data['ord_id'];
		$toner['orderno'] = $this->input->post('order_number');
		$toner['order_date'] = $this->input->post('date');
		$toner['custname'] = $this->input->post('name');
		$toner['place'] = $this->input->post('Place');
		$toner['mobile'] = $this->input->post('Mobile');
		$toner['brand'] = $this->input->post('Brand');
		$toner['model'] = $this->input->post('Model');
		$toner['remarks'] = $this->input->post('Remark');
		$toner['emp_id'] = $this->lgid;
		$toner['soft_id'] = $this->softid;
		$this->db->insert('tonerrefill', $toner);

		$id = $this->db->insert_id();

		$mob = $this->input->post('Mobile');
		$dte = date('Y-m-d');


		$ornum = $_POST['order_number'];

		$apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
		// Message details
		$numbers = array($mob);
		$sender = urlencode('TXTLCL');
		$message = rawurlencode('Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte .  ' Ph :' . $mob . '');

		$numbers = implode(',', $numbers);

		// Prepare data for POST request
		$data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
		// Send the POST request with cURL
		$ch = curl_init('https://api.textlocal.in/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		// Process your response here
		//$response = array("balance"=>29,"status"=>"success");

		$decode =  json_decode($response);
		$ord_id =  $this->toner_model->selectmax_smsId();
		if ($decode->status) {

			$sms = array(
				'smsId' => $ord_id['ord_id'],
				'mobile' =>  $mob,
				'message' => 'Dear Customer, Your order has been registered with MATRIX.Order No. : .' . $ornum . ' Date:' . $dte .  ' Ph :' . $mob . '',
				'orderno' => $ornum,
				'smstype' => 'New Toner Order ',
				'mgr_id' =>  $this->softid,

				'status' => $decode->status,
				'soft_id' =>  $this->softid,
			);
			$this->db->insert('sendsms', $sms);
		}



		$this->session->set_flashdata('app_success', 'Added successfully');
		redirect('toner');
	}
	public function updatetoner()
	{
		$tid = $_POST['tonerid'];

		$toner['orderno'] = $this->input->post('order_number');
		$toner['order_date'] = $this->input->post('date');
		$toner['custname'] = $this->input->post('name');
		$toner['place'] = $this->input->post('Place');
		$toner['mobile'] = $this->input->post('Mobile');
		$toner['brand'] = $this->input->post('Brand');
		$toner['model'] = $this->input->post('Model');
		$toner['remarks'] = $this->input->post('Remark');
		$toner['emp_id'] = $this->lgid;
		$toner['soft_id'] = $this->softid;
		$this->db->where('toner_id ', $tid)->update('tonerrefill', $toner);

		$id = $this->db->affected_rows();
		$this->session->set_flashdata('app_success', 'updated successfully');
		redirect('toner');
	}
	public function delete($id)
	{
		$id = base64_decode($id);
		$this->db->where('orderno ', $id)->delete('tonerrefill');
		$this->session->set_flashdata('app_success', 'deleted successfully');
		redirect('toner');
	}
	public function assigntechi()
	{
		$techId = $_POST['techId'];
		$techtype = $_POST['techtype'];
		$ordnum = $_POST['ordnum'];
		$ordnum = base64_decode($ordnum);
		$data = $this->toner_model->getMaxOfTechId();
		if ($techtype == 0) {
			$techtype == 'INHOUSE';
		} else {
			$techtype == 'OUTSOURCE';
		}

		$techi = array(
			'tonertech_id' => $data['ord_id'],
			'allocate_date' => date('Y-m-d'),
			'techn_id' => $techId,
			'orderno' => $ordnum,
			'mgr_id' => $this->lgid,
			'soft_id' => $this->softid,
			'technician_type' => $techtype,
		);
		$this->db->insert('toner_technician', $techi);

		$id = $this->db->insert_id();
		$this->db->set('status', 'Allocated')->where('orderno ', $ordnum)->update('tonerrefill');
		$this->session->set_flashdata('app_success', 'Added successfully');
		redirect('toner');
	}
	public function allocated()
	{
		$data = [];
		// $data['ord_id']	= $this->toner_model->getMaxOFOrdNum();
		// $data['orders']	= $this->toner_model->getAllTonerOrders();
		$data['orders'] = $this->toner_model->getAllocated();


		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('allocated', $data);
		$this->load->view('layout/footer');
	}
	public function cancel($id)
	{
		$id = base64_decode($id);
		$this->db->set('status', 'Cancelled')->where('orderno ', $id)->update('tonerrefill');
		$this->session->set_flashdata('app_success', 'Cancelled successfully');
		redirect('toner/allocated');
	}
	public function view($id)
	{
		$id = base64_decode($id);
		$data = [];
		$data['alloted_list'] = $this->toner_model->getOrderDtlsForTechi($id);

		// $data['alloted_orders'] = $this->toner_model->getAlottedOrdersForTechi($id);
		// $data['issueditem'] = $this->toner_model->getIssuedItemsTechi($id);


		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('allote', $data);
		$this->load->view('layout/footer');
	}
	public function updatetechistatus()
	{
		$sts =	$_POST['_status'];
		$ord_num =	base64_decode($_POST['_num_ord']);
		$this->db->set('status', $sts)->where('orderno ', $ord_num)->update('tonerrefill');

		$id = $this->db->affected_rows();
		echo json_encode($id);
	}
	public function savefinished()
	{
		$onum =	$_POST['_num_ord'];
		$this->db->set('status', "Finished")->where('orderno ', $onum)->update('tonerrefill');

		$id = $this->db->affected_rows();
		echo json_encode($id);
	}
	public function updatetechiectimate()
	{

		$amount =	$_POST['_status'];
		$onum =	$_POST['_num_ord'];
		$this->db->set('amount', $amount)->where('orderno ', $onum)->update('toner_technician');

		$id = $this->db->affected_rows();
		echo json_encode($id);
	}
	public function addcallingdetails()
	{
		$mob =   $_POST['cal_mob'];
		$ordnum =   $_POST['ord_num'];
		$date =    $_POST['call_date'];
		$calling_person = $_POST['cl_person'];
		$sub =    $_POST['subj'];
		$remrk =    $_POST['cl_rmrk'];

		$max_id =  $this->toner_model->getCallMAxID();
		$this->db->where('orderno', $ordnum)->delete('callstatus');
		$call = array(
			'call_id' =>  $max_id['ord_id'],
			'call_date' => $date,
			'caller' => $calling_person,
			'orderno' => $ordnum,
			'mobileno' => $mob,
			'type' => '',
			'subject' => $sub,
			'reason' => $remrk,
			'cur_status' => 'REFILL',
			'soft_id' => $this->softid,
		);
		$this->db->insert('callstatus', $call);

		$id =  $this->db->insert_id();
		if ($id >= 0) {
			$this->session->set_flashdata('app_success', 'Added successfully');
			redirect('toner/view/' . base64_encode($ordnum));
		}
	}
	public function finished()
	{
		$data = [];

		$data['orders'] = $this->toner_model->getFinished();


		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('finished', $data);
		$this->load->view('layout/footer');
	}
	public function viewfinished($id)
	{
		$id = base64_decode($id);
		$data = [];
		$data['alloted_list'] = $this->toner_model->getOrderDtlsForTechi($id);
		$data['receiptamount'] = $this->toner_model->getreceiptamount($id);

		$data['ord_id']	= $this->toner_model->getMaxOftonerReceiptNum();
		$data['cost']	= $this->toner_model->getcostbyId($id);






		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('v_finished', $data);
		$this->load->view('layout/footer');
	}
	public function tonerreceipt()
	{
		$ord_id	= $this->toner_model->getMaxOfrECEIPTID();
		$Receipt_no = $this->input->post('Receipt_no');
		$Receipt_dte = $this->input->post('Receipt_dte');
		$done_by = $this->input->post('done_by');
		$technicianId = $this->input->post('technicianId');
		$ordernum = $this->input->post('ordernum');
		$cost = $this->input->post('cost');
		$receipt_amt = $this->input->post('receipt_amt');

		$tnr = array(
			'receipt_id ' => $ord_id,
			'receiptno ' => $Receipt_no,
			'orderno ' => $ordernum,
			'receipttype ' => 'REFILL',
			'receipt_date ' => $Receipt_dte,
			'receipt_amt ' => $receipt_amt,
			'cost ' => $cost,
			'techn_id ' => $technicianId,
			'refno ' => '',
			'status ' => 'Paid',
			'paid_date ' => date('Y-m-d'),
			'soft_id ' => $this->softid,
		);
		$rslt =	$this->db->select('orderno')->where('orderno', $ordernum)->get('receipts')->row_array();
		if ($rslt) {
			$this->db->set('status', 'Delivered')->where('orderno ', $ordernum)->update('tonerrefill');

			$this->db->where('orderno ', $ordernum)->update('receipts', $tnr);
			$this->session->set_flashdata('app_success', 'Updated successfully');
			redirect('toner/viewfinished/' . base64_encode($ordernum));
		} else {
			$this->db->insert('receipts', $tnr);
			$id = $this->db->insert_id();

			$this->db->set('status', 'Delivered')->where('orderno ', $ordernum)->update('tonerrefill');




			$ord_id =  $this->toner_model->selectmax_smsId();
			$smsdata =  $this->db->where('orderno', $ordernum)->get('sendsms')->row_array();
			$dte = date('Y-m-d');


			$apiKey = urlencode('Ubq7+w67bn8-1aiJJoC3iSriWjYwgmA9H7EnMUWrEl');
			// Message details
			$numbers = array($smsdata['mobile']);
			$sender = urlencode('TXTLCL');
			$message = rawurlencode('Dear Customer, Your order has been Delivered Order No. : .' . $ordernum . ' Date:' . $dte . '');

			$numbers = implode(',', $numbers);

			// Prepare data for POST request
			$data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender' => $sender, 'message' => $message);
			// Send the POST request with cURL
			$ch = curl_init('https://api.textlocal.in/send/');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			// Process your response here
			//$response = array("balance"=>29,"status"=>"success");
			$decode =  json_decode($response);
			// if ($decode->status) {
			$sms = array(
				'smsId' => $ord_id['ord_id'],
				'mobile' =>  $smsdata['mobile'],
				'message' => 'Dear Customer, Your order has been Verified Order No. : .' . $ordernum . ' Date:' . $dte . '',
				'orderno' => $ordernum,
				'smstype' => 'Toner Delivered',
				'mgr_id' =>  $this->softid,

				'status' => $decode->status,
				'soft_id' =>  $this->softid,
			);
			$this->db->insert('sendsms', $sms);









			$this->session->set_flashdata('app_success', 'Added successfully');
			redirect('toner/viewfinished/' . base64_encode($ordernum));
		}
	}
	public function delivered()
	{
		$data = [];

		$data['orders'] = $this->toner_model->getDelivered();


		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('delivered', $data);
		$this->load->view('layout/footer');
	}
	public function printreceipt()
	{
		$id = $_POST['resp'];
		$data['customer'] = $this->toner_model->getOrddtls($id);
		$this->load->view('print_receipt', $data);
	}
}
