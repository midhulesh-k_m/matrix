<!-- app-content-->
<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">
<link href="<?= base_url() ?>assets/plugins/select2/select2.min.css" rel="stylesheet" />
<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Welcome To</span> E-Commerce Dashboard</h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- contecnt -->

        <div class="col-md-12">
            <div class="col-md-12">
                <?php if ($success = $this->session->flashdata('app_error')) : ?>
                    <div class="alert alert-danger alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <h6> <?php echo $success ?></h6>
                        <strong></strong>
                    </div>
                <?php endif ?>
                <?php if ($success = $this->session->flashdata('app_success')) : ?>
                    <div class="alert alert-success alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <?php echo $success ?>
                        <strong></strong>
                    </div>
                <?php endif ?>
            </div>
            <div class="card">
                <div class="card-body">
                    <?php if ($edit['toner_id']) { ?>
                        <form method="post" action="<?= base_url() ?>toner/updatetoner">
                        <?php } else { ?> <form method="post" action="<?= base_url() ?>toner/neworder"><?php } ?>
                            <div class="row">
                                <input type="hidden" value="<?= $edit['toner_id']; ?>" class="form-control" name="tonerid">
                                <div class="col-md-3">
                                    Order number:
                                    <input type="text" value="<?= isset($edit['orderno']) ? $edit['orderno'] : $ord_id; ?>" class="form-control" name="order_number" readonly="">

                                </div>
                                <div class="col-md-3">
                                    Order date:
                                    <input type="date" value="<?php if ($edit['order_date']) {
                                                                    echo date('Y-m-d', strtotime($edit['order_date']));
                                                                } else {
                                                                    echo date('Y-m-d');
                                                                } ?>" class="form-control" name="date">
                                </div>
                                <div class="col-md-3">
                                    Customer name:
                                    <input type="text" value="<?= isset($edit['custname']) ? $edit['custname'] : ''; ?>" required="" class="form-control" name="name">
                                </div>
                                <div class="col-md-3">
                                    Place:
                                    <input required="" type="text" class="form-control" value="<?= isset($edit['place']) ? $edit['place'] : ''; ?>" name="Place">
                                </div>
                                <div class="col-md-3">
                                    Mobile:
                                    <input value="<?= isset($edit['mobile']) ? $edit['mobile'] : ''; ?>" required="" type="text" class="form-control" name="Mobile">
                                </div>
                                <div class="col-md-3">
                                    Brand:
                                    <input value="<?= isset($edit['brand']) ? $edit['brand'] : ''; ?>" required="" type="text" class="form-control" name="Brand">
                                </div>
                                <div class="col-md-3">
                                    Model:
                                    <input value="<?= isset($edit['model']) ? $edit['model'] : ''; ?>" required="" type="text" class="form-control" name="Model">
                                </div>
                                <div class="col-md-3">
                                    Remark:
                                    <input value="<?= isset($edit['remarks']) ? $edit['remarks'] : ''; ?>" type="text" class="form-control" name="Remark">
                                </div>
                                <div class="com-md-12">
                                    <br>

                                    <!-- <button class="btn btn-info btn-sm">Call</button>&nbsp&nbsp&nbsp<button class="btn btn-info btn-sm">SMS</button>&nbsp&nbsp&nbsp -->
                                    <?php if ($edit['remarks']) { ?>
                                        <button class="btn btn-info btn-sm">Update</button>
                                    <?php  } else { ?>
                                        <button class="btn btn-info btn-sm">Save</button>
                                    <?php } ?>
                                </div>
                            </div>
                            </form>
                </div>
            </div>

        </div>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12">



                        <div class="table-responsive">
                            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-striped table-bordered w-100 text-nowrap display dataTable no-footer" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">
                                                    <th>Order Number</th>
                                                    <th>Order Date</th>
                                                    <th>Customer Name</th>
                                                    <th>Place</th>
                                                    <th>Mobile</th>

                                                    <th>Brand</th>
                                                    <th>Model</th>
                                                    <th>Remarks</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php if ($orders) foreach ($orders as $keys => $ord_val) { ?>

                                                    <tr>
                                                        <td><?= $ord_val['orderno']; ?> </td>
                                                        <td><?php $fte =  $ord_val['order_date'];
                                                            $fte = strtotime($fte);
                                                            echo date('Y-m-d', $fte); ?> </td>


                                                        <td> <?= $ord_val['custname']; ?></td>
                                                        <td> <?= $ord_val['place']; ?></td>
                                                        <td> <?= $ord_val['mobile']; ?></td>
                                                        <td> <?= $ord_val['brand']; ?></td>
                                                        <td> <?= $ord_val['model']; ?></td>
                                                        <td> <?= $ord_val['remarks']; ?></td>
                                                        <td> <?= $ord_val['status']; ?></td>






                                                        <td><a href="<?= base_url() ?>toner/index/<?php echo base64_encode($ord_val['orderno']); ?>" class="btn btn-info btn-sm">Edit</a> &nbsp <a href="<?= base_url() ?>toner/delete/<?php echo base64_encode($ord_val['orderno']); ?>" class="btn btn-info btn-sm">delete</a>&nbsp <a href="#" data-ordnum="<?php echo base64_encode($ord_val['orderno']); ?>" class="btn btn-info btn-sm btn_assign">Assign</a> </td>

                                                    </tr>
                                                <?php } ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-5">

                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">


                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- end content -->

    </div><!-- End app-content-->
</div><!-- End app-content-->

<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<script>
    $('.btn_assign').click(function(e) {
        e.preventDefault();
        var _id = $(this).data('ordnum');
        $('.ordnum').html('<input type="hidden" name="ordnum" value="' + _id + '">');
        $("#exampleModal3").modal('show');
    })
</script>


<!-- Message Modal -->
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="technifrm" method="post" action="<?= base_url() ?>toner/assigntechi">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Select Technician:</label>
                                    <select name="technician_id" class="form-control select2-show-search technician_id">
                                        <option value="">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</option>
                                        <?php if ($tech) {
                                            foreach ($tech as $key5 => $t_val) { ?>
                                                <option value="<?= $t_val['techn_id']; ?>" data-tech_type="<?= $t_val['outsource']; ?>"><?= $t_val['tech_name']; ?></option>
                                        <?php }
                                        } ?>
                                    </select>
                                </div>
                                <div class="tech_type">

                                </div>
                                <div class="tech_id">

                                </div>
                                <div class="ordnum">

                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="assign_tech" type="button" class="btn btn-primary">Assign Technician</button>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $('#assign_tech').click(function(e) {
        e.preventDefault();
        var _techid = $('.technician_id').val();
        var _type = $('.technician_id option:selected').data('tech_type');
        $('.tech_id').html('<input type="hidden" name="techId" value="' + _techid + '">');
        $('.tech_type').html('<input type="hidden" name="techtype" value="' + _type + '">');
        if (_techid == '') {
            alert('select technicain');
        } else {
            $('#technifrm').submit();
        }
    });
</script>