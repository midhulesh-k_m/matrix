<!-- app-content-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/multipleselect/multiple-select.css">

<!-- Data table css -->
<link href="<?= base_url() ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
<link href="<?= base_url() ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title"><span class="subpage-title">Alloted Order</span></h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="top" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <?php if ($success = $this->session->flashdata('app_error')) : ?>
                    <div class="alert alert-danger alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <h6> <?php echo $success ?></h6>
                        <strong></strong>
                    </div>
                <?php endif ?>
                <?php if ($success = $this->session->flashdata('app_success')) : ?>
                    <div class="alert alert-success alert-dismissible msgBox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <!-- <i class="fas fa-user"></i> -->
                        <?php echo $success ?>
                        <strong></strong>
                    </div>
                <?php endif ?>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            Technician Name:<b><?= $alloted_list['tech_name']; ?></b>
                        </div>
                        <div class="col-md-12">

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label>Select Filter</label>
                            <select class="filter-multi" style="display: none;">

                            </select>

                        </div>

                    </div> -->
                    <div class="col-md-12">
                        <div class="row">


                            <div class="col-md-6">
                                Order NO:
                                <b><?= $alloted_list['orderno']; ?></b>
                            </div>


                            <div class="col-md-6">
                                Customer name:
                                <b><?= $alloted_list['custname']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Mobile Number:
                                <b><?= $alloted_list['mobile']; ?></b>
                            </div>
                            <div class="col-md-6">
                                Item:
                                <b><?= $alloted_list['item']; ?></b>
                            </div><br><br>

                            <div class="col-md-6">
                                Brand: <b><?= $alloted_list['brand']; ?></b>
                            </div><br><br>
                            <div class="col-md-6">
                                Model: <b><?= $alloted_list['model']; ?></b>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header bg-info">
                                        <center>Repair Report</center>
                                    </div>
                                    <div class="card-body" style="">
                                        <div class="row">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>



                                                        <th>
                                                            Status
                                                        </th>
                                                        <th>
                                                            Amount
                                                        </th>
                                                        <th>
                                                            Remarks
                                                        </th>


                                                    </tr>
                                                </thead>
                                                <tbody>



                                                    <td>
                                                        <?php if ($alloted_list) {
                                                        ?>
                                                            <select id="ord_sts" class="form-control ord_sts">

                                                                <option data-ord_num="<?= $alloted_list['orderno']; ?>" <?php if ($alloted_list['status'] == 'Allocated') {
                                                                                                                            echo 'selected';
                                                                                                                        } ?> value="Allocated">Pending</option>
                                                                <option data-ord_num="<?= $alloted_list['orderno']; ?>" <?php if ($alloted_list['status'] == 'Reparing') {
                                                                                                                            echo 'selected';
                                                                                                                        } ?> value="Reparing">Reparing</option>
                                                                <option data-ord_num="<?= $alloted_list['orderno']; ?>" <?php if ($alloted_list['status'] == 'Checking') {
                                                                                                                            echo 'selected';
                                                                                                                        } ?> value="Checking">Checking</option>
                                                                <option data-ord_num="<?= $alloted_list['orderno']; ?>" <?php if ($alloted_list['status'] == 'Completed') {
                                                                                                                            echo 'selected';
                                                                                                                        } ?> value="Completed">Finished</option>
                                                                <option data-ord_num="<?= $alloted_list['orderno']; ?>" <?php if ($alloted_list['status'] == 'Ignore') {
                                                                                                                            echo 'selected';
                                                                                                                        } ?> value="Ignore">Ignore</option>
                                                            </select><br>

                                                    </td>
                                                    <td>

                                                        <input data-ord_num="<?= $alloted_list['complaint_id']; ?>" id="estimate" type="text" value="<?= $alloted_list['amount']; ?>" class="form-control estimate"><br>

                                                    </td>
                                                    <td>

                                                        <button data-ord_num="<?= $alloted_list['complaint_id']; ?>" class="btn btn-info btn-sm btn_remarks">Remark</button><br><br>
                                                    <?php
                                                        } ?>
                                                    </td>
                                                    <!-- <td>

                                                        <textarea data-ord_num="<?= $ord_num; ?>" id="rmrks" class="form-control"> <?= $alloted_orders['remarks_sts']['tech_remarks']; ?></textarea>
                                                    </td> -->
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div><br><br>
                            <?php if ($issueditem) { ?>

                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header bg-primary">
                                            Issued Item
                                        </div>
                                        <div class="card-body">
                                            <table class="table">
                                                <tr>
                                                    <th>Issued Item</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <!-- <th>Warranty Status</th> -->
                                                    <!-- <th>Issued Item</th> -->
                                                </tr>
                                                <tr>
                                                    <?php foreach ($issueditem as $kys => $is_vl) { ?>
                                                <tr>
                                                    <td><?= $is_vl['issued_item']; ?></td>
                                                    <td><?= $is_vl['quantity']; ?></td>
                                                    <td><?= $is_vl['price']; ?></td>
                                                    <!-- <td><?= $is_vl['warranty_status']; ?></td> -->
                                                    <!-- <td><?= $is_vl['issued_item']; ?></td> -->
                                                </tr>
                                            <?php  }  ?>
                                            </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                            <div class="col-md-6">

                                <!-- <button type="button" class="btn btn-info ">Call</button> -->

                                <button type="button" data-ord_num="<?= $ord_num; ?>" onclick="finishorder()" id="Finished" class="btn btn-info Finished">Finished</button>
                                <button class="btn btn-info" id="call_customer">Call</button>
                                <!-- <button type="button" class="btn btn-info" onclick="return  confirm('Cancel Order ?');">Cancel</button> -->
                            </div>


                        </div>



                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
</div>


<script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
<!--MutipleSelect js-->
<script src="<?= base_url() ?>assets/plugins/multipleselect/new_multiple.js"></script>
<script src="<?= base_url() ?>assets/plugins/multipleselect/multi-select.js"></script>


<!-- Message Modal -->
<script>
    $('.btn_remarks').click(function(e) {
        var _num_ord = $(this).data('ord_num');
        $("#mdl_remark").val(_num_ord);


        $.ajax({
            url: '<?= base_url() ?>inhouse/getcmplntremarks',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    $("#rmrk_data").text(evnt.remarks);
                }
            }
        });


        $("#exampleModal3").modal('show');
    });
</script>
<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Add Remarks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>inhouse/addtechiremarkz">
                    <div class="col-md-12">
                        Remarks:
                        <textarea name="remarks_bdl" class="form-control" id="rmrk_data"></textarea>
                        <input type="hidden" name="remarks_id" id="mdl_remark">
                        <input type="hidden" name="ord_nuum" id="" value="<?= $ord_num; ?>">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Remark</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- // calling customer  -->

<script>
    $("#call_customer").click(function(e) {
        e.preventDefault();
        var _num_ord = "<?= $alloted_list['orderno']; ?>";
        var _mobile = "<?= $alloted_list['mobile']; ?>";
        $("#ord_num_mobile").val(_mobile);
        $("#ord_num_m").val(_num_ord);

        $.ajax({
            url: '<?= base_url() ?>inhouse/getcallingdetails',
            method: 'post',
            data: {
                // _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    console.log(evnt);
                    $("#cl_date").val(evnt.date);
                    $("#subj").val(evnt.subject);
                    $("#cl_person").val(evnt.caller);
                    $("#cl_rmrk").text(evnt.reason);
                }
            }
        });

        $("#exampleModal4").modal('show');
    });
</script>


<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- <div class="modal-dialog " role="document"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Call Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url() ?>toner/addcallingdetails">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                Order number:
                                <input class="form-control" type="text" readonly="" name="ord_num" id="ord_num_m">
                            </div>
                            <div class="col-md-3">
                                Date:
                                <input class="form-control" type="date" value="<?= date('Y-m-d'); ?>" name="call_date" id="cl_date">
                            </div>
                            <div class="col-md-3">
                                Mobile:
                                <input class="form-control" type="text" readonly="" name="cal_mob" id="ord_num_mobile">
                            </div>
                            <div class="col-md-3">
                                Calling Person:
                                <input class="form-control" type="text" name="cl_person" id="cl_person">
                            </div>
                            <div class="col-md-3">
                                Subject:
                                <input class="form-control" type="text" name="subj" id="subj">
                            </div>
                            <div class="col-md-12">
                                Remarks:
                                <Textarea class="form-control" id="cl_rmrk" name="cl_rmrk"></Textarea>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Call Status</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(function() {
        var _stz = '';
        $(".ord_sts option:selected").each(function(k, valz) {
            var _c_sts = $(this).val();

            if (_c_sts === 'Allocated') {
                $(".Finished").prop('disabled', true);
            }
            if (_c_sts === 'Reparing') {
                $(".Finished").prop('disabled', true);
            }
            if (_c_sts === 'Checking') {
                $(".Finished").prop('disabled', true);
            }
        });

    });
</script>

<script>
    $(".ord_sts").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('option:selected', this).data('ord_num');
        var _order_number = "<?= $alloted_list['orderno']; ?>";
        _num_ord = btoa(_num_ord);
        // if ((_status == 'Completed') || (_status == 'Ignore')) {
        //     $("#Finished").prop('disabled', false);
        // } else {
        //     $("#Finished").prop('disabled', true);
        // }
        $.ajax({
            url: '<?= base_url() ?>toner/updatetechistatus',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {

                if (evnt != '') {
                    alert("Success");
                    location.reload(true);
                }
            }
        });
    });
    // amount
    $(".estimate").change(function(e) {
        var _status = $(this).val();
        var _num_ord = "<?= $alloted_list['orderno']; ?>";
        $.ajax({
            url: '<?= base_url() ?>toner/updatetechiectimate',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    // location.reload(true);
                }
            }
        });
    });
    // remarks
    $("#rmrks").change(function(e) {
        var _status = $(this).val();
        var _num_ord = $('#estimate').data('ord_num');
        $.ajax({
            url: '<?= base_url() ?>inhouse/updatetechiremarks',
            method: 'post',
            data: {
                _status: _status,
                _num_ord: _num_ord,
            },
            dataType: 'json',
            success: function(evnt) {
                if (evnt != '') {
                    alert("Success");
                    location.reload(true);
                }
            }
        });
    });
    //finished
    function finishorder(e) {
        // e.preventDefault();
        var _num_ord = "<?= $alloted_list['orderno']; ?>";

        if (confirm('Confirm ?')) {
            $.ajax({
                url: '<?= base_url() ?>toner/savefinished',
                method: 'post',
                data: {

                    _num_ord: _num_ord,
                },
                dataType: 'json',
                success: function(evnt) {
                    if (evnt != '') {
                        alert("Success");
                        window.location.href = "<?= base_url() ?>toner/allocated";
                    }
                }
            });
        }
    }
</script>