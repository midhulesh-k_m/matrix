<?php
class Toner_model extends CI_Model
{
    public function __construct()
    {
        $this->lgid = $_SESSION['login_id'];
        $this->u_type = $_SESSION['user_type'];
        $this->softid = $_SESSION['soft_id'];
    }
    public function getMaxOFOrdNum()
    {


        $date = date('Y');
        $array = str_split($date, 1);
        $int = $array[3] + 1;
        $yr =  $array[2] . $array[3];

        $order_id =   $this->db->select('orderno')->where('soft_id',  $this->softid)->order_by('toner_id ', 'desc')->get('tonerrefill')->row_array();
        if ($order_id) {

            $id = explode('/', $order_id['orderno']);

            $id = $id[1] + 1;
            return  'TR0' . $int . '-' . $yr . '/' . $id;
        } else {
            return  'TR0' . $int . '-' . $yr . '/1';
        }


        // 
    }
    public function getMaxOfId()
    {
        return  $this->db->query("SELECT IFNULL(max(`toner_id` + 1),1)  as ord_id FROM tonerrefill")->row_array();
    }
    public function getAllTonerOrders()
    {
        return  $this->db->query("SELECT * FROM `tonerrefill` WHERE `soft_id` = '$this->softid' AND status = 'Pending' ")->result_array();
    }
    public function getOrderById($id)
    {
        return  $this->db->query("SELECT * FROM `tonerrefill` WHERE `soft_id` = '$this->softid' AND orderno = '$id' ")->row_array();
    }
    public function getInouseTechnician()
    {
        return  $this->db->query("SELECT techn_id,tech_name,outsource FROM technicians WHERE `refill` = 1   AND status = 'Valid' AND soft_id = $this->softid")->result_array();
    }
    public function getMaxOfTechId()
    {
        return  $this->db->query("SELECT IFNULL(max(`tonertech_id` + 1),1)  as ord_id FROM  toner_technician")->row_array();
    }
    public function getAllocated()
    {
        return   $this->db->query("SELECT tonerrefill.*,toner_technician.tonertech_id,toner_technician.technician_type,technicians.tech_name FROM tonerrefill INNER JOIN toner_technician on toner_technician.orderno = tonerrefill.orderno INNER JOIN technicians on technicians.techn_id = toner_technician.techn_id  WHERE (tonerrefill.status ='Cancelled' OR tonerrefill.status ='Checking' OR tonerrefill.status ='Reparing' OR tonerrefill.status ='Completed' OR tonerrefill.status ='Ignore' OR tonerrefill.status ='Allocated') AND tonerrefill.`soft_id` = '$this->softid' ")->result_array();
    }
    public function getFinished()
    {
        return   $this->db->query("SELECT tonerrefill.*,toner_technician.tonertech_id,toner_technician.technician_type,technicians.tech_name FROM tonerrefill INNER JOIN toner_technician on toner_technician.orderno = tonerrefill.orderno INNER JOIN technicians on technicians.techn_id = toner_technician.techn_id  WHERE (tonerrefill.status ='Finished' OR tonerrefill.status ='ignored' ) AND tonerrefill.`soft_id` = '$this->softid' ")->result_array();
    }
    public function getDelivered()
    {
        return   $this->db->query("SELECT tonerrefill.*,toner_technician.tonertech_id,toner_technician.technician_type,technicians.tech_name FROM tonerrefill INNER JOIN toner_technician on toner_technician.orderno = tonerrefill.orderno INNER JOIN technicians on technicians.techn_id = toner_technician.techn_id  WHERE tonerrefill.status ='Delivered' AND tonerrefill.`soft_id` = '$this->softid' ")->result_array();
    }
    public function finished()
    {
        return   $this->db->query("SELECT tonerrefill.*,toner_technician.tonertech_id,toner_technician.technician_type,technicians.tech_name FROM tonerrefill INNER JOIN toner_technician on toner_technician.orderno = tonerrefill.orderno INNER JOIN technicians on technicians.techn_id = toner_technician.techn_id  WHERE tonerrefill.status ='Finished'  AND tonerrefill.`soft_id` = '$this->softid' ")->result_array();
    }
    public function getOrderDtlsForTechi($id)
    {
        return   $this->db->query("SELECT tonerrefill.*,toner_technician.tonertech_id,toner_technician.technician_type,toner_technician.amount,technicians.tech_name FROM tonerrefill INNER JOIN toner_technician on toner_technician.orderno = tonerrefill.orderno INNER JOIN technicians on technicians.techn_id = toner_technician.techn_id WHERE tonerrefill.status != 'Pending' AND tonerrefill.`soft_id` = '$this->softid' AND tonerrefill.orderno = '$id' ")->row_array();
    }
    public function getCallMAxID()
    {
        return  $this->db->query("SELECT IFNULL(max(`call_id` + 1),1)  as ord_id FROM callstatus")->row_array();
    }
    public function getMaxOftonerReceiptNum()
    {
        $oid =  $this->db->query("SELECT IFNULL(max(`receiptno` + 1),1)  as ord_id FROM receipts")->row_array();
        return  $oid['ord_id'];
    }
    public function getcostbyId($id)
    {
        $amt =  $this->db->select('amount')->where('orderno', $id)->get('toner_technician')->row_array();
        return $amt['amount'];
    }
    public function getMaxOfrECEIPTID()
    {
        $oid =  $this->db->query("SELECT IFNULL(max(`receipt_id` + 1),1)  as ord_id FROM receipts")->row_array();
        return  $oid['ord_id'];
    }
    public function getreceiptamount($id)
    {
        $amt =  $this->db->select('receipt_amt,receipt_date')->where('orderno', $id)->get('receipts')->row_array();
        if ($amt) {
            return $amt;
        } else {
            return '';
        }
    }
    public function getOrddtls($id)
    {
        return   $this->db->where('orderno', $id)->get(' tonerrefill')->row_array();
    }
    public function selectmax_smsId()
    {
        return  $this->db->query("SELECT IFNULL(max(`smsId` + 1),1)  as ord_id FROM sendsms")->row_array();
    }
}
