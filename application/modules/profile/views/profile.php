<div class="app-content toggle-content">
    <div class="side-app">
        <!-- page-header -->
        <div class="page-header">
            <h1 class="page-title">Form Elements</h1>
            <div class="ml-auto">
                <div class="input-group">
                    <a class="btn btn-primary btn-icon text-white mr-2" id="daterange-btn" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Calendar">
                        <span>
                            <i class="fe fe-calendar"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-secondary btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Rating">
                        <span>
                            <i class="fe fe-star"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-success btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Email">
                        <span>
                            <i class="fe fe-mail"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-warning btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Chat">
                        <span>
                            <i class="fe fe-message-square"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-info btn-icon mr-2" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                        <span>
                            <i class="fe fe-plus"></i>
                        </span>
                    </a>
                    <a href="#" class="btn btn-danger btn-icon" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Support">
                        <span>
                            <i class="fe fe-help-circle"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- End page-header -->
        <!-- Row-->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Default Form Input Fields</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="input" placeholder="Enter Your Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="example-disabled-input" placeholder="Read Only Text area.">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="example-disabled-input" placeholder="Disabled text area.." value="" disabled="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                    <input type="text" class="form-control is-valid state-valid" name="example-text-input-valid" placeholder="Valid Email..">
                                </div>
                                <div class="form-group  has-danger">
                                    <input type="text" class="form-control is-invalid state-invalid" name="example-text-input-invalid" placeholder="Invalid feedback">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="example-password-input" placeholder="Password..">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write a large text here ..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Default Form Input Fields with labels</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Enter Name</label>
                                    <input type="text" class="form-control" name="example-text-input" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Disabled</label>
                                    <input type="text" class="form-control" name="example-disabled-input" placeholder="Disabled text area.." value="" disabled="">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Readonly</label>
                                    <input type="text" class="form-control" name="example-disabled-input" placeholder="Read Only Text area." readonly="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Valid Email</label>
                                    <input type="text" class="form-control is-valid state-valid" name="example-text-input-valid" placeholder="Valid Email..">
                                </div>
                                <div class="form-group m-0">
                                    <label class="form-label">Invalid Number</label>
                                    <input type="text" class="form-control is-invalid state-invalid" name="example-text-input-invalid" placeholder="Invalid Number..">
                                    <div class="invalid-feedback">Invalid feedback</div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <input type="password" class="form-control" name="example-password-input" placeholder="Password..">
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="form-group mb-0">
                                    <label class="form-label">Message</label>
                                    <textarea class="form-control" name="example-textarea-input" rows="4" placeholder="text here.."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Different Input Style Forms</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label class="form-label">Text</label>
                                    <input type="text" class="form-control" name="example-text-input" placeholder="Text..">
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Country</label>
                                    <select name="country" id="select-countries" class="form-control custom-select">
                                        <option value="br" data-data="{&quot;image&quot;: &quot;https://laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/assets/images/flag/flags/br.svg&quot;}">Brazil</option>
                                        <option value="cz" data-data="{&quot;image&quot;: &quot;https://laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/assets/images/flag/flags/cz.svg&quot;}">Czech Republic</option>
                                        <option value="de" data-data="{&quot;image&quot;: &quot;https://laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/assets/images/flag/flags/de.svg&quot;}">Germany</option>
                                        <option value="pl" data-data="{&quot;image&quot;: &quot;https://laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/assets/images/flag/flags/pl.svg&quot;}" selected="">Poland</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Input group</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-append">
                                            <button class="btn btn-primary" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label class="form-label">Input group buttons</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary">Action</button>
                                            <button data-toggle="dropdown" type="button" class="btn btn-primary dropdown-toggle"></button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="javascript:void(0)">News</a>
                                                <a class="dropdown-item" href="javascript:void(0)">Messages</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="javascript:void(0)">Edit Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label class="form-label">Separated inputs</label>
                                    <div class="row gutters-xs">
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="Search for...">
                                        </div>
                                        <span class="col-auto">
                                            <button class="btn btn-primary" type="button"><i class="fe fe-search"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">ZIP Code</label>
                                    <div class="row gutters-sm">
                                        <div class="col">
                                            <input type="text" class="form-control" placeholder="Search for...">
                                        </div>
                                        <span class="col-auto align-self-center">
                                            <span class="form-help" data-toggle="popover" data-placement="top" data-content="<p>ZIP Code must be US or CDN format. You can use an extended ZIP+4 code to determine address more accurately.</p>
															<p class='mb-0'><a href='#'>USP ZIP codes lookup tools</a></p>
															" data-original-title="" title="">?</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-label">Bootstrap's Custom File Input</div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="example-file-input-custom">
                                        <label class="custom-file-label">Choose file</label>
                                    </div>
                                </div>
                                <div class="form-group m-0">
                                    <label class="form-label">Date of birth</label>
                                    <div class="row gutters-xs">
                                        <div class="col-5">
                                            <select name="user[month]" class="form-control custom-select select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value="">Month</option>
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option selected="selected" value="6" data-select2-id="3">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2" style="width: 231.825px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-usermonth-3u-container"><span class="select2-selection__rendered" id="select2-usermonth-3u-container" role="textbox" aria-readonly="true" title="June">June</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                        </div>
                                        <div class="col-3">
                                            <select name="user[day]" class="form-control custom-select select2 select2-hidden-accessible" data-select2-id="4" tabindex="-1" aria-hidden="true">
                                                <option value="">Day</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option selected="selected" value="20" data-select2-id="6">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="5" style="width: 135.9px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-userday-lp-container"><span class="select2-selection__rendered" id="select2-userday-lp-container" role="textbox" aria-readonly="true" title="20">20</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                        </div>
                                        <div class="col-4">
                                            <select name="user[year]" class="form-control custom-select select2 select2-hidden-accessible" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                                <option value="">Year</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                                <option value="2011">2011</option>
                                                <option value="2010">2010</option>
                                                <option value="2009">2009</option>
                                                <option value="2008">2008</option>
                                                <option value="2007">2007</option>
                                                <option value="2006">2006</option>
                                                <option value="2005">2005</option>
                                                <option value="2004">2004</option>
                                                <option value="2003">2003</option>
                                                <option value="2002">2002</option>
                                                <option value="2001">2001</option>
                                                <option value="2000">2000</option>
                                                <option value="1999">1999</option>
                                                <option value="1998">1998</option>
                                                <option value="1997">1997</option>
                                                <option value="1996">1996</option>
                                                <option value="1995">1995</option>
                                                <option value="1994">1994</option>
                                                <option value="1993">1993</option>
                                                <option value="1992">1992</option>
                                                <option value="1991">1991</option>
                                                <option value="1990">1990</option>
                                                <option selected="selected" value="1989" data-select2-id="9">1989</option>
                                                <option value="1988">1988</option>
                                                <option value="1987">1987</option>
                                                <option value="1986">1986</option>
                                                <option value="1985">1985</option>
                                                <option value="1984">1984</option>
                                                <option value="1983">1983</option>
                                                <option value="1982">1982</option>
                                                <option value="1981">1981</option>
                                                <option value="1980">1980</option>
                                                <option value="1979">1979</option>
                                                <option value="1978">1978</option>
                                                <option value="1977">1977</option>
                                                <option value="1976">1976</option>
                                                <option value="1975">1975</option>
                                                <option value="1974">1974</option>
                                                <option value="1973">1973</option>
                                                <option value="1972">1972</option>
                                                <option value="1971">1971</option>
                                                <option value="1970">1970</option>
                                                <option value="1969">1969</option>
                                                <option value="1968">1968</option>
                                                <option value="1967">1967</option>
                                                <option value="1966">1966</option>
                                                <option value="1965">1965</option>
                                                <option value="1964">1964</option>
                                                <option value="1963">1963</option>
                                                <option value="1962">1962</option>
                                                <option value="1961">1961</option>
                                                <option value="1960">1960</option>
                                                <option value="1959">1959</option>
                                                <option value="1958">1958</option>
                                                <option value="1957">1957</option>
                                                <option value="1956">1956</option>
                                                <option value="1955">1955</option>
                                                <option value="1954">1954</option>
                                                <option value="1953">1953</option>
                                                <option value="1952">1952</option>
                                                <option value="1951">1951</option>
                                                <option value="1950">1950</option>
                                                <option value="1949">1949</option>
                                                <option value="1948">1948</option>
                                                <option value="1947">1947</option>
                                                <option value="1946">1946</option>
                                                <option value="1945">1945</option>
                                                <option value="1944">1944</option>
                                                <option value="1943">1943</option>
                                                <option value="1942">1942</option>
                                                <option value="1941">1941</option>
                                                <option value="1940">1940</option>
                                                <option value="1939">1939</option>
                                                <option value="1938">1938</option>
                                                <option value="1937">1937</option>
                                                <option value="1936">1936</option>
                                                <option value="1935">1935</option>
                                                <option value="1934">1934</option>
                                                <option value="1933">1933</option>
                                                <option value="1932">1932</option>
                                                <option value="1931">1931</option>
                                                <option value="1930">1930</option>
                                                <option value="1929">1929</option>
                                                <option value="1928">1928</option>
                                                <option value="1927">1927</option>
                                                <option value="1926">1926</option>
                                                <option value="1925">1925</option>
                                                <option value="1924">1924</option>
                                                <option value="1923">1923</option>
                                                <option value="1922">1922</option>
                                                <option value="1921">1921</option>
                                                <option value="1920">1920</option>
                                                <option value="1919">1919</option>
                                                <option value="1918">1918</option>
                                                <option value="1917">1917</option>
                                                <option value="1916">1916</option>
                                                <option value="1915">1915</option>
                                                <option value="1914">1914</option>
                                                <option value="1913">1913</option>
                                                <option value="1912">1912</option>
                                                <option value="1911">1911</option>
                                                <option value="1910">1910</option>
                                                <option value="1909">1909</option>
                                                <option value="1908">1908</option>
                                                <option value="1907">1907</option>
                                                <option value="1906">1906</option>
                                                <option value="1905">1905</option>
                                                <option value="1904">1904</option>
                                                <option value="1903">1903</option>
                                                <option value="1902">1902</option>
                                                <option value="1901">1901</option>
                                                <option value="1900">1900</option>
                                                <option value="1899">1899</option>
                                                <option value="1898">1898</option>
                                                <option value="1897">1897</option>
                                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="8" style="width: 183.863px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-useryear-fr-container"><span class="select2-selection__rendered" id="select2-useryear-fr-container" role="textbox" aria-readonly="true" title="1989">1989</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row-->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Switch &amp; Colors</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=" card-body">
                        <div class="form-group">
                            <div class="form-label">Toggle switch single</div>
                            <label class="custom-switch">
                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">I agree with terms and conditions</span>
                            </label>
                            <div class="form-label">Toggle switch single Checked</div>
                            <label class="custom-switch">
                                <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" checked="">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">I agree with terms and conditions</span>
                            </label>
                        </div>
                        <div class="form-group ">
                            <label class="form-label">Your skills</label>
                            <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="HTML" class="selectgroup-input" checked="">
                                    <span class="selectgroup-button">HTML</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="CSS" class="selectgroup-input">
                                    <span class="selectgroup-button">CSS</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="PHP" class="selectgroup-input">
                                    <span class="selectgroup-button">PHP</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="JavaScript" class="selectgroup-input">
                                    <span class="selectgroup-button">JavaScript</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="Angular" class="selectgroup-input">
                                    <span class="selectgroup-button">Angular</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="Java" class="selectgroup-input">
                                    <span class="selectgroup-button">Java</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="checkbox" name="value" value="C++" class="selectgroup-input">
                                    <span class="selectgroup-button">C++</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-0">
                            <label class="form-label">Select Color</label>
                            <div class="row gutters-xs">
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="azure" class="colorinput-input" checked="">
                                        <span class="colorinput-color bg-azure"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="indigo" class="colorinput-input">
                                        <span class="colorinput-color bg-indigo"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="purple" class="colorinput-input">
                                        <span class="colorinput-color bg-purple"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="pink" class="colorinput-input">
                                        <span class="colorinput-color bg-pink"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="red" class="colorinput-input">
                                        <span class="colorinput-color bg-red"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="orange" class="colorinput-input">
                                        <span class="colorinput-color bg-orange"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="yellow" class="colorinput-input">
                                        <span class="colorinput-color bg-yellow"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="lime" class="colorinput-input">
                                        <span class="colorinput-color bg-lime"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="green" class="colorinput-input">
                                        <span class="colorinput-color bg-green"></span>
                                    </label>
                                </div>
                                <div class="col-auto">
                                    <label class="colorinput">
                                        <input name="color" type="checkbox" value="teal" class="colorinput-input">
                                        <span class="colorinput-color bg-teal"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Radios &amp; Checkboxes</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=" card-body">
                        <div class="form-group form-elements">
                            <div class="form-label">Radios</div>
                            <div class="custom-controls-stacked">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="example-radios" value="option1" checked="">
                                    <span class="custom-control-label">Option 1</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="example-radios" value="option2">
                                    <span class="custom-control-label">Option 2</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="example-radios" value="option3" disabled="">
                                    <span class="custom-control-label">Option Disabled</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" name="example-radios2" value="option4" disabled="" checked="">
                                    <span class="custom-control-label">Option Disabled Checked</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-elements m-0">
                            <div class="form-label">Checkboxes</div>
                            <div class="custom-controls-stacked">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" checked="">
                                    <span class="custom-control-label">Option 1</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2">
                                    <span class="custom-control-label">Option 2</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" disabled="">
                                    <span class="custom-control-label">Option Disabled</span>
                                </label>
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="example-checkbox4" value="option4" checked="" disabled="">
                                    <span class="custom-control-label">Option Disabled Checked</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-4 col-lg-12 col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">File upload</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dropify-wrapper">
                            <div class="dropify-message"><span class="file-icon"></span>
                                <p>Drag and drop a file here or click</p>
                                <p class="dropify-error">Ooops, something wrong appended.</p>
                            </div>
                            <div class="dropify-loader"></div>
                            <div class="dropify-errors-container">
                                <ul></ul>
                            </div><input type="file" class="dropify"><button type="button" class="dropify-clear">Remove</button>
                            <div class="dropify-preview"><span class="dropify-render"></span>
                                <div class="dropify-infos">
                                    <div class="dropify-infos-inner">
                                        <p class="dropify-filename"><span class="dropify-filename-inner"></span></p>
                                        <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12 col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">File Upload with Default Image</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dropify-wrapper has-preview">
                            <div class="dropify-message"><span class="file-icon"></span>
                                <p>Drag and drop a file here or click</p>
                                <p class="dropify-error">Ooops, something wrong appended.</p>
                            </div>
                            <div class="dropify-loader" style="display: none;"></div>
                            <div class="dropify-errors-container">
                                <ul></ul>
                            </div><input type="file" class="dropify" data-default-file="assets/images/photos/1.jpg"><button type="button" class="dropify-clear">Remove</button>
                            <div class="dropify-preview" style="display: block;"><span class="dropify-render"><img src="assets/images/photos/1.jpg"></span>
                                <div class="dropify-infos">
                                    <div class="dropify-infos-inner">
                                        <p class="dropify-filename"><span class="dropify-filename-inner">1.jpg</span></p>
                                        <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12 col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Disabled File Upload</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dropify-wrapper disabled">
                            <div class="dropify-message"><span class="file-icon"></span>
                                <p>Drag and drop a file here or click</p>
                                <p class="dropify-error">Ooops, something wrong appended.</p>
                            </div>
                            <div class="dropify-loader"></div>
                            <div class="dropify-errors-container">
                                <ul></ul>
                            </div><input type="file" class="dropify" disabled="disabled">
                            <div class="dropify-preview"><span class="dropify-render"></span>
                                <div class="dropify-infos">
                                    <div class="dropify-infos-inner">
                                        <p class="dropify-filename"><span class="dropify-filename-inner"></span></p>
                                        <p class="dropify-infos-message">Drag and drop or click to replace</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->

        <!--row open-->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">MutipleSelect Styles</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Basic MutipleSelect</label>
                                    <select multiple="multiple" class="multi-select" style="display: none;">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <div class="ms-parent multi-select" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>January</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>February</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>March</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>April</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>May</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>June</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>July</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>August</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>September</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>October</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>November</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>December</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Disabled MutipleSelect</label>
                                    <select multiple="multiple" class="multi-select" disabled="disabled" style="display: none;">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <div class="ms-parent multi-select" style="width: 80.0875px;"><button type="button" class="ms-choice disabled"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>January</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>February</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>March</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>April</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>May</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>June</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>July</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>August</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>September</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>October</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>November</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>December</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Single Group Disabled MutipleSelect</label>
                                    <select multiple="multiple" class="multi-select" style="display: none;">
                                        <optgroup label="Group 1" disabled="disabled">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                        </optgroup>
                                        <optgroup label="Group 2">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </optgroup>
                                        <optgroup label="Group 3">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                            <option value="9">Option 9</option>
                                        </optgroup>
                                    </select>
                                    <div class="ms-parent multi-select" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="group"><label class="optgroup disabled" data-group="group_0"><input type="checkbox" data-name="selectGroup" disabled="disabled">Group 1</label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" data-group="group_0" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" data-group="group_0" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" data-group="group_0" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" data-group="group_0" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" data-group="group_0" value="5"><span>Option 5</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_1"><input type="checkbox" data-name="selectGroup">Group 2</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="5"><span>Option 5</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="6"><span>Option 6</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="7"><span>Option 7</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="8"><span>Option 8</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_2"><input type="checkbox" data-name="selectGroup">Group 3</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="5"><span>Option 5</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="6"><span>Option 6</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="7"><span>Option 7</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="8"><span>Option 8</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="9"><span>Option 9</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Multiple Items With Group-Option</label>
                                    <select multiple="multiple" class="optmulti-select" style="display: none;">
                                        <optgroup label="Group 1">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </optgroup>
                                        <optgroup label="Group 2">
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </optgroup>
                                        <optgroup label="Group 3">
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                        </optgroup>
                                    </select>
                                    <div class="ms-parent optmulti-select" style="width: 100%;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="group"><label class="optgroup " data-group="group_0"><input type="checkbox" data-name="selectGroup">Group 1</label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="1"><span>1</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="2"><span>2</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="3"><span>3</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="4"><span>4</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="5"><span>5</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_1"><input type="checkbox" data-name="selectGroup">Group 2</label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="6"><span>6</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="7"><span>7</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="8"><span>8</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="9"><span>9</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="10"><span>10</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_2"><input type="checkbox" data-name="selectGroup">Group 3</label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="11"><span>11</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="12"><span>12</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="13"><span>13</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="14"><span>14</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="15"><span>15</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Single Row</label>
                                    <select multiple="multiple" class="single-select" style="display: none;">
                                        <option value="1">First</option>
                                        <option value="2">Second</option>
                                        <option value="3">Third</option>
                                        <option value="4" selected="selected">Fourth</option>
                                    </select>
                                    <div class="ms-parent single-select" style="width: 59.0875px;"><button type="button" class="ms-choice"><span class="">Fourth</span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="" style="false"><label class=""><input type="radio" data-name="selectItem" value="1"><span>First</span></label></li>
                                                <li class="" style="false"><label class=""><input type="radio" data-name="selectItem" value="2"><span>Second</span></label></li>
                                                <li class="" style="false"><label class=""><input type="radio" data-name="selectItem" value="3"><span>Third</span></label></li>
                                                <li class="selected" style="false"><label class=""><input type="radio" data-name="selectItem" checked="checked" value="4"><span>Fourth</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Group-Option Filter</label>
                                    <select multiple="multiple" class="group-filter" style="display: none;">
                                        <optgroup label="Group 1">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                        </optgroup>
                                        <optgroup label="Group 2">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </optgroup>
                                        <optgroup label="Group 3">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                            <option value="9">Option 9</option>
                                        </optgroup>
                                    </select>
                                    <div class="ms-parent group-filter" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <div class="ms-search"><input type="text" autocomplete="off" autocorrect="off" autocapitilize="off" spellcheck="false"></div>
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="group"><label class="optgroup " data-group="group_0"><input type="checkbox" data-name="selectGroup">Group 1</label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="1"><span>Option 1</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="2"><span>Option 2</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="3"><span>Option 3</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="4"><span>Option 4</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="5"><span>Option 5</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_1"><input type="checkbox" data-name="selectGroup">Group 2</label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="1"><span>Option 1</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="2"><span>Option 2</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="3"><span>Option 3</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="4"><span>Option 4</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="5"><span>Option 5</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="6"><span>Option 6</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="7"><span>Option 7</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="8"><span>Option 8</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_2"><input type="checkbox" data-name="selectGroup">Group 3</label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="1"><span>Option 1</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="2"><span>Option 2</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="3"><span>Option 3</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="4"><span>Option 4</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="5"><span>Option 5</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="6"><span>Option 6</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="7"><span>Option 7</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="8"><span>Option 8</span></label></li>
                                                <li class="multiple" style="width: 80px;"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="9"><span>Option 9</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>MutipleSelect</label>
                                    <select multiple="multiple" class="multi-select" style="display: none;">
                                        <option value="1" selected="selected">January</option>
                                        <option value="2" disabled="disabled">February</option>
                                        <option value="3" selected="selected" disabled="disabled">March</option>
                                        <option value="4" disabled="disabled">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <div class="ms-parent multi-select" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="">January, March</span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="selected" style="false"><label class=""><input type="checkbox" data-name="selectItem" checked="checked" value="1"><span>January</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" value="2"><span>February</span></label></li>
                                                <li class="selected" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" checked="checked" disabled="disabled" value="3"><span>March</span></label></li>
                                                <li class="" style="false"><label class="disabled"><input type="checkbox" data-name="selectItem" disabled="disabled" value="4"><span>April</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>May</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>June</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>July</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>August</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>September</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>October</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>November</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>December</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Group-Option MutipleSelect</label>
                                    <select multiple="multiple" class="multi-select" style="display: none;">
                                        <optgroup label="Group 1">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                        </optgroup>
                                        <optgroup label="Group 2">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                        </optgroup>
                                        <optgroup label="Group 3">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                            <option value="4">Option 4</option>
                                            <option value="5">Option 5</option>
                                            <option value="6">Option 6</option>
                                            <option value="7">Option 7</option>
                                            <option value="8">Option 8</option>
                                            <option value="9">Option 9</option>
                                        </optgroup>
                                    </select>
                                    <div class="ms-parent multi-select" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="group"><label class="optgroup " data-group="group_0"><input type="checkbox" data-name="selectGroup">Group 1</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_0" value="5"><span>Option 5</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_1"><input type="checkbox" data-name="selectGroup">Group 2</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="5"><span>Option 5</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="6"><span>Option 6</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="7"><span>Option 7</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_1" value="8"><span>Option 8</span></label></li>
                                                <li class="group"><label class="optgroup " data-group="group_2"><input type="checkbox" data-name="selectGroup">Group 3</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="1"><span>Option 1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="2"><span>Option 2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="3"><span>Option 3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="4"><span>Option 4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="5"><span>Option 5</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="6"><span>Option 6</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="7"><span>Option 7</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="8"><span>Option 8</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" data-group="group_2" value="9"><span>Option 9</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Multiple Items</label>
                                    <select multiple="multiple" class="multiselect" style="display: none;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                    </select>
                                    <div class="ms-parent multiselect" style="width: 460px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>1</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>2</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>3</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>4</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>5</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>6</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>7</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>8</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>9</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>10</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>11</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>12</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="13"><span>13</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="14"><span>14</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="15"><span>15</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="16"><span>16</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="17"><span>17</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="18"><span>18</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="19"><span>19</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="20"><span>20</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="21"><span>21</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="22"><span>22</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="23"><span>23</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="24"><span>24</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="25"><span>25</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="26"><span>26</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="27"><span>27</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="28"><span>28</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="29"><span>29</span></label></li>
                                                <li class="multiple" style="width: 55px;"><label class=""><input type="checkbox" data-name="selectItem" value="30"><span>30</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Hide SelectAll</label>
                                    <select multiple="multiple" class="hide-select" style="display: none;">
                                        <option value="1">First</option>
                                        <option value="2">Second</option>
                                        <option value="3">Third</option>
                                        <option value="4">Fourth</option>
                                    </select>
                                    <div class="ms-parent hide-select" style="width: 59.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>First</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>Second</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>Third</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>Fourth</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Select Filter</label>
                                    <select multiple="multiple" class="filter-multi" style="display: none;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                    </select>
                                    <div class="ms-parent filter-multi" style="width: 27.175px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <div class="ms-search"><input type="text" autocomplete="off" autocorrect="off" autocapitilize="off" spellcheck="false"></div>
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>1</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>2</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>3</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>4</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>5</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>6</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>7</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>8</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>9</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>10</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>11</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>12</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="13"><span>13</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="14"><span>14</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="15"><span>15</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="16"><span>16</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="17"><span>17</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="18"><span>18</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="19"><span>19</span></label></li>
                                                <li class="" style="false"><label class=""><input type="checkbox" data-name="selectItem" value="20"><span>20</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Custom Style</label>
                                    <select multiple="multiple" class="custom-multiselect" style="display: none;">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <div class="ms-parent custom-multiselect" style="width: 80.0875px;"><button type="button" class="ms-choice"><span class="placeholder"></span>
                                            <div></div>
                                        </button>
                                        <div class="ms-drop bottom">
                                            <ul style="max-height: 250px;">
                                                <li class="ms-select-all"><label><input type="checkbox" data-name="selectAll"> [Select all]</label></li>
                                                <li class="" style="background-color: #5964ff ; color: #ffffff;"><label class=""><input type="checkbox" data-name="selectItem" value="1"><span>January</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="2"><span>February</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="3"><span>March</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="4"><span>April</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="5"><span>May</span></label></li>
                                                <li class="" style="background-color: #32cafe; color: #ffffff;"><label class=""><input type="checkbox" data-name="selectItem" value="6"><span>June</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="7"><span>July</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="8"><span>August</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="9"><span>September</span></label></li>
                                                <li class="" style="background-color: #ff7088; color: #ffffff;"><label class=""><input type="checkbox" data-name="selectItem" value="10"><span>October</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="11"><span>November</span></label></li>
                                                <li class=""><label class=""><input type="checkbox" data-name="selectItem" value="12"><span>December</span></label></li>
                                                <li class="ms-no-results">No matches found</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Switches</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Bootstrap Switch Default
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionDefault" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionDefault" class="label-default"></label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Bootstrap Switch Primary
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionPrimary" class="label-primary"></label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Bootstrap Switch Success
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionSuccess" class="label-success"></label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Bootstrap Switch Info
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionInfo" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionInfo" class="label-info"></label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Bootstrap Switch Warning
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionWarning" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionWarning" class="label-warning"></label>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Bootstrap Switch Danger
                                <div class="material-switch pull-right">
                                    <input id="someSwitchOptionDanger" name="someSwitchOption001" type="checkbox">
                                    <label for="someSwitchOptionDanger" class="label-danger"></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <form method="post" class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Select2 elements</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="form-group ">
                            <label class="form-label">Beast</label>
                            <select class="form-control select2 custom-select select2-hidden-accessible" data-placeholder="Choose one" data-select2-id="10" tabindex="-1" aria-hidden="true">
                                <option label="Choose one" data-select2-id="12">
                                </option>
                                <option value="1">Chuck Testa</option>
                                <option value="2">Sage Cattabriga-Alosa</option>
                                <option value="3">Nikola Tesla</option>
                                <option value="4">Cattabriga-Alosa</option>
                                <option value="5">Nikola Alosa</option>
                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="11" style="width: 542.6px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-qtwp-container"><span class="select2-selection__rendered" id="select2-qtwp-container" role="textbox" aria-readonly="true"><span class="select2-selection__placeholder">Choose one</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Basic Select2</label>
                            <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose one (with optgroup)" data-select2-id="13" tabindex="-1" aria-hidden="true">
                                <optgroup label="Mountain Time Zone">
                                    <option value="AZ" data-select2-id="15">Arizona</option>
                                    <option value="CO">Colorado</option>
                                    <option value="ID">Idaho</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="UT">Utah</option>
                                    <option value="WY">Wyoming</option>
                                </optgroup>
                                <optgroup label="Central Time Zone">
                                    <option value="AL">Alabama</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TX">Texas</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="WI">Wisconsin</option>
                                </optgroup>
                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="14" style="width: 542.6px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-8586-container"><span class="select2-selection__rendered" id="select2-8586-container" role="textbox" aria-readonly="true" title="Arizona">Arizona</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>
                        <div class="form-group">
                            <label class="form-label"> Select2 with search box</label>
                            <select class="form-control select2-show-search select2-hidden-accessible" data-placeholder="Choose one (with searchbox)" data-select2-id="20" tabindex="-1" aria-hidden="true">
                                <optgroup label="Mountain Time Zone">
                                    <option value="AZ" data-select2-id="22">Arizona</option>
                                    <option value="CO">Colorado</option>
                                    <option value="ID">Idaho</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="UT">Utah</option>
                                    <option value="WY">Wyoming</option>
                                </optgroup>
                                <optgroup label="Central Time Zone">
                                    <option value="AL">Alabama</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TX">Texas</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="WI">Wisconsin</option>
                                </optgroup>
                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="21" style="width: 542.6px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-9kcd-container"><span class="select2-selection__rendered" id="select2-9kcd-container" role="textbox" aria-readonly="true" title="Arizona">Arizona</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Users list</label>
                            <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Browser" multiple="" data-select2-id="16" tabindex="-1" aria-hidden="true">
                                <option value="Firefox">
                                    Firefox
                                </option>
                                <option value="Chrome selected">
                                    Chrome
                                </option>
                                <option value="Safari">
                                    Safari
                                </option>
                                <option selected="" value="Opera" data-select2-id="18">
                                    Opera
                                </option>
                                <option value="Internet Explorer">
                                    Internet Explorer
                                </option>
                            </select><span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="17" style="width: 542.6px;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false">
                                        <ul class="select2-selection__rendered">
                                            <li class="select2-selection__choice" title="
													Opera
												" data-select2-id="19"><span class="select2-selection__choice__remove" role="presentation">×</span>
                                                Opera
                                            </li>
                                            <li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li>
                                        </ul>
                                    </span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xl-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Time Picker</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=" card-body">
                        <label>Default Time Picker:</label>
                        <div class="wd-150 mg-b-30">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-clock-o tx-16 lh-0 op-6"></i>
                                    </div>
                                </div><!-- input-group-prepend -->
                                <input class="form-control ui-timepicker-input" id="tpBasic" placeholder="Set time" type="text" autocomplete="off">
                            </div>
                        </div><!-- wd-150 -->
                        <label class="mt-4">Set the scroll position to local time if no value selected:</label>
                        <div class="wd-150 mg-b-30">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-clock-o tx-16 lh-0 op-6"></i>
                                    </div><!-- input-group-text -->
                                </div><!-- input-group-prepend -->
                                <input class="form-control ui-timepicker-input" id="tp2" placeholder="Set time" type="text" autocomplete="off">
                            </div>
                        </div><!-- wd-150 -->
                        <label class="mt-4 ">Dynamically set the time using a Javascript Date object:</label>
                        <div class="d-flex">
                            <div class="input-group wd-150">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-clock-o tx-16 lh-0 op-6"></i>
                                    </div><!-- input-group-text -->
                                </div><!-- input-group-prepend -->
                                <input class="form-control ui-timepicker-input" id="tp3" placeholder="Set time" type="text" autocomplete="off">
                                <button class="btn btn btn-primary " id="setTimeButton">Set Current Time</button>
                            </div><!-- input-group -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Color Picker</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=" card-body">
                        <p class=" mb-1">A simple component to select color in the same way you select color in Adobe Photoshop.</p><input id="colorpicker" type="text" style="display: none;">
                        <div class="sp-replacer sp-light">
                            <div class="sp-preview">
                                <div class="sp-preview-inner" style="background-color: rgb(0, 97, 218);"></div>
                            </div>
                            <div class="sp-dd">▼</div>
                        </div>
                        <p class="mt-3  mb-1">You can allow alpha transparency selection. Check out these example.</p><input id="showAlpha" type="text" style="display: none;">
                        <div class="sp-replacer sp-light">
                            <div class="sp-preview">
                                <div class="sp-preview-inner" style="background-color: rgba(0, 97, 218, 0.5);"></div>
                            </div>
                            <div class="sp-dd">▼</div>
                        </div>
                        <p class="mt-4 mb-1">Show pallete only. If you'd like, spectrum can show the palettes you specify, and nothing else.</p><input id="showPaletteOnly" type="text" style="display: none;">
                        <div class="sp-replacer sp-light">
                            <div class="sp-preview">
                                <div class="sp-preview-inner" style="background-color: rgb(220, 53, 69);"></div>
                            </div>
                            <div class="sp-dd">▼</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Date Range Picker</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div><!-- input-group-prepend -->
                            <input type="text" class="form-control pull-right" id="reservation">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Single Month Display Date Picker</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="mg-b-20 mg-sm-b-40">The datepicker is tied to a standard form input field. Click on the input to open an interactive calendar in a small overlay. If a date is chosen, feedback is shown as the input's value.</p>
                        <div class="wd-200 mg-b-30">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                    </div>
                                </div><input class="form-control fc-datepicker hasDatepicker" placeholder="MM/DD/YYYY" type="text" id="dp1599285399264">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div>
                            <h3 class="card-title">Multiple Months Display Date Picker</h3>
                        </div>
                        <div class="card-options">
                            <a href="#" class="mr-4 text-default" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                <span class="fe fe-more-horizontal fs-20"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#"><i class="fe fe-eye mr-2"></i>View</a></li>
                                <li><a href="#"><i class="fe fe-plus-circle mr-2"></i>Add</a></li>
                                <li><a href="#"><i class="fe fe-trash-2 mr-2"></i>Remove</a></li>
                                <li><a href="#"><i class="fe fe-download-cloud mr-2"></i>Download</a></li>
                                <li><a href="#"><i class="fe fe-settings mr-2"></i>More</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="mg-b-20 mg-sm-b-40">The datepicker is tied to a standard form input field. Click on the input to open an interactive calendar in a small overlay. If a date is chosen, feedback is shown as the input's value.</p>
                        <div class="wd-200 mg-b-30">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                    </div>
                                </div><input class="form-control hasDatepicker" id="datepickerNoOfMonths" placeholder="MM/DD/YYYY" type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row-->

    </div>
    <!-- Right-sidebar-->
    <div class="sidebar sidebar-right sidebar-animate ps">
        <div class="tab-menu-heading siderbar-tabs border-0">
            <div class="tabs-menu ">
                <!-- Tabs -->
                <ul class="nav panel-tabs">
                    <li class=""><a href="#tab1" class="active" data-toggle="tab">Chat</a></li>
                    <li><a href="#tab2" data-toggle="tab">Activity</a></li>
                    <li><a href="#tab3" data-toggle="tab">Todo</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body tabs-menu-body side-tab-body p-0 border-0 ">
            <div class="tab-content border-top">
                <div class="tab-pane active" id="tab1">
                    <div class="chat">
                        <div class="contacts_card">
                            <div class="input-group p-3">
                                <input type="text" placeholder="Search..." class="form-control search">
                                <div class="input-group-prepend">
                                    <span class="input-group-text search_btn  "><i class="fa fa-search"></i></span>
                                </div>
                            </div>
                            <ul class="contacts mb-0">
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/male/3.jpg" class="rounded-circle user_img" alt="img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Maryam Naz</h6>
                                            <small class="text-muted">Maryam is online</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>01-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/female/1.jpg" class="rounded-circle user_img" alt="img">

                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Sahar Darya</h6>
                                            <small class="text-muted">Sahar left 7 mins ago</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>01-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/female/9.jpg" class="rounded-circle user_img" alt="img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Maryam Naz</h6>
                                            <small class="text-muted">Maryam is online</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>01-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/female/10.jpg" class="rounded-circle user_img" alt="img">

                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Yolduz Rafi</h6>
                                            <small class="text-muted">Yolduz is online</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>02-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/male/5.jpg" class="rounded-circle user_img" alt="img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Nargis Hawa</h6>
                                            <small class="text-muted">Nargis left 30 mins ago</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>02-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/male/7.jpg" class="rounded-circle user_img" alt="img">
                                            <span class="online_icon"></span>
                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Khadija Mehr</h6>
                                            <small class="text-muted">Khadija left 50 mins ago</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>03-02-2019</small></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex bd-highlight">
                                        <div class="img_cont">
                                            <img src="assets/images/users/female/8.jpg" class="rounded-circle user_img" alt="img">

                                        </div>
                                        <div class="user_info">
                                            <h6 class="mt-1 mb-0 ">Khadija Mehr</h6>
                                            <small class="text-muted">Khadija left 50 mins ago</small>
                                        </div>
                                        <div class="float-right text-right ml-auto mt-auto mb-auto"><small>03-02-2019</small></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane  " id="tab2">
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-primary brround avatar-md">CH</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>New Websites is Created</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">30 mins ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-danger brround avatar-md">N</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Prepare For the Next Project</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">2 hours ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-info brround avatar-md">S</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Decide the live Discussion Time</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">3 hours ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-warning brround avatar-md">K</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Team Review meeting at yesterday at 3:00 pm</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">4 hours ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-success brround avatar-md">R</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Prepare for Presentation</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">1 days ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center  border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-pink brround avatar-md">MS</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Prepare for Presentation</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">1 days ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center border-bottom p-4">
                        <div class="">
                            <span class="avatar bg-purple brround avatar-md">L</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Prepare for Presentation</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">45 mintues ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list d-flex align-items-center p-4">
                        <div class="">
                            <span class="avatar bg-blue brround avatar-md">U</span>
                        </div>
                        <div class="wrapper w-100 ml-3">
                            <p class="mb-0 d-flex">
                                <b>Prepare for Presentation</b>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex align-items-center">
                                    <i class="mdi mdi-clock text-muted mr-1"></i>
                                    <small class="text-muted ml-auto">2 days ago</small>
                                    <p class="mb-0"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="">
                        <div class="d-flex p-3">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" checked="">
                                <span class="custom-control-label">Do Even More..</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" checked="">
                                <span class="custom-control-label">Find an idea.</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" checked="">
                                <span class="custom-control-label">Hangout with friends</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox4" value="option4">
                                <span class="custom-control-label">Do Something else</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox5" value="option5">
                                <span class="custom-control-label">Eat healthy, Eat Fresh..</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox6" value="option6" checked="">
                                <span class="custom-control-label">Finsh something more..</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox7" value="option7" checked="">
                                <span class="custom-control-label">Do something more</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox8" value="option8">
                                <span class="custom-control-label">Updated more files</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox9" value="option9">
                                <span class="custom-control-label">System updated</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="d-flex p-3 border-top border-bottom">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox10" value="option10">
                                <span class="custom-control-label">Settings Changings...</span>
                            </label>
                            <span class="ml-auto">
                                <i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Edit"></i>
                                <i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
                            </span>
                        </div>
                        <div class="text-center pt-5">
                            <a href="#" class="btn btn-primary">Upgrade more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </div>
    <!-- End Rightsidebar-->
</div>