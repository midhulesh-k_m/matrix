<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}
	public function index()
	{

		$this->load->view('layout/header');
		$this->load->view('layout/menu');
		$this->load->view('dashboard');
		$this->load->view('layout/footer');
	}
}
