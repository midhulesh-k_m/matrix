<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/login by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:31:07 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="IndoUi – Bootstrap 4 Admin Dashboard HTML Template" name="description">
    <meta content="Spruko Technologies Private Limited" name="author">
    <meta name="keywords" content="admin, admin dashboard template, admin panel template, admin template, best bootstrap admin template, bootstrap 4 admin template, bootstrap 4 dashboard template, bootstrap admin template, bootstrap dashboard template, html admin template, html5 dashboard template, html5 admin template, modern admin template, simple admin template, template admin bootstrap 4" />
    <!-- Favicon -->
    <link rel="icon" href="assets/images/brand/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/brand/favicon.ico" />
    <!-- Title -->
    <title>IndoUi – Bootstrap 4 Admin Dashboard HTML Template</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <!--Style css -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/dark-style.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/css/skin-modes.css" rel="stylesheet">
    <!-- P-scrollbar css-->
    <link href="<?= base_url() ?>assets/plugins/p-scrollbar/p-scrollbar.css" rel="stylesheet" />
    <!-- Sidemenu css -->
    <link href="<?= base_url() ?>assets/plugins/sidemenu/sidemenu.css" rel="stylesheet" />
    <!--Daterangepicker css-->
    <link href="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <!-- Rightsidebar css -->
    <link href="<?= base_url() ?>assets/plugins/sidebar/sidebar.css" rel="stylesheet">
    <!---Icons css-->
    <link href="<?= base_url() ?>assets/css/icons.css" rel="stylesheet" />

    <!---Switcher css-->
    <link href="<?= base_url() ?>assets/switcher/css/switcher.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/switcher/demo.css" rel="stylesheet" />
</head>

<body class="bg-account">
    <!-- page -->
    <div class="page">

        <!-- page-content -->
        <div class="page-content">
            <div class="container text-center text-dark">
                <div class="row">
                    <div class="col-lg-4 d-block mx-auto">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-md-12">
                                <div class="card">
                                    <?php if ($success = $this->session->flashdata('app_error')) : ?>
                                        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                                            <span class="alert-inner--icon"><i class="fe fe-slash"></i></span>
                                            <span class="alert-inner--text"><strong>Incorrect</strong>&nbsp username or password !</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <div class="card-body">
                                        <div class="text-center mb-6">
                                            <img src="<?= base_url() ?>assets/images/brand/logo.png" class="header-brand-img main-logo" alt="IndoUi logo">
                                            <img src="<?= base_url() ?>assets/images/brand/logo-light.png" class="header-brand-img dark-main-logo" alt="IndoUi logo">
                                        </div>

                                        <h3>Login</h3>
                                        <p class="text-muted">Sign In to your account</p>
                                        <form method="post" action="<?= base_url() ?>login/login_chk">
                                            <div class="input-group mb-3">
                                                <span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
                                                <input type="text" name="u_name" class="form-control" placeholder="Username">
                                            </div>
                                            <div class="input-group mb-4">
                                                <span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
                                                <input type="password" name="pass" class="form-control" placeholder="Password">
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                                </div>
                                        </form>
                                        <div class="col-12">
                                            <a href="forgot-password.html" class="btn btn-link box-shadow-0 px-0">Forgot password?</a>
                                        </div>
                                    </div>
                                    <div class="mt-6 btn-list">
                                        <button type="button" class="btn btn-icon btn-facebook"><i class="fa fa-facebook"></i></button>
                                        <button type="button" class="btn btn-icon btn-google"><i class="fa fa-google"></i></button>
                                        <button type="button" class="btn btn-icon btn-twitter"><i class="fa fa-twitter"></i></button>
                                        <button type="button" class="btn btn-icon btn-dribbble"><i class="fa fa-dribbble"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-content end -->

    <!-- Jquery js-->
    <script src="<?= base_url() ?>assets/js/jquery-3.4.1.min.js"></script>
    <!--Bootstrap.min js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--Jquery Sparkline js-->
    <script src="<?= base_url() ?>assets/js/jquery.sparkline.min.js"></script>
    <!-- Chart Circle js-->
    <script src="<?= base_url() ?>assets/js/circle-progress.min.js"></script>
    <!-- Star Rating js-->
    <script src="<?= base_url() ?>assets/plugins/rating/jquery.rating-stars.js"></script>
    <!--Moment js-->
    <script src="<?= base_url() ?>assets/plugins/moment/moment.min.js"></script>
    <!-- Daterangepicker js-->
    <script src="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Custom js-->
    <script src="<?= base_url() ?>assets/js/custom.js"></script>
</body>

<!-- Mirrored from laravel.spruko.com/indoui/Leftmenu-Icon-LightSidebar-ltr/login by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Jul 2020 18:31:07 GMT -->

</html>