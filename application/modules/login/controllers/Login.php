<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}
	public function index()
	{
		$this->load->view('login');
	}
	public function login_chk()
	{
		$uname = 	$this->input->post('u_name');
		$pass =	$this->input->post('pass');
		$response =	$this->login_model->checkLogin($uname, $pass);
		if ($response) {
			$user_data = array(
				'login_id'  => $response['login_id'],
				'email_id'  => $response['email_id'],
				'user_name'  => $response['user_name'],
				'user_type'  => $response['user_type'],
				'soft_id'  => $response['soft_id'],
			);

			$this->session->set_userdata($user_data);

			redirect('dashboard');
		} else {
			$this->session->set_flashdata('app_error', 'Incorrect username or password');
			redirect('login');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata(array("login_id" => "", "user_type" => "", "email" => "", "user_name" => "", "role" => "",));
		$this->session->sess_destroy();
		redirect('login');
		// $this->session->set_flashdata('app_success', 'logout');
		// if ($_SESSION['user_type'] == 4) {
		// 	redirect('login');
		// } else {
		// 	redirect('login');
		// }
	}
	public function notfound()
	{
		$this->load->view('404');
	}
}
