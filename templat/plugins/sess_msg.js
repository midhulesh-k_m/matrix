function sessionmsg(status, msg, time = null) {
	const Toast = Swal.mixin({
		toast: true,
		position: "top-end",
		showConfirmButton: false,
		timer: 3000,
	});
	if (status == "error") {
		Toast.fire({
			icon: "error",
			title: msg,
		});
	} else {
		Toast.fire({
			icon: "success",
			title: msg,
		});
		$("#brand_frm")[0].reset();
	}
	if (time != "") {
		setTimeout(function () {
			location.reload(true);
		}, 2000);
	}
}
